[日本語](README.md) | [Esperanto](README.eo.md) | do Zokale wagone

**nubome:** suboko dokume ta.

# Zokale (do Zokale wagone)

tekemo gulobo wagone do Zokale wagone. pazila ise duwole imu polokalame wagone.

- **gulobo wagone**
	- wagone ito ola maheno puhuta eyobudazo nedo imo eyogawibo nedo.
	- ino suta kilehe ima kilehobelube.
- **taluko tinabo wagone**
	- layoto napila inozelase ola taluko tinaba makune ito ola ino tukala ise eyonede wade imu makune.
- **linego wagone**
	- wagone ito ola hasoga tehudanede imu yulilane iniba.

## ekuside imu bumuto zodase ima tulage

pamelo kalone (RC) ekusido tulage.

ino tukala ide duwolo tulage **kalame**.

- nahuda hatalo onodete aga kalame iniba.
- mahika kamalo tukale ago wa na ima tikima imo tasalipasa imo hapusa sa iso na ide mahikonede wa ima eka hatola isi ino masale wa.

**ino pawata yumo wade.** oyo tulaka iyu kalame imu tukale imo wadotehude sa.

## ibo kalame

- **kumolo hopute** ekusido selite. ino kulaso lukela oyu kilibe sa. ihi yotula anu iniselite ya ihe kumola kumupalo pege ya.
- ihasido menulisa ola oli menulisa isa do Laten letole selite. ihi menulisa isa da Zokale imu letole (edu pamela anu ga [Dropbox](https://www.dropbox.com/scl/fo/02c9r81ihsflukr8u6mvs/AJTkcdwBNMKo1nLSC6OawYg?rlkey=2sz58wb7cz2ipn4kilyi0ooy6&st=bukjurun&dl=0) suboko tipalo) ya ihe oyo suta eyowahe hoputa.

## ibo kamuse

- yubite imo sugate melukowade. womita luzoselesoletole aga wade ya ego owa obu humana kasume.
- ino pisama putake ima meluke. kumola kumupalo kalame ya.
- ino leniso belisa waheno nome tako wade.
- ihi bolida mudaho kubitela aga muto luze tizo luze ihe oyo ino pisama anu kamuse so luze. **ihi bolida memaha makune ihe bolida kayuta ino pisamo luze (libana olu belisa go:392 “◯◯化禁止” lipe wade iniba).**
- huwala globosebulo kepile pekeme. **ino huwala lominakopol pekeme ine ino segula tike sa.** ihi moka pekeme ihe hapusa pekeme aga kamuse wa inu leniso ino hala wade imu tukale.

## ibo kalamotikimabiluke

- ihi suta yamako nabite ihe menulisa yamako lalute wa.
- ihi bolida tulukita eze kakuso hesile ihe ezuluko menulisa lalute wa imo menulisa olo iha latuta tehudanede wa.

## letezo litibe

nubome: menusa eze 95% isa do Nihon wagone toto leteze.

- [kalamo leteze](https://eotplb.gitlab.io/do-zokale-wagone/)
- [Tumblr](https://www.tumblr.com/zokale4blog) - siso kepilo leteze
- [Mastodon](https://mastodon.social/@zokale) \* **yapita lipasa to pukete.**

## lisene

- edu tehuda ise da [SELIYAMA Halusige](https://gitlab.com/eotplb) to tekemo wagone.
- ga [Creative Commons (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.eo)* nabite - peline 4.0 gulobe lisene.

\* do Esupelanto page.