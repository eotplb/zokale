# 修飾詞

## 概要

後に続く名詞、動詞、他の修飾詞を修飾します。

他の一般的な言語では、名詞を修飾する場合は形容詞、動詞を修飾する場合は副詞と呼ばれます。この解説でも便宜上これらの呼称を使う事があります。

修飾方法は以下の解説以外に合成語による表現もあります。詳細は合成語のページを参照して下さい。

### 単語の構造

品詞語尾 **‐o** を付ける事で表します。

- lapolo = 大きい
	- **lapolo** okonede = **大きな**男性
- pileno = 小さい
	- **pileno** pusile = **小さな**猫

## 名詞を修飾する

- kekete = 笑い
	- **keketo** nugale = **笑いの**極意
- dolume = 眠り
	- **dolumo** zokale = **眠りの**世界
- line = 鉄
	- **lino** hapage = **鉄の**机
- sekole = 学校
	- **sekolo** gule = **学校の**先生

## 動詞を修飾する

- lapido = 速い、急ぐ
	- **lapido** kabela = **速く**歩く、**急いで**歩く
- lapolo = 大きい
	- **lapolo** baluta = **大きく**避ける

## 他の修飾詞を修飾する

- lago = とても、非常に
	- **lago** lapolo kudole = **とても**大きい犬
	- **lago** sukalo hesile = **非常に**難しい質問

## 動詞的修飾詞

修飾詞によっては動詞のように使う事も出来ます。

- **lapolo** ta pusile. = この猫は**大きい。**
- **lago kahiko** za helamaze. = あのコンピューターは**とても古い。**

## 近似表現(喩え)

修飾詞の前に推測を表すマーカー **oyu** を置く事で「～のような(もの)」「～のような(何か)」を表します。

一部の修飾詞は初めから近似表現を含むものもあります。その場合は **oyu** は不要です。

近似表現は動詞の代わりに使うもののため、**動詞を伴う事は出来ません。⁠**動詞を伴う場合は**比喩**(前置詞のページを参照)を使います。

- pusile = 猫
	- **oyu** pusilo = 猫**のようなもの**
		- edu lenuta lane oyu pusilo. = 猫のようなものが空を飛んでいます。
- penitale = 鉛筆
	- **oyu** penitalo = 鉛筆**のようなもの**
		- oyu penitalo ta. = これは鉛筆のようなものです。
- wida = 泳ぐ
	- **oyu** wido = 泳ぐ**ようなもの**
		- oyu wido ani mapele sa. = それは地中で泳ぐようなものです。
- pileno huta = 小声で叫ぶ
	- **oyu** pileno huto = 小声で叫ぶ**ようなもの**
		- oyu pileno huto za. = あれは小声で叫ぶようなものです。

## 文全体の修飾

文頭に修飾詞とカンマを置き、その後に本文を続けます。カンマの部分で一度発音を区切ります。

- **kawolo,** ino bolida siwoda pawome wa. = **残念な事ですが、⁠**私はリンゴを食べられません。

## 注意点

修飾詞と修飾される単語は必ず対になる必要があります。

|||
|-|-|
|**welo** lumahe ima mobile|**赤い**家と(色が不明の)車|
|**welo** lumahe ima **welo** mobile|**赤い**家と**赤い**車|
|**lapido** kulena ima siwoda|**急いで**掃除し、(不明の速度で)食べる|
|**lapido** kulena ima **lapido** siwoda|**急いで**掃除し、**急いで**食べる|
|**oyu** pusilo ima kudolo|猫**のようなもの**と犬|
|**oyu** pusilo ima **oyu** kudolo|猫**のようなもの**と犬**のようなもの**|
