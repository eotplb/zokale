# 分類名詞辞書

原則として学術名(ラテン語)を語源としていますが、起源を元にした方が良いと判断した場合は単語が大幅に変更される可能性があります。

分類名称識別子が省略されているものは単語の前に置く必要があります。

|名詞|修飾詞|自動詞|
|:-:|:-:|:-:|
|da|do|du|

アクセントは**最終音節で固定**です。元の言語のアクセントを使わないよう注意して下さい。

## 周期表 (118)

|⁠⁠|⁠|日本名|分類名詞|
|-:|:-:|-|-|
|1|H|水素|Hidologeniumu|
|2|He|ヘリウム|Heliumu|
|3|Li|リチウム|Liciumu|
|4|Be|ベリリウム|Beliliumu|
|5|B|ホウ素|Boliumu|
|6|C|炭素|Kaluboniumu|
|7|N|窒素|Nitologeniumu|
|8|O|酸素|Osigeniumu|
|9|F|フッ素|Huluolumu|
|10|Ne|ネオン|Neon|
|11|Na|ナトリウム|Natoliumu|
|12|Mg|マグネシウム|Magunesiumu|
|13|Al|アルミニウム|Aluminiumu|
|14|Si|ケイ素|Silikiumu|
|15|P|リン|Fosufolusu|
|16|S|硫黄|Suluhulu|
|17|Cl|塩素|Kulolumu|
|18|Ar|アルゴン|Alugon|
|19|K|カリウム|Kaliumu|
|20|Ca|カルシウム|Kalukiumu|
|21|Sc|スカンジウム|Sukandiumu|
|22|Ti|チタン|Titaniumu|
|23|V|バナジウム|Banadiumu|
|24|Cr|クロム|Kulomiumu|
|25|Mn|マンガン|Manganumu|
|26|Fe|鉄|Felumu|
|27|Co|コバルト|Kobalutumu|
|28|Ni|ニッケル|Nikolumu|
|29|Cu|銅|Kupulumu|
|30|Zn|亜鉛|Zinkumu|
|31|Ga|ガリウム|Galiumu|
|32|Ge|ゲルマニウム|Gelumaniumu|
|33|As|ヒ素|Alusenikumu|
|34|Se|セレン|Seleniumu|
|35|Br|臭素|Bulomu|
|36|Kr|クリプトン|Kuliputon|
|37|Rb|ルビジウム|Lubidiumu|
|38|Sr|ストロンチウム|Sutolontiumu|
|39|Y|イットリウム|Itoliumu|
|40|Zr|ジルコニウム|Zilukoniumu|
|41|Nb|ニオブ|Niobiumu|
|42|Mo|モリブデン|Molibudenumu|
|43|Tc|テクネチウム|Tekunetiumu|
|44|Ru|ルテニウム|Luteniumu|
|45|Rh|ロジウム|Lodiumu|
|46|Pd|パラジウム|Paladiumu|
|47|Ag|銀|Alugentumu|
|48|Cd|カドミウム|Kadomiumu|
|49|In|インジウム|Indiumu|
|50|Sn|スズ|Sutanum|
|51|Sb|アンチモン|Sutibiumu|
|52|Te|テルル|Teluliumu|
|53|I|ヨウ素|Iodumu|
|54|Xe|キセノン|Xenon|
|55|Cs|セシウム|Kaesiumu|
|56|Ba|バリウム|Baliumu|
|57|La|ランタン|Lantanumu|
|58|Ce|セリウム|Keliumu|
|59|Pr|プラセオジム|Pulaseodimiumu|
|60|Nd|ネオジム|Neodimiumu|
|61|Pm|プロメチウム|Pulomeciumu|
|62|Sm|サマリウム|Samaliumu|
|63|Eu|ユウロピウム|Yulopiumu|
|64|Gd|ガドリニウム|Gadoliniumu|
|65|Tb|テルビウム|Telubiumu|
|66|Dy|ジスプロシウム|Disupulosiumu|
|67|Ho|ホルミウム|Holumiumu|
|68|Er|エルビウム|Elubiumu|
|69|Tm|ツリウム|Tuliumu|
|70|Yb|イッテルビウム|Itelubiumu|
|71|Lu|ルテチウム|Lutetiumu|
|72|Hf|ハフニウム|Hahuniumu|
|73|Ta|タンタル|Tantalumu|
|74|W|タングステン|Woluhulamiumu|
|75|Re|レニウム|Leniumu|
|76|Os|オスミウム|Osumiumu|
|77|Ir|イリジウム|Ilidiumu|
|78|Pt|白金|Pulatinumu|
|79|Au|金|Aulumu|
|80|Hg|水銀|Hidolalugilumu|
|81|Tl|タリウム|Taliumu|
|82|Pb|鉛|Pulumubumu|
|83|Bi|ビスマス|Bisemutumu|
|84|Po|ポロニウム|Poloniumu|
|85|At|アスタチン|Asutatumu|
|86|Rn|ラドン|Ladon|
|87|Fr|フランシウム|Hulankiumu|
|88|Ra|ラジウム|Ladiumu|
|89|Ac|アクチニウム|Akutiniumu|
|90|Th|トリウム|Toliumu|
|91|Pa|プロトアクチニウム|Pulotakutiniumu|
|92|U|ウラン|Ulaniumu|
|93|Np|ネプツニウム|Neputuniumu|
|94|Pu|プルトニウム|Pulutoniumu|
|95|Am|アメリシウム|Amelikiumu|
|96|Cm|キュリウム|Kuliumu|
|97|Bk|バークリウム|Belukeliumu|
|98|Cf|カリホルニウム|Kalifoluniumu|
|99|Es|アインスタイニウム|Ainsuteiniumu|
|100|Fm|フェルミウム|Felumiumu|
|101|Md|メンデレビウム|Mendelebiumu|
|102|No|ノーベリウム|Nobeliumu|
|103|Lr|ローレンシウム|Lolenkiumu|
|104|Rf|ラザホージウム|Lucelufodiumu|
|105|Db|ドブニウム|Dubuniumu|
|106|Sg|シーボーギウム|Sibolugiumu|
|107|Bh|ボーリウム|Bohuliumu|
|108|Hs|ハッシウム|Hasiumu|
|109|Mt|マイトネリウム|Meitoneliumu|
|110|Ds|ダームスタチウム|Dalumusutadotiumu|
|111|Rg|レントゲニウム|Loentogeniumu|
|112|Cn|コペルニシウム|Kopelunikiumu|
|113|Nh|ニホニウム|Nihoniumu|
|114|Fl|フレロビウム|Hulelobiumu|
|115|Mc|モスコビウム|Mosukobiumu|
|116|Lv|リバモリウム|Libelumoliumu|
|117|Ts|テネシン|Tenesine|
|118|Og|オガネソン|Oganeson|

## 星座 (88)

黄道十二宮の星座は占星術に使う単語とは異なります。

|日本名|分類名詞|
|-|-|
|アンドロメダ座|Andolomeda|
|いっかくじゅう座|Monokelosu|
|いて座|Sazitaliusu|
|いるか座|Delufinusu|
|インディアン座|Indusu|
|うお座|Pisukesu|
|うさぎ座|Lepusu|
|うしかい座|Bootesu|
|うみへび座|Hidola|
|エリダヌス座|Elidanusu|
|おうし座|Taulusu|
|おおいぬ座|lapolo da Kanisu|
|おおかみ座|Lupusu|
|おおぐま座|lapolo da Ulusa|
|おとめ座|Bilugo|
|おひつじ座|Aliesu|
|オリオン座|Olion|
|がか座|Pikutolu|
|カシオペヤ座|Kasiopea|
|かじき座|Dolado|
|かに座|Kankelu|
|かみのけ座|Koma|
|カメレオン座|Kameleon|
|からす座|Kolubusu|
|かんむり座|Kolona|
|きょしちょう座|Tukana|
|ぎょしゃ座|Auliga|
|きりん座|Kamelopaludalisu|
|くじゃく座|Pabo|
|くじら座|Ketusu|
|ケフェウス座|Kefeusu|
|ケンタウルス座|Kentaulusu|
|けんびきょう座|Mikulosukopiumu|
|こいぬ座|pileno da kanisu|
|こうま座|Ekuuleusu|
|こぎつね座|Bulupekula|
|こぐま座|pileno da Ulusa|
|こじし座|pileno da Leo|
|コップ座|Kulatelu|
|こと座|Lila|
|コンパス座|Kilukinusu|
|さいだん座|Ala|
|さそり座|Sukolupiusu|
|さんかく座|Toliangulumu|
|しし座|Leo|
|じょうぎ座|Noluma|
|たて座|Sukutumu|
|ちょうこくぐ座|Kaelumu|
|ちょうこくしつ座|Sukuluputolu|
|つる座|Gulus|
|テーブルさん座|Mensa|
|てんびん座|Libula|
|とかげ座|Lakeluta|
|とけい座|Holologiumu|
|とびうお座|Bolansu|
|とも座|Pupisu|
|はえ座|Musuka|
|はくちょう座|Kigunusu|
|はちぶんぎ座|Okutanusu|
|はと座|Kolumuba|
|ふうちょう座|Apusu|
|ふたご座|Gemini|
|ペガスス座|Pegasusu|
|へび座|Selupensu|
|へびつかい座|Ofiucusu|
|ヘルクレス座|Helukulesu|
|ペルセウス座|Peluseusu|
|ほ座|Bela|
|ぼうえんきょう座|Telesukopiumu|
|ほうおう座|Fenikusu|
|ポンプ座|Antila|
|みずがめ座|Akualius|
|みずへび座|Hidolusu|
|みなみじゅうじ座|Kulukusu|
|みなみのうお座|selato da Pisukisu|
|みなみのかんむり座|selato da Kolona|
|みなみのさんかく座|selato da Toliangulume|
|や座|Sagita|
|やぎ座|Kapulikolunusu|
|やまねこ座|Linkusu|
|らしんばん座|Pikusisu|
|りゅう座|Dolako|
|りゅうこつ座|Kalina|
|りょうけん座|Kanesu|
|レチクル座|Letikulumu|
|ろ座|Folunalusu|
|ろくぶんぎ座|Sekusutansu|
|わし座|Akuila|

## 宝石 (32)

周期表と重複しているものがあります。

名称に色を含むものは修飾詞で補います。**普通の単語と分類名詞は合成出来ないので注意して下さい。**

- ブラックダイアモンド = musuto da Adamasu
- ローズクォーツ = momolo da Kuwoluzumu

|日本名|分類名詞|
|-|-|
|アイオライト|Aiolaito|
|アクアマリン|Akuamalina|
|アゲート(瑪瑙)|Ageto|
|アメシスト|Amesisutasu|
|アレキサンドライト|Alekisandolita|
|アンバー(琥珀)|Sukinumu|
|エメラルド|Sumalagudusu|
|オパール|Opalusu|
|ガーネット(柘榴石)|Gulanatumu|
|カウリガム|Kauligam|
|クォーツ(石英)|Kuwoluzumu|
|クリソベリル･キャッツ･アイ|Kulisoberilasukatusuokulo|
|クンツァイト|Kuntuwaito|
|コーラル(珊瑚)|Kolaliumu|
|サードニクス|Saadonikusu|
|サファイア|Safilusu|
|ジャスパー(碧玉)|Jasupaa|
|ジェダイト(硬翡翠)|Jedaitosu|
|シトリン|Sitolin|
|ジルコン|Zilukoniumu|
|スピネル|Supinele|
|スフィーネ|Sufiine|
|ターコイズ|Kalainusu|
|ダイアモンド|Adamasu|
|タンザナイト|Tanuzanaito|
|トパーズ|Topaziumu|
|トルマリン|Taamalinusu|
|パール(養殖真珠)|Malugalita|
|ブラッドストーン|Heliotolopiumu|
|ペリドット|Kulisolitosu|
|ムーンストーン|Lapisulunalisu|
|モルガナイト|Moluganaito|
|ラピス･ラズリ|Lapisulazuli|
|ルビー|Kaabankulu|

## 国名 (206)

国名は通常 ISO 3166-1 の番号を0で空桁を埋めて以下のように表記します。

【例】 banusoluko 056 = ベルギー王国

分類名詞による表記(英語が語源)は補助的なものです。

**国際情勢により、内容が変更される可能性があります。**

|番号|日本名|分類名詞(非推奨)|
|:-:|-|-|
|352|アイスランド共和国|Aisulando|
|372|アイルランド|Ailulando|
|031|アゼルバイジャン共和国|Azelubaijan|
|004|アフガニスタン･イスラム共和国|Ahuganisutan|
|---|アブハジア共和国|Abuhazia|
|840|アメリカ合衆国|Amelika|
|784|アラブ首長国連邦|Alabu|
|012|アルジェリア民主人民共和国|Alujelia|
|032|アルゼンチン共和国|Alujentina|
|---|アルツァフ共和国|Alutuwahu|
|008|アルバニア共和国|Alubania|
|051|アルメニア共和国|Alumenia|
|024|アンゴラ共和国|Angola|
|028|アンティグア･バーブーダ|Antigua Ima Baabuuda|
|020|アンドラ公国|Andola|
|887|イエメン共和国|Iemen|
|826|イギリス(グレートブリテン及び北アイルランド連合王国)|Bulitixu|
|376|イスラエル国|Isulaelu|
|380|イタリア共和国|Italii|
|368|イラク共和国|Ilaku|
|364|イラン･イスラム共和国|Ilan|
|356|インド|India|
|360|インドネシア共和国|Indonesia|
|800|ウガンダ共和国|Uganda|
|804|ウクライナ|Ukulaina|
|860|ウズベキスタン共和国|Uzubekisutan|
|858|ウルグアイ東方共和国|Uluguai|
|218|エクアドル共和国|Ekuadolu|
|818|エジプト･アラブ共和国|Eziputo|
|233|エストニア共和国|Esutonia|
|748|エスワティニ王国|Wesuwatini|
|231|エチオピア連邦民主共和国|Eciopia|
|232|エリトリア国|Elitolia|
|222|エルサルバドル共和国|Elusalubadolu|
|512|オマーン国|Omaan|
|528|オランダ王国|Neizaalando|
|036|オーストラリア連邦|Oosutolalia|
|040|オーストリア共和国|Oosutolia|
|328|ガイアナ共和国|Gaiana|
|398|カザフスタン共和国|Kazahusutan|
|634|カタール国|Kataalu|
|124|カナダ|Kanada|
|266|ガボン共和国|Gabon|
|120|カメルーン共和国|Kameluun|
|270|ガンビア共和国|Ganbia|
|116|カンボジア王国|Kanbodia|
|288|ガーナ共和国|Gaana|
|132|カーボベルデ共和国|Kapebelude|
|624|ギニアビサウ共和国|Giniabisau|
|324|ギニア共和国|Giana|
|196|キプロス共和国|Kipulosu|
|192|キューバ共和国|Kuuba|
|300|ギリシャ共和国|Guleese|
|296|キリバス共和国|Kilibasu|
|417|キルギス共和国|Kilugisutan|
|320|グアテマラ共和国|Guatemala|
|414|クウェート国|Kuweito|
|184|クック諸島|Kuku|
|308|グレナダ|Gulenada|
|191|クロアチア共和国|Kuloatia|
|404|ケニア共和国|Kenia|
|188|コスタリカ共和国|Kosutalika|
|---|コソボ共和国|Kosobo|
|174|コモロ連合|Komolo|
|170|コロンビア共和国|Kolonbia|
|178|コンゴ共和国|Kongo|
|180|コンゴ民主共和国|lipubiko da Kongo|
|384|コートジボワール共和国|Kootoziboaalu|
|682|サウジアラビア|Saudialabia|
|732|サハラ･アラブ民主共和国(西サハラ)|balato Sahala|
|882|サモア独立国|Samoa|
|678|サントメ･プリンシペ民主共和国|Saotome Ima Pulinsipe|
|894|ザンビア共和国|Zanbia|
|674|サンマリノ共和国|Sanmalino|
|694|シエラレオネ共和国|Sielaleone|
|262|ジブチ共和国|Zibuti|
|388|ジャマイカ|Jamaika|
|268|ジョージア|Joozia|
|760|シリア･アラブ共和国|Silia|
|702|シンガポール共和国|Singapoolu|
|716|ジンバブエ共和国|Zinbabuwe|
|756|スイス連邦|Suwitulando|
|752|スウェーデン王国|Suweeden|
|724|スペイン王国|Supein|
|740|スリナム共和国|Sulinamu|
|144|スリランカ民主社会主義共和国|Sulilanka|
|703|スロバキア共和国|Sulobakia|
|705|スロベニア共和国|Sulobenia|
|729|スーダン共和国|Suudan|
|686|セネガル共和国|Senegalu|
|688|セルビア共和国|Selubia|
|659|セントクリストファー･ネイビス|Sentokitu Ima Nebisu|
|670|セントビンセント及びグレナディーン諸島|Sentobinsento Ima Gulenadiin|
|662|セントルシア|Sentolusia|
|690|セーシェル|Seexelu|
|706|ソマリア連邦共和国|Somalia|
|---|ソマリランド共和国|Somalilando|
|090|ソロモン諸島|Solomon|
|764|タイ王国|Tailando|
|762|タジキスタン共和国|Tazikisutan|
|834|タンザニア連合共和国|Tanzania|
|203|チェコ共和国|Cecia|
|148|チャド共和国|Cado|
|788|チュニジア共和国|Cunizia|
|152|チリ共和国|Cili|
|798|ツバル|Tubalu|
|208|デンマーク王国|Denmaaku|
|276|ドイツ連邦共和国|Jaamani|
|214|ドミニカ共和国|lipubiko da Dominika|
|212|ドミニカ国|Dominika|
|780|トリニダード･トバゴ共和国|Tolinidaado Imu Tobago|
|795|トルクメニスタン|Tolukumenisutan|
|792|トルコ共和国|Toluko|
|776|トンガ王国|Tonga|
|768|トーゴ|Toogo|
|566|ナイジェリア連邦共和国|Naijelia|
|520|ナウル共和国|Naulu|
|516|ナミビア共和国|Namibia|
|570|ニウエ|Niue|
|558|ニカラグア共和国|Nikalagua|
|562|ニジェール共和国|Nijeelu|
|554|ニュージーランド|Niuziilando|
|524|ネパール連邦民主共和国|Nepaal|
|578|ノルウェー王国|Noluwei|
|332|ハイチ共和国|Haici|
|586|パキスタン･イスラム共和国|Pakisutan|
|336|バチカン市国|Bacikan|
|591|パナマ共和国|Panama|
|548|バヌアツ共和国|Banuatu|
|044|バハマ国|Bahama|
|598|パプアニューギニア独立国|Papuwaniuginia|
|585|パラオ共和国|Palau|
|600|パラグアイ共和国|Palaguai|
|052|バルバドス|Balubadosu|
|275|パレスチナ国|Palesucina|
|348|ハンガリー|Hangali|
|050|バングラデシュ人民共和国|Banguladexu|
|048|バーレーン王国|Baalain|
|242|フィジー共和国|Fizi|
|608|フィリピン共和国|Filipin|
|246|フィンランド共和国|Finlando|
|076|ブラジル連邦共和国|Bulazilu|
|250|フランス共和国|Hulanse|
|100|ブルガリア共和国|Bulugalia|
|854|ブルキナファソ|Bulukinafaso|
|096|ブルネイ･ダルサラーム国|Bulunei|
|108|ブルンジ共和国|Bulundi|
|064|ブータン王国|Buutan|
|704|ベトナム社会主義共和国|Betonamu|
|204|ベナン共和国|Benin|
|862|ベネズエラ･ボリバル共和国|Benezuela|
|112|ベラルーシ共和国|Belalusu|
|084|ベリーズ|Belize|
|056|ベルギー王国|Belugiumu|
|604|ペルー共和国|Peluu|
|070|ボスニア･ヘルツェゴビナ|Bosunia Ima Heluzegobina|
|072|ボツワナ共和国|Botuwana|
|068|ボリビア多民族国|Bolibia|
|620|ポルトガル共和国|Polutugalu|
|340|ホンジュラス共和国|Honjulasu|
|616|ポーランド共和国|Poolando|
|450|マダガスカル共和国|Madagasukalu|
|454|マラウイ共和国|Malawi|
|466|マリ共和国|Mali|
|470|マルタ共和国|Maluta|
|458|マレーシア|Maleisia|
|584|マーシャル諸島共和国|Maaxalu|
|583|ミクロネシア連邦|Mikulonesia|
|104|ミャンマー連邦共和国|Miyanmaa|
|484|メキシコ合衆国|Mekisiko|
|508|モザンビーク･イスラム共和国|Mozanbiiku|
|492|モナコ公国|Monako|
|462|モルディブ共和国|Moludibu|
|498|モルドバ共和国|Moludoba|
|504|モロッコ王国|Moloko|
|496|モンゴル国|Mongolu|
|499|モンテネグロ|Montenegulo|
|480|モーリシャス共和国|Moolixasu|
|478|モーリタニア･イスラム共和国|Moolitania|
|400|ヨルダン|Yoludan|
|418|ラオス人民民主共和国|Lao|
|428|ラトビア共和国|Latobia|
|440|リトアニア共和国|Litoania|
|434|リビア|Libia|
|438|リヒテンシュタイン公国|Lihitenxutain|
|430|リベリア共和国|Libelia|
|442|ルクセンブルク大公国|Lukusenbulugu|
|646|ルワンダ共和国|Luwanda|
|642|ルーマニア|Luumania|
|426|レソト王国|Lesoto|
|422|レバノン共和国|Lebanon|
|643|ロシア連邦|Lusia|
|---|沿ドニエストル共和国|Tolansunisutolia|
|226|赤道ギニア共和国|wekudo da Ginia|
|410|大韓民国(韓国)|Kolea|
|140|中央アフリカ共和国|tunugo da Ahulikan|
|156|中華人民共和国(中国)|Cina|
|158|中華民国(台湾)|Taiwan|
|243|朝鮮民主主義人民共和国(北朝鮮)|nutalo da Korea|
|626|東ティモール民主共和国|timulo da Timoolu|
|710|南アフリカ共和国|selato da Ahulika|
|---|南オセチア共和国･アラニヤ国|selato da Osetia / Alania|
|728|南スーダン共和国|selato da Suudan|
|392|日本国|Japan / Nihon|
|---|北キプロス･トルコ共和国|nutalo da Kipulosu|
|807|北マケドニア共和国|nulato da Makedonia|

## 海外領土･自治領 (52)

一部の地域は国名一覧と重複しています。複数の地域に跨がっている場合もあります。

これらは通常 ISO 3166-1 の番号を0で空桁を埋めて以下のように表記します。

【例】 anemowanoluko 060 = バミューダ諸島

分類名詞による表記は補助的なものです。**可能な限り現地または属する国の言語の発音を反映しているため、一般に耳にする呼称とは大きく異なるものもあります。**

**国際情勢により、内容が変更される可能性があります。**

### ヨーロッパ 7

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|831|ガーンジー|Gaanzii|イギリス|
|292|ジブラルタル|Zibulalutalu|イギリス|
|832|ジャージー|Jaazii|イギリス|
|833|マン島|nuso da Man|イギリス|
|234|フェロー諸島|monetonuso da Feloo|デンマーク|
|744|スヴァールバル諸島|monetonuso da Subaalubalu|ノルウェー|
|248|オーランド諸島|monetonuso da Oolando|フィンランド|

### アフリカ 4

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|086|インド洋地域|mowano do India delahe|イギリス|
|654|セントヘレナ･アセンション及びトリスタンダクーニャ|da Seintohelena ima da Asenxon ima da Tolisutandakuuniya|イギリス|
|175|マヨット|Mayotuto|フランス|
|638|レユニオン|Leyunion|フランス|

### 北アメリカ 18

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|850|ヴァージン諸島|monetonuso do Amelika da Baazin|アメリカ|
|581|合衆国領有小離島|monetopilenonuso da Amelika|アメリカ|
|630|プエルトリコ|Puelutoliko|アメリカ|
|660|アンギラ|Anguila|イギリス|
|092|ヴァージン諸島|monetonuso do Bulitixu da Baazin|イギリス|
|136|ケイマン諸島|monetonuso da Keiman|イギリス|
|796|タークス･カイコス諸島|monetonuso da Taakusu ima da Keikosu|イギリス|
|060|バミューダ諸島|Bamiuda|イギリス|
|500|モントセラト|Montoselato|イギリス|
|533|アルバ|Aluba|オランダ|
|531|キュラソー|Kiyulaso|オランダ|
|534|シント･マールテン|Sintomaaluten|オランダ|
|312|グアドループ|Guadoluupu|フランス|
|652|サン･バルテルミー|Sanbasutelemii|フランス|
|663|サン･マルタン|Sanmalusutan|フランス|
|666|サンピエール島･ミクロン島|nuso da Sanpiele ima nuso da Mikulon|フランス|
|474|マルティニーク|Masutiniiku|フランス|
|304|グリーンランド|Kalaasitonunaato|デンマーク|

### 南アメリカ 2

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|238|フォークランド諸島(マルビナス諸島)|monetonuso da Fookulando / monetonuso da Malubinasu|イギリス|
|254|ギアナ|do Hulanse da Guian|フランス|

### オセアニア 13

\* 日本は国家として承認しています。

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|581|合衆国領有小離島|monetopilenonuso da Amelika|アメリカ|
|580|北マリアナ諸島|monetonuso nulato da Malianse|アメリカ|
|316|グアム|Guahan|アメリカ|
|016|サモア|do Amelika da Samoa|アメリカ|
|876|ウォリス･フツナ|Walisuhutuna|フランス|
|---|クリッパートン島|nuso da Kulipehuton|フランス|
|540|ニューカレドニア|Nuubelukaledonie|フランス|
|258|ポリネシア|do Hulanse da Polinesia|フランス|
|612|ピトケアン諸島|monetonuso da Pitokean|イギリス|
|162|クリスマス島|nuso da Kulisumasu|オーストラリア|
|166|ココス諸島|monetonuso da Kokosu|オーストラリア|
|574|ノーフォーク島|nuso da Noofooku|オーストラリア|
|334|ハード島･マクドナルド諸島|nuso da Haado ima monetonuso da Makudonaludo|オーストラリア|
|184|クック諸島\*|monetonuso da Kuku|ニュージーランド|
|772|トケラウ|Tokelau|ニュージーランド|
|570|ニウエ\*|Niue|ニュージーランド|

### 南極及び周辺の無人領土 8

\* 南極条約により領土主張が凍結されています。

|番⁠号|名称|分類名詞(非推奨)|属する国|
|:-:|-|-|-|
|239|サウスジョージア･サウスサンドウィッチ諸島|nuso selato da Joozia ima monetonuso selato da Sandowici|イギリス|
|---|南極地域\*|do Bulitixu selatopolo delahe|イギリス|
|260|南方･南極地域(一部\*)|do Hulanse selato ima do Hulanse selatopolo delahe|フランス|
|---|ドロンニング･モード･ランド\*|Dolonningumoodolando|ノルウェー|
|---|ピョートル1世島\*|nuso da Piata1|ノルウェー|
|074|ブーベ島|nuso da Buube|ノルウェー|
|---|南極領土\*|do Oosutolalia selatopolo delahe|オーストラリア|
|---|ロス海属領\*|mowano da Losu gulupe|ニュージーランド|
