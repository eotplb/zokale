[日本語](README.md) | Esperanto | [do Zokale wagone](README.zokale.md)

**Noto:** Ne garantias kvaliton de tradukoj el originala japana frazo.

# Zokale (Zokalezo)

Zokalezo estas artefarita internacia helplingvo konsistita de:

- **Internacia helplingvo**
	- Lingvo, kiu permesas al homoj kun malsamaj kulturoj kaj kutimoj paroli glate.
	- Idiomaĵoj kaj frazverboj ne ekzistas.
- **Strikta difinlingvo**
	- Forigi malpresizon ĝis maksimumo per strikte difinado de signifoj por malhelpi malsamajn homojn malsame interpreti saman vorton.
- **Ideala lingvo**
	- Lingvo, kiu ĉasas idealojn de aŭtoro.

## Nuna evoluostadio kaj stato 

Nuna versio estas publikiga kanditato (RC).

Principe **gramatiko** ne estos ŝanĝita krom en sekvaj kazoj.

- Kiam troviĝas fatala kriplaĵo en gramatiko.
- Kiam estas peto de tria, kaj kiel rezulto de konfirmo kun tria alia ol petanto, oni taksas, ke ne estas problemo aldoni ĝin al gramatiko.

**Ĉiaj vortoj ne fiksas.** Vortoj povas ŝanĝiĝi aŭ raciigi aŭ forstreki pro gramatikaj ŝanĝoj aŭ aldoni vorto.

## Pri gramatiko

- Nuna ekspliko estas **referenca formo.** Tio ne povas legi ilin en ordo kiel libro. Se vi renkontas neklarigitan enhavon, bonvolu referenci al responda paĝo.
- Ekspliko estas verkita supozante, ke ĝi estos verkita per latinaj literoj. Se vi skribas frazojn per specialaj literoj (prototipaj tiparoj haveblas ĉe [Dropbox](https://www.dropbox.com/scl/fo/02c9r81ihsflukr8u6mvs/AJTkcdwBNMKo1nLSC6OawYg?rlkey=2sz58wb7cz2ipn4kilyi0ooy6&st=bukjurun&dl=0)), ĝi povas esti malsama. 

## Pri vortaro

- Kapvorto estas radiko aŭ radikalo. Bonvolu elipse serĉi vortospeca finaĵo.
- Konigiloj kaj markiloj ne estas inkluzivaj. Bonvolu referenci al responda gramatikoj.
- Propra nomo ne estas inkludita en fundamenta vorto kiel ĝenerala regulo.
- Se vortospeco de vorto povas esti imagita de alia vortospeco, ĝi eble ne priskribas. **Se iu povas kompreni signifon de vortospeco, tiam vortospeco povas esti uzata (Ekskludu vortojn kun la etikedo “◯◯化禁止”).**
- Etimologio baziĝas sur informoj en interreto. **Ĝi ne garantias precizecon pro ne vortaro de eldonejo.**
 Se ĝi baziĝas sur malĝusta etimologio, forstrekas tiun etimologion. Tamen principe ne ŝanĝas vortojn.

 ## Pri gramatikprovfrazo

 - Se estas multaj esprimmanieroj, prezentas multajn ekzemplajn frazojn. 
 - Se demando povas esti multa interpretata, prezentas multajn ekzemplajn frazojn aŭ skribas respondon, kiun aŭtoro atendas.

 ## Retligiloj

Noto: Ĉiom retejoj proksimume skribas japana.

- [Gramatika retejo](https://eotplb.gitlab.io/do-zokale-wagone/)
- [Tumblr](https://www.tumblr.com/zokale4blog) - Intergrada informa retejo
- [Mastodon](https://mastodon.social/@zokale) \* **Ĉi tiu konto estos aboliciita.**

## Licenco

 Ĉi tio internacia planlingvo verkas far [SERIJAMA Haruŝige.](https://gitlab.com/eotplb)

 Licenco **momente** estas [Creative Commons Atribuite-Samkondiĉe 4.0 Tutmonda (CC BY-SA 4.0).](https://creativecommons.org/licenses/by-sa/4.0/deed.eo)