# 55 sentences to eat more apples

Quote source: [りんご文 - 人工言語学 Wiki](https://conlinguistics.wikia.org/ja/wiki/りんご文)

> Apple text are a group of example sentences created starting with the apple sentences (Zasuron list) created by Zasuron on September 20, 2014. These example sentences were created for the purpose of identifying undefined grammatical categories of the artificial language by translating them.

> 55 Sentences to Eat More Apples is an apple sentence created by Ziphil on September 2, 2015.

**Note: 🔰 disambiguates the question for clarity.**

1. This apple is bigger than that apple.
	- lapolo ika zo pawome to pawome. \* Comparison
	- ezo zo pawome to pawome. \* Calculation

2. An apple bigger than that apple.
	- olu lapolo ika zo pawome pawome ⚠Cannot be used as a stand-alone sentence.

3. This apple is larger (omit comparator)❌
	- ika lapolo to pawome. ⚠A sentence is not a sentence unless a comparison already exists in the context.
		- Example: pawome ima da Mandalin kalake tu. ika lapolo to pawome.

4. This apple is as big as that apple.
	- lapolo iku zo pawome to pawome. \* Comparison
	- eyu zo pawome to pawome. \* Calculation

5. An apple as big as that apple.
	- olu lapolo iku zo pawome pawome ⚠Cannot be used as a stand-alone sentence.

6. These apples are just as big (omission of comparables)❌
	- iku lapolo to pawome. = oli eyu to pawome. ⚠A sentence is not a sentence unless a comparison already exists in the context.
		- Example: pawome ima da Mandalin kalake tu. iku lapolo to pawome. / oli eyu to pawome.

7. This apple is the biggest in that box.
	- lapolo iki so kahone to pawome.

8. The biggest apple in that box.
	- olu lapolo iki so kahone pawome ⚠Cannot be used as a stand-alone sentence.

9. This apple is the largest (compare range omitted)❌
	- iki lapolo to pawome. ⚠A sentence is not a sentence unless a comparison already exists in the context.
		- Example: pawome ima da Mandalin kalake tu. iki lapolo to pawome.

10. I eat apples faster than he does (the basis of comparison is an adverb)
	- lapido siwoda pawome ika okoba wa.

11. I eat apples as fast as he does.
	- lapido siwoda pawome iku okoba wa.

12. I'm the fastest eater of apples in the family.
	- lapido siwoda pawome iki peluhe wa.

13. This apple is bigger than or as big as that apple (this apple ≥ that apple, include the equal sign)
	- lapolo iku ika zo pawome to pawome.

14. This apple is not as big as that one.
	- ino lapolo iku zo pawome to pawome.

15. I like this person more than that person (about how much I like that person < this person)
	- maloha ika zo nede iko to nede wa.

16. I like this person as much as I like that person (about my degree of like, that person ≒ this person)
	- maloha iku zo nede iko to nede wa.

17. 🔰I like this person more than I think that person likes this person (degree to which that person likes this person < degree to which I like this person, can it be separated from the previous question?)
	- maloha to nede zo nede oto maloha ika sa iko to nede wa.

18. 🔰I like this person as much as that person likes this person (degree to which that person likes this person ≒ degree to which I like this person)
	- maloha to nede zo nede oto maloha iku sa iko to nede wa.

19. This apple is bigger than before (compared to the past)
	- lapolo ika mukasine to pawome. \* Sentence comparison
	- ezo mukasine to pawome. \* Numerical comparison 

20. This apple is as big as it ever was.
	- lapola iku mukasine to pawome. \* Sentence comparison
	- eyu mukasine to pawome. \* Numerical comparison

21. This apple is bigger than it looks.
	- lapolo ika muyote to pawome.

22. This apple is bigger than he imagined (if the comparison is to a verse)
	- lapolo ika ole eka kubitela sa okoba to pawome.

23. This apple is as big as he imagined it would be.
	- lapolo iku ole eka kubitela sa okoba to pawome.

24. This apple is bigger than what he was saying about that apple.
	- lapolo ika ola eka puhuta ibo zo pawome okoba to pawome.

25. This apple is just as big as the one he was talking about.
	- lapolo iku ola eka puhuta ibo zo pawome okoba to pawome.

26. I can't eat this apple now any faster than he could have eaten the apple yesterday.
	- eku ino bolida lapido siwoda to pawome ika ola eka edu siwoda pawome egu kahapo okoba wa.

27. I can't stop eating these apples, just as flowers will one day fall.
	- ino tigila siwoda to pawome iku ola eko mahano kuwola bunuge wa.

28. Eat this apple as fast as you can.
	- obi lapido siwoda to pawome iku bolido lapide.
	- obi bolidebolido lapido siwoda to pawome.

29. This apple is just a little bigger than that apple.
	- tolo lapolo ika zo pawome to pawome.
	- ezelapolo ika zo pawome to pawome.

30. This apple is much bigger than that apple.
	- lagolago lapolo ika zo pawome to pawome.
	- ezolapolo ika zo pawome to pawome.

31. This apple is 7 cm bigger than that apple.
	- 7 ㎝ lapolo ika zo pawome to pawome.
		- How to read: sepo senitometolo lapolo ika zo pawome to pawome.

32. This apple is 7 cm wider than that apple.
	- lebalo 7 ㎝ lapolo ika zo pawome to pawome.
		- How to read: lebalo sepo senitometolo lapolo ika zo pawome to pawome.

33. This apple is three times bigger than that apple.
	- 3x lapolo ika zo pawome to pawome.
		- How to read: kolumo bano lapolo ika zo pawome to pawome.

34. This apple is 20 % larger than that apple.
	- 20% lapolo ika zo pawome to pawome.
		- How to read: kakuso deko paseto lapolo ika zo pawome to pawome.

35. This apple is the fifth largest in the box.
	- eso biso lapolo ani so kahone to pawome.

36. This apple is the fifth largest in the box from the bottom (can you describe it without saying "fifth smallest"?)
	- epo eso biso lapolo ani so kahone to pawome.

37. The price of the apple is more than $3 (price ≥ $3).
	- kolukeno iku ika 3 da Dolu so pawomo bidone.
	- eze 3 da Dolu so pawomo bidone.

38. The price of that apple is over $3 (price > $3).
	- kolukeno ika 3 da Dolu so pawomo bidone.
	- ezo 3 da Dolu so pawomo bidone.

39. The price of the apple is less than $3 (price ≤ $3).
	- malato iku ika 3 da Dolu so pawomo bidone.
	- ezi 3 da Dolu so pawomo bidone.

40. The price of the apple is less than $3 (price < $3).
	- malato iku 3 da Dolu so pawomo bidone.
	- eza 3 da Dolu so pawomo bidone.

41. That apple weighs at least 2 kg.
	- minimo 2 ㎏ so pawomo belate.
		- How to read: minimo kakuso kilogulamo so pawomo belate.

42. That apple only weighs 2 kg at most.
	- siko masimo 2 ㎏ so pawomo belate.
		- How to read: siko masimo kakuso kilogulamo so pawomo belate.

43. The bigger the apple, the sweeter it is.
	- ihi ika lapolo pawome ihe ika maniso sa.

44. The bigger the apple, the less sweet it is.
	- ihi ika lapolo pawome ihe ino ika maniso sa.

45. That apple is like a jewel.
	- kohulika oyu hiyase so pawome.

46. Jewel-like apples
	- olu kohulika oyu hiyase pawome ⚠Cannot be used as a stand-alone sentence.

47. He can run as fast as an athlete.
	- bolida lapido yukusa oyu kapale okoba.

48. Someone who can run as fast as an athlete.
	- olu bolida lapido yukusa oyu kapale nede ⚠Cannot be used as a stand-alone sentence.

49. He ate that apple as if it were a gorilla.
	- eka oyu golile siwoda so pawome okoba.

50. Who eats that apple like a gorilla.
	- olu oyu golile siwoda so pawome nede ⚠Cannot be used as a stand-alone sentence.

51. He ate that apple like a gorilla eats a banana.
	- ola oyu ola siwoda banane golile siwoda so pawome okoba.

52. The apple was so sweet it made me jump.
	- eka sapato maniso isu ola oli hipata so pawome.
	- eka bidona hipata so maniso pawome.

53. Apples so sweet they make you jump.
	- olu sapato maniso isu ola oli hipata pawome ⚠Cannot be used as a stand-alone sentence.
	- olu eka bidona hipata maniso pawome ⚠Cannot be used as a stand-alone sentence.

54. I had never eaten an apple that sweet before.
	- ino eka misuta mukaso siwoda maniso pawome oyu zo wa. 

55. That was a delicious meal (said after meals).
	- mahalasiwoda.
