# 57 sentences of not eating enough apples

Quote source: [りんご文 - 人工言語学 Wiki](https://conlinguistics.wikia.org/ja/wiki/りんご文)

> Apple text are a group of example sentences created starting with the apple sentences (Zasuron list) created by Zasuron on September 20, 2014. These example sentences were created for the purpose of identifying undefined grammatical categories of the artificial language by translating them.

> 57 Sentences of Not Eating Enough Apples is an apple sentence created by Moyashi on July 8, 2015. Commonly known as the Moyashi List.

1. Hello.
	- taloha.

2. My name is XXX.
	- da XXX wa.
	- da XXX wo nomo.

3. What is this?
	- owa igo ta?

4. This is an apple.
	- pawome ta.

5. This is a big apple.
	- lapolo pawome ta.

6. Is this your apple?
	- owa yo pawome ta?

7. This is my apple.
	- wo pawome ta.

8. (pointing closer to me) This apple is small.
	- pileno to pawome.

9. (pointing closer to you) That apple is big.
	- lapolo so pawome.

10. (pointing far away from me and you) That apple is very big.
	- lago lapolo zo pawome.

11. An apple is a fruit.
	- huwate pawome.

12. Some apples are big.
	- lapolo tizo pawome.

13. In general, apples are sweet.
	- tatelo maniso pawome.

14. The wind blows.
	- makana iniba.

15. The apple falls.
	- pudota pawome.

16. The apple is in front of me.
	- uhu wa pawome.

17. I grab an apple.
	- linuga pawome wa.

18. I will give you the apple.
	- kumuha pawome anu ya wa.

19. You eat the apples I give you.
	- siwoda ole kumuha pawome wa ya.

20. (You are not included.) We (as a habit) eat apples.
	- mayuso siwoda pawome la walewale. \* Inclusion/exclusion is determined by context.
	- mayuso siwoda pawome iniwawa. \* **unofficial** first person plural excluded form

21. You eat apples (as a custom).
	- mayuso siwoda pawome yaya.

22. We (including you) eat apples (as a habit).
	- mayuso siwoda pawome wawa.

23. He (as was once his habit) ate an apple.
	- eka mayuso siwoda pawome okoba.

24. She would eat apples (habitually).
	- eko oyo mayuso siwoda pawome omoba.

25. You start to eat the apple.
	- timata siwoda pawome ya.

26. You are eating an apple.
	- edu siwoda pawome ya.

27. You finish eating the apple.
	- edo siwoda pawome ya.

28. He would start eating the apples.
	- oyo eda siwoda pawome okoba.

29. He began to eat the apple.
	- eka eda siwoda pawome okoba.

30. He's eating an apple.
	- edu siwoda pawome okoba.

31. He would finish eating the apple.
	- oyo edo siwoda pawome okoba.

32. He had finished his apple.
	- eka edo siwoda pawome okoba.

33. This apple was small.
	- eka pileno to pawome.

34. This apple is not small.
	- ino pileno to pawome.

35. This apple will get bigger.
	- oyo ika lapolo to pawome.

36. “He was eating an apple,” you say.
	- “eka edu siwoda pawome okoba.” ota puhuta ya.

37. He was eating an apple, you said.
	- eka edu siwoda pawome okoba oto eka puhuta ya.

38. He is eating an apple, you said.
	- edu siwoda pawome okoba eka puhuta ya.

39. He will eat the apple, you will say.
	- oyo siwoda pawome okoba oto oyo puhuta ya.

40. He will eat the apple, you said.
	- oyo siwoda pawome okoba oto eka puhuta ya.

41. I could potentially eat an apple.
	- oyo bolida siwoda pawome wa.

42. If I could eat an apple, I would be happy.
	- ihi bolida siwoda pawome wa ihe senano wa.

43. It was inevitable that I would not be able to eat an apple.
	- mobono ola ino bolida siwoda pawome wa.

44. I wish I could eat an apple.
	- dezila siwoda pawome wa.

45. I need to eat an apple.
	- halusa siwoda pawome wa.

46. You should eat an apple.
	- nasiha siwoda pawome anu ya wa. \* encourage
	- bona ika ola ino siwoda pawome ya ola siwoda pawome ya. \* Expression by comparative class

47. You can choose to eat the apple or not.
	- bolida balita siwoda imo ino siwoda pawome ya.

48. You should not eat the apple.
	- nasiha ino siwoda pawome anu ya wa.
	- bona ika ola siwoda pawome ya ola ino siwoda pawome ya. \* Expression by comparative class

49. Do not eat the apple.
	- obi ino siwoda pawome.

50. I can't (competently) eat an apple.
	- ino pesutiba siwoda pawome wa.

51. I eat this big and sweet apple.
	- siwoda to lapolo imo maniso pawome wa.

52. I eat this apple or that apple or both.
	- siwoda to pawome imo so pawome imo so kedulo wa.

53. He eats only one of either large or sweet apples.
	- siwoda kekaho lapolo pawome imo maniso pawome okoba.

54. If you eat that apple, then and only then will I eat this apple.
	- ihi siwoda so pawome ya imo siko so tulage ihe siwoda to pawome wa.

55. Regardless of whether he eats that apple or not, I will eat this apple.
	- hapako siwoda zo pawome okoba oto siwoda to pawome wa.

56. Unfortunately, there are no apples here.
	- kawolo, ino suta anu tu pawome. \* Emphasize that it's a shame.
	- kawolo ino suta anu tu pawome.

57. Good bye
	- toloha.
