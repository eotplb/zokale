# About 100 linguistically interesting sentences

Quote source: 『資料 多言語平行コーパスのための「言語学的におもしろい100の文」 著: 山崎直樹』

Since the item name uses the original text as it is, there are terms and concepts that do not exist in Zokale. 

⭕Correct expression 　🔺Non-standard / deprecated 　📝Descriptive notation 　💬Actual pronunciation 　*Italic*: Unofficial word

## Sentences that express “equivalence” (i.e., “A is B” type sentences)

||
|-|
|I am teacher.|
|gule wa.|

||
|-|
|My mother was a lawyer.|
|eka bogade wo omotine.|

||
|-|
|I want to be a doctor in the future.|
|eko tuwiba mika anu pahame wa.|

||
|-|
|Today is the 29th of February.|
|📝2m‐29p nugame.|
|💬kakuso mahine kakuso nado palibe nugame.|

||
|-|
|My husband is 30 years old.|
|📝30 ekabuwose wo okotane.|
|💬kakuso deko wo okotane.|

## “Descriptive sentences” (i.e., "A is -" type sentences)

||
|-|
|This bridge is old.|
|kahiko to silute.|

||
|-|
|These walls used to be blue.|
|eka bile egu mukase to kabe.|

## State sentences

||
|-|
|The new prime minister looks very much like a monkey.|
|lago milipo anu mapine bago banusopunake.|
|⚠Difficult to understand redundant expressions are prohibited in principle.|

||
|-|
|I don't get along with my neighbors.|
|ola kino seyule isu lahelo nede wa.|

## Intransitive verb sentence

||
|-|
|Bats fly at night.|
|⭕lenuta egu nokute lepake.|
|🔺lenuta egu nokute ibo lepake iniba.|

||
|-|
|The baby laughed.|
|eka keketa sagole|

## Transitive verb sentence

||
|-|
|Cats chase mice.|
|keyala nisume pusile.|

||
|-|
|The coach put up one flag.|
|eka sutala taso badele hayukogule.|

||
|-|
|The soldiers destroyed the house.|
|eka sumila so lumahe sudal.|

## Intransitive and transitive verb pairs

||
|-|
|We won.|
|eka lanakila wawa.|

||
|-|
|We beat our rival team.|
|eka lanakila isu lawano gulupe wawa.|

||
|-|
|The gates are open.|
|eka tuwela liluno gelube.|

||
|-|
|The citizens opened the city gates.|
|eka tuwela liluno gelube waloge.|

## Yes‐No question sentences (* Question sentences)

||
|-|
|Are you a spy?|
|owa bakoze ya?|

||
|-|
|Is tomorrow Sunday?|
|owa sepodolabe bukase?|

||
|-|
|Are you 20 years old?|
|📝owa 20 ekabuwose ya?|
|💬owa kakuso deko ekabuwose ya?|

||
|-|
|Is this food good?|
|owa lezata to tiwale?|

||
|-|
|Do cats chase mice?|
|owa keyala nisume pusile?|

## Wh question sentence

||
|-|
|What do giraffes eat?|
|owa siwoda igo kilahube?|

||
|-|
|Whose Bible is this?|
|owa igu do Kilisuto pito kilibe ta?|
|⚠Religion-related terms should be written as classification nouns.|

## Double subject (* Topic sentence)

Warn: free translation

||
|-|
|The noze is long about elephant.|
|⭕ola pitoko nazone gayahe.|
|🔺pitoko ibo gayahe nazone.|
|⚠The method of expressing subjects with “ibo (about)” is retained for compatibility.|

||
|-|
|This tree has large leaves and small flowers.|
|⭕ola lapolo lehe ima pileno bunuge to lakabe.|
|🔺lapolo ibo to lakabe lehe ima pileno bunuge.|

||
|-|
|I like diamonds as a gift.|
|⭕ola haluta da Daiamondo hadime.|
|🔺haluta da Daiamondo ibo hadime iniba.|

## Double object

||
|-|
|I sent him a dictionary.|
|eka laheta taso kamuse ago okoba wa.|
|⚠Use “ago” for “to” without a result (which cannot be confirmed).|

||
|-|
|I sent a dictionary that I had compiled to a schoolboy.|
|eka laheta ole eka mukata kamuse wa ago no tasomagale wa.|

||
|-|
|Everyone calls him a “coward.”|
|sanola okoba isi “duwagonede” na.|

||
|-|
|We call people from other countries “foreigners.”|
|sanola olu eka tula aga eyobanuse nede isi “banage” wawa.|

||
|-|
|The robbers took some of the money from him.|
|eka tolo kapata kane aga omoba kapatanede.|

||
|-|
|Apparently, the thief stole the knife from some store.|
|eka oyo gawola so lagopoto kalise aga igaligido mimale gawolanede.|

## Request

||
|-|
|I can't wait for Christmas to come.|
|wanaso dezila taloha da Kulisumasu iniba.|

||
|-|
|I want you to investigate this matter.|
|haluta humana to masale anu ka kimi wa.|

## Causative verb

||
|-|
|My sister forced me to eat spinach.|
|oda bozimo siwoda *da Bayamu* anu wa omogonotene.|

||
|-|
|The nurse made the student stand up.|
|eka sutala so magala tapuha.|

||
|-|
|Let's send those who want to go abroad.|
|odi menuga halutanede ano banuse iniba.|

||
|-|
|I let my child catch a cold.|
|1️⃣eka haga sipone anu soze wa.|
|2️⃣eka haga sipone isu soze iyu onotupate wa.|
|⁉I can't tell if it's 1️⃣parents passing on a cold to their child or 2️⃣parents being careless and their child catching a cold.|

||
|-|
|I had a barber cut my hair.|
|oda eka tipa malale isu makalo mimale wa.|

||
|-|
|The whole family convinced her father to admit her to the hospital.|
|buyuka okotine ise peluhe ima eka oda lumasa iniba.|

||
|-|
|The government set the revolutionaries free.|
|eka baputa lebolusonede gabete.|
|⚠If no specific number is given, the number is not important.|

||
|-|
|Clean up your room before you leave.|
|obi kulena kamale ega ola oli lomamenuga iniba.|

## Passive

||
|-|
|My donkey was hit by a truck.|
|eka odo pitika anu tulake wo yasine.|

||
|-|
|I was bitten on the hand by a dog that I own.|
|odo eka moluda mane anu edubumilo kudole wa.|
|⚠If you omit the object of the object, the subject of the action becomes the object.|

||
|-|
|My second wife left me, too.|
|eka idi pakela anu eso kakuso omotane wa.|

||
|-|
|This novel has been read by many people.|
|eka odo lukela anu moneto nede to nobela.|

||
|-|
|That building was built in the 19th century.|
|📝eka odo gusala egu 19 sigole to lalugo gedune.|
|💬eka odo gusala egu deko nado sigole to lalugo gedune.|

## Mediopassive voice

||
|-|
|This knife cuts well.|
|lago bolida tipa to lagopoto kalise.|

||
|-|
|This book sells well.|
|lago mawida to kilibe.|

## Existence

||
|-|
|There's a pen on that desk.|
|(suta) upu so hapage taso pene.|
|⚠The word "~ exists (suta)" is omitted if the word indicating existence already exists.|

||
|-|
|There are many skyscrapers on both sides of this road.|
|⭕ola suta moneto lagolago gedune to bogo kedulo puwole.|
|🔺anu to bogo kedulo puwole moneto lagolago gedune.|
|⚠The upper part is a thematic sentence, and the lower part is a Western linguistic expression (meaningful translation with the subject converted into a preposition).|

## Whereabouts

||
|-|
|Your pen is on that desk.|
|(suta) upu so hapage ko kimi pene.|
|⚠Japan-specific pronouns are expressed with unique personal identifiers.|

||
|-|
|My grandson is here.|
|anu tu wo kupunosoze.|

## Appearance

||
|-|
|A policeman came running from the other side.|
|eka yukusatula uta iniba taso polisonede.|
|⚠Multiple abstractions are prohibited in principle.|

||
|-|
|The cop who left earlier is back.|
|eka yaleno palatatula olu eki lomamenuga polisonede.|

||
|-|
|It's starting to rain.|
|(eda) sada.|
|⚠Verbs that contain an object or subject can be used as interjections.|

||
|-|
|It's snowing.|
|(edu) luma.|

||
|-|
|This house is often haunted.|
|⭕ola lago yuseno lumita henuke to lapolo lumahe.|
|🔺lago yuseno lumita anu to lapolo lumahe henuke.|
|⚠The upper part is a thematic sentence, and the lower part is a Western linguistic expression (meaningful translation with the subject converted into a preposition).|

## Disappear

||
|-|
|One of the cows escaped from the barn.|
|eka pakela aga lehumolumahe taso lehuma.|

||
|-|
|The youngest cow escaped from the bovine pen.|
|eka pakela aga lehumolumahe ikiyuno lehuma.|

## Breakdown

||
|-|
|Two of the four glasses were broken.|
|eka malepa agi nelumo sa kakuso basokupe.|

||
|-|
|My brother ate three of the five apples.|
|eka siwoda kakuso sa agi biso pawome okoyunotene.|

## Reault

||
|-|
|She beat her husband's mother to death with a hammer.|
|eka menelapatuga okotano tine isa basale omoba.|
|⚠Multiple verbs that share a common object or complement can be combined into a single verb in a compound word.|

||
|-|
|She unintentionally hit her husband's father with a frying pan and left him for dead.|
|eka mapaga menelapatuga okotano okotine isa kapete omoba.|

## Unachieved

||
|-|
|I removed (or tried to remove) a poster from the wall, but it didn't come off.|
|eka eda melepe kabo pelahe iniba inu eka ino melepe sa.|
|⁉The sentence “I peeled it off, but it didn't come off” is a contradiction in terms. The sentence “I started to peel off the poster, but most of it remained” is inconsistent. If you want to use the same action, you should use “I started to peel it off, but”. If the same action is used, “I tried to remove it, but” is correct.|

||
|-|
|I erased (or tried to erase) the writing on the blackboard, but it didn't disappear.|
|eka eda binasa sekolopowale imu letole iniba inu eka ino binasa sa.|

||
|-|
|I opened (or tried to open) the lid of the trash can, but it didn't open.|
|eka eda tuwela bopalokahone imu takipe iniba inu eka ino tuwela sa iniba.|

## Arrival point

||
|-|
|He hit the ball at the target 100 meters away.|
|📝eka wosuma piluke ubu 100 metole anu kohude okoba.|
|💬eka wosuma piluke ubu hako metole anu kohude okoba.|

||
|-|
|Take off your shoes and put them under the bed.|
|obi tala ekanugulo sebe anu udu buwode.|

||
|-|
|Let's swim to the other side of the river.|
|obu wida ago utupihe.|

||
|-|
|The library is open until 9 p.m.|
|📝tuwela eto 21:00 kilibogenude.|
|💬tuwela eto kakuso deko taso ima nulo kilibogenude.|

||
|-|
|She worked until her body was a wreck.|
|eka galuga eto ola oli malepa kehe omoba.|

## Route

||
|-|
|Camels walk in the desert.|
|kabula gulune *da Kamelu*.|

||
|-|
|The enemy must have escaped this way.|
|oyo pakela agu to bogo musuhe.|

## Starting point

||
|-|
|My daughter fell out of a tree.|
|edu pudota upa lakabe omosoze|

||
|-|
|Tomorrow, we will fly from this base to our destination.|
|bukaso lenuta aga to takogedune ago bagahonohage wawa.|

## Cause

||
|-|
|The army delayed their departure because of the snow.|
|odamohana latehe iyu ola oli eka luma hukubogulupe.|

||
|-|
|The crowd was overjoyed and jumped into the moat.|
|senana iniba ima eka selama ana basoge aga iniba buguse.|

## Experienced subject

||
|-|
|All the people in this country like beer.|
|ola yumonedo kutena *da Pia* to banuso nede.|

||
|-|
|How can a monkey understand art?|
|ola oya ino memaha tabide mapine.|

## The passage of time

||
|-|
|We've been married for 20 years.|
|📝egu 20 buwose eta nikaha wawa.|
|💬egu kakuso deko buwose eta nikaha wawa.|

||
|-|
|It's been 2,500 years since the Buddha died.|
|📝egu 2500 buwose eta da Buhuda imu alozokale iniba.|
|💬egu kakuso seko biso hako buwose eta da Buhuda imu alozokale iniba.|

## Comparison

||
|-|
|Red leaves are redder than rose blossoms.|
|welo ika *do Mawalu* bunuge ekalehewelo lehe.|

||
|-|
|The Arctic is not as cold as the Antarctic.|
|ino kilumo ika selatopole nutalopole.|

## Metaphor

||
|-|
|The falling petals look like snow.|
|kohulika oyu lume olu oli kuwola bunugowahe.|

||
|-|
|I swam through the sea like a flying bird.|
|edu lenuta linute oto eka oyu so wida ani mowane wa.|

## Quote (* Direct speech and Indirect speech)

||
|-|
|Teachers say, “Get along with your friends.”|
|“obi bonoseyula isu yusutabe.” ota sanola gule.|

||
|-|
|Some people say he's an honest man.|
|dagalo okoba oto sanola na.|

## Conviction

||
|-|
|Enthusiasts believe the earth is round.|
|wakapona ola bilogo da Mapele toliko wakaponanede.|

||
|-|
|His classmates thought he was a woman (but he wasn't).|
|eka edu wakala ola omonede ba luzoyusutabe.|
|⚠It is inappropriate in some situations and developments because the answer is given before the result is stated. It is better to use “that person” instead of “he is”. It's a good idea to use the following words.|

## Knowledge

||
|-|
|We knew that he was an alien.|
|yunibonede okoba oto eka tileta sa wawa.|
|eka tileta ola yunibonede okoba wawa.|

||
|-|
|Do you know if he's still alive?|
|owa eku edu mabuha okoba oto owa tileta sa ya?|

## Objective

||
|-|
|My father went outside to look at the moon.|
|eka loma ano iniba ito ola oli kita malame okotine.|

||
|-|
|We take medicines to cure diseases.|
|siwoda yake ito ola oli gumala sakite wawa.|

||
|-|
|The nurse opened the curtains so she could see outside.|
|eka tuwela beluhe ito ola oli bolida kita anoside tapuhadene.|

## Successive act

||
|-|
|My sister bought one TV and sent it to my grandmother.|
|salapa taso bidenemaze ima eka laheta ago omogonotine omogonotene.|

||
|-|
|My grandfather went outside to smoke a cigarette.|
|loma ano iniba ima eka sabuka okogonotine.|

## The structure of a noun phrase

||
|-|
|a friend of mine|
|wo no yusutabe|

||
|-|
|this friend of mine|
|wo to yusutabe|

||
|-|
|this small garden|
|to pileno tamane|

||
|-|
|this too small garden|
|to lago pileno tamane|

||
|-|
|this house which too small garden|
|pileno tamane imu to lumane|

## Relational clause (* Embedded text)

||
|-|
|person which flower pick|
|olu patuga bunuga nede|
|⚠pick flowers = kill flowers|

||
|-|
|net which catch a bird|
|olu napata linute sebule|

||
|-|
|place which to grow a insect|
|olo oli bumila belume anu nohage|

||
|-|
|smog which grill a fish|
|olu poluta kale pawobe|

||
|-|
|He has two sons who are studying in Paris.|
|ola suta ola edu anebelana anu da Pali kakuso okosoze okoba imu iniba.|

||
|-|
|He has two sons, all of whom are studying in Paris.|
|ola suta kakuso okosoze okoba imu iniba ima edu anebelana anu da Pali so yume.|
