# 58 sentences I want to eat an apple

Quote source: [りんご文 - 人工言語学 Wiki](https://conlinguistics.wikia.org/ja/wiki/りんご文)

> Apple text are a group of example sentences created starting with the apple sentences (Zasuron list) created by Zasuron on September 20, 2014. These example sentences were created for the purpose of identifying undefined grammatical categories of the artificial language by translating them.

1. I eat an apple.
	- siwoda pawome wa.

2. I ate an apple.
	- eka siwoda pawome wa.

3. He is eating an apple.
	- edu siwoda pawome okoba.

4. She finishes eating the apple.
	- edo siwoda pawome omoba.

5. She had finished her apple.
	- eka edo siwoda pawome omoba.

6. My wife has eaten an apple.
	- misuta eka siwoda pawome wo omotane.

7. My wife eats apples every day.
	- siwoda pawome ege palibe wo omotane.

8. Me and my wife ate an apple yesterday.
	- eka siwoda pawome egu kahapo wa ima wo omotane.

9. Me and my wife ate an apple 6 days ago.
	- eka siwoda pawome ega 6(kuloso) palibe wa ima wo omotane.

10. They will eat the apples tomorrow.
	- eko siwoda pawome egu bukaso okobaba.

11. They eat the apples after six days.
	- eko siwoda pawome ego 6(kuloso) palibe okobaba.

12. The girls have been eating apples for three days. (They eat only apples as a meal).
	- siwoda pawome etu 3(kolumo) palibe omobaba.

13. They keep eating the apple for five minutes. (The time required to eat the apple is five minutes.)
	- siwoda pawome etu 5(biso) minute omobaba. \* Eat an apple in 5 minutes.
	- siwoda pawome etu eze 5(biso) minute omobaba. \* Spend more than 5 minutes eating the apple.

14. He is always eating apples. (He is always eating apples at all times.)
	- edu selalo siwoda pawome okoba.

15. There are three apples.
	- kolumo pawome.

16. The apples are on the table.
	- upu hapage pawome.

17. Someone has eaten that apple. (As a result, that apple is not there now.)
	- eka siwoda zo pawome na.
	- eka odo siwoda iyu na zo pawome.

18. Somebody is going to eat this apple. (Guess)
	- oyo siwoda pawome na.

19. I heard that someone ate this apple, that apple, and that apple. (hearsay)
	- eka siwoda to pawome imo so pawome imo zo pawome na oto eka oyo putuha na.

20. Not a single apple.
	- ino lago taso pawome.

21. I don't eat apples.
	- ino siwoda pawome wa.

22. I can't eat apples. (I don't have any apples, so I can't eat them under the circumstances.)
	- ino bolida siwoda pawome wa iho ino sata sa.

23. I dropped the apple on the floor.
	- eka pudota pawome anu latine wa. \* Accidental
	- eka osopudota pawome anu latine wa. \* Spontaneous

24. The apple fell to the floor.
	- eka pudota anu latine pawome.

25. Apples are not edible. (The nature of apples is that they are no longer edible.)
	- ino bolida siwoda pawome iho eka ino bono sa.

26. I want to eat an apple.
	- haluta siwoda pawome wa.

27. I want to buy an apple.
	- haluta salapa pawome wa.

28. These apples are dirty.
	- malumo to pawome.

29. These apples are not beautiful.
	- ino kuleno to pawome.

30. I think I can eat that apple.
	- oyo bolida siwoda zo pawome.

31. That's not an apple.
	- ino pawome za.

32. This is not an apple, it's a mandarin orange.
	- ino pawome ta inu do Mandalin kalake (ta).

33. Do you eat mandarin oranges?
	- owa siwoda do Mandalin kalake ya?

34. Yes, I eat mandarin oranges.
	- siwoda. \* Zokare doesn't answer questions with a “yes/no” answer.

35. You don't eat mandarin oranges?
	- owa ino siwoda do Mandalin kalake ya?

36. No, I don't eat mandarin oranges.
	- ino siwoda. \* Zokare doesn't answer questions with a “yes/no” answer.

37. You wanted to eat the oranges. Isn't that right?
	- eka haluta siwoda do Mandalin kalake ya owa?

38. No, I don't want to eat the mandarin oranges.
	- ino haluta. \* Zokare doesn't answer questions with a “yes/no” answer.

39. You don't eat apples or mandarin oranges?
	- owa ino siwoda pawome ihu do Mandalin kalake ya?

40. No, I eat apples.
	- siwoda pawome wa. \* Zokare doesn't answer questions with a “yes/no” answer.

41. I don't eat oranges. Therefore, I can't eat mandarin oranges. (Passive denial)
	- ino siwoda do Mandalin kalake wa. ine ino bolida do Mandalin kalake wa.

42. You can't eat apples. Because apples are not beautiful.
	- ino bolida siwoda pawome. iho ino kuleno sa.

43. Apples are dirty. Mandarin oranges, on the other hand, are beautiful.
	- malumo pawome. ido kuleno da Mandalin kalake.

44. Let's eat mandarin oranges, shall we? (Solicitation)
	- owa obu isosido siwoda do Mandalin kalake?

45. No, I refuse.
	- ino siwoda. \* Zokare doesn't answer questions with a “yes/no” answer.

46. Eat the mandarin oranges over there. Don't eat the apples.
	- oba siwoda do Mandalin kalake anu zu. oba ino siwoda pawome.

47. Can I have an apple? You can have the mandarin oranges.
	- owa obu lawola pawome agu ya wa? howata do Mandalin kalake.

48. I said, “Don't eat the apple, eat the mandarin orange.”
	-“oba ino siwoda pawome. oba siwoda do Mandalin kalake.”ota eka sanola wa.

49. Are you asking me to eat an apple?
	- oba siwoda pawome oto owa sanola anu wa ya?

50. You said you wanted to eat an apple.
	- haluta siwoda pawome wa oto eka sanola ka kisama.

51. I don't remember saying I wanted to eat an apple.
	- ino misuta ola eka sanola ola haluta siwoda pawome wa wa.

52. I believe you said, “I want to eat an apple.”
	- “haluta siwoda pawome wa.” ota eka oyo sanola ya.

53. I might have possibly said that.
	- oyo sanola sa wa. \* Zocale forbids multiple guesses.

54. You absolutely said to me, “I want to eat an apple.”
	- “haluta siwoda pawome wa.” ota eka oya sanola anu wa ya.

55. He makes me eat an apple.
	- oda siwoda pawome anu wa okoba. \* He ordered me to do it.
	- oda siwoda pawome aga osoba ago wa okoba. \* He fed me to me.

56. The mandarin oranges have not been eaten by me.
	- ino odo siwoda anu wa do Mandalin kalake.

57. The apples were fed to me by him.
	- oda odo siwoda iyu okoba anu wa pawome.

58. There's nothing here.
	- nulo ta.
