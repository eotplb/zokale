# 1. Language Overview

Zokale (do Zokale wagone) is an artificial international auxiliary language consisting of an "international auxiliary language," a "strictly defined language," and an "ideal language."

## 1.1. Main specifications

- There are no either consonant clusters or double vowels.
	- There is no either long vowels or double geminate consonants.
- Accents for regular words and markers are fixed at the first syllable.
- The range of characters and syllables that can be used differs between normal words and classification nouns.
- There is a letter B, but no V. There is a letter L, but no R. The same is true for categorial nouns. This is also true for categorical nouns.
- There are no *be* verbs, articles, or definite articles.
	- The object is in principle the subject, otherwise it must be explicit.
- No emphasis on number. Nouns do not indicate the number of objects. They are supplemented by adjectives or numerals when necessary.
- Gender-neutral. Gender is complemented with a gender prefix.
	- In fact, you can add a gender prefix to any word except markers.
- The word order is **Verb + Object + Subject**.
- Prepositional modification. Modifiers are placed before the word they modify.
- Questions that can be answered with “yes/no” are answered with a verb (or sentence). I don't answer with “yes/no.” There is no word to begin with.
- Classification nouns are always preceded by a **classification name identifier**. Classification nouns have a fixed accent on the final syllable.
- The **unique personal identifier** can be used to expand pronouns freely. You can use “Ore (俺)”, “Warawa (妾)”, or even your own name as a pronoun.
- **Embedding markers** allow you to place sentences in places where they should not be, such as in prepositional phrases. **There are no relational phrases**.
- Whenever possible, we have eliminated expressions that depend on culture or custom. For example, expressions such as “He's as cunning as a fox,” “He's as slow as a turtle,” and “He ran away like a rabbit” cannot be used in principle. If you want to use them, you need to put a declaration of use before the actual sentence.

### 1.1.1. Basic sentence structure

**Verb + Object + Subject**.

<table style="text-align:center">
<tr><td><strong>Example</strong></td><td>siwoda</td><td>pawome</td><td>wa.</td></tr>
<tr><td><strong>Part of speech</strong></td><td>Verb</td><td>Object</td><td>Subject</td></tr>
<tr><td><strong>Literal translation</strong></td><td>eat</td><td>apple</td><td>I</td></tr>
<tr><td><strong>Translation</strong></td><td colspan="3">I eat apple. </td></tr>
</table>

If the (possessive) object is omitted, it is judged from the context, but **in principle the subject is the object**.

<table style="text-align:center">
<tr><td><strong>Example</strong></td><td>pesa</td><td>kasobe</td><td>ya.</td></tr>
<tr><td><strong>Part of speech</strong></td><td>Verb</td><td>Object</td><td>Subject</td></tr>
<tr><td><strong>Literal translation</strong></td><td>wash</td><td>face</td><td>you</td></tr>
<tr><td><strong>Translation</strong></td><td colspan="3">You wash <strong>your</strong> face. </td></tr>
</table>

For example, if there is only one person, the face to be washed will inevitably be the person who acts.

If there is a possibility that there are multiple targets, but the context makes it impossible for any one of them to be the target, then the person who acts becomes the target.

In this case, it is the person who acts. On the other hand, if there is a possibility of multiple targets, but the context does not allow for anyone to be the target, it is indefinite or unspecified.

### 1.1.2. Copula sentence

A sentence such as “A is B” is **a predicate (noun phrase) + topic (noun phrase)**.

<table style="text-align:center">
<tr><td><strong>Classification</strong></td><td colspan="2">Predicate (noun phrase)</td><td>Topic</td></tr>
<tr><td><strong>Example</strong></td><td>pileno</td><td>pawome</td><td>ta.</td></tr>
<tr><td><strong>Part of speech</strong></td><td>Modifier</td><td>Noun</td><td>Demonstrative</td></tr>
<tr><td><strong>Literal translation</strong></td><td>small</td><td>apple</td><td>this</td></tr>
<tr><td><strong>Translation</strong></td><td colspan="3">This is a small apple.</td></tr>
</table>

You can also use a modifier for a predicate.

<table style="text-align:center">
<tr><td><strong>Classification</strong></td><td colspan="2">Predicade (modifier phrase)</td><td colspan="2">Topic (noun phrase)</td></tr>
<tr><td><strong>Example</strong></td><td>lago</td><td>lapolo</td><td>zo</td><td>kudole.</td></tr>
<tr><td><strong>Part of speech</strong></td><td>Modifier</td><td>Modifier</td><td>Demonstrative (possessive)</td><td>Noun</td></tr>
<tr><td><strong>Literal translation</strong></td><td>very</td><td>big</td><td>that</td><td>dog</td></tr>
<tr><td><strong>Translation</strong></td><td colspan="4">That dog is very big. </td></tr>
</table>

### 1.1.3. Basic sentence pattern lists

The minimum structures are as follows.

|||
|-|-|
|P + T|Predicate + Topic|
|V + S|Intransive verb + Subject|
|V + C + S|Intransive verb + Complememt + Subject|
|V + O + S|Transive verb + Object + Subject|
|V + O + C + S|Transive verb + Object + Complememt + Subject|

The maximum structures are as follows.

|||
|-|-|
|V + C (+ C …) + S + C|Intransive verb + Complement (+ Complement …) + Subject + Complement|
|V + O + C (+ C …) + S + C|Transive verb + Object + Complement (+ Complement …) + Subject + Complement|

## 1.2. Word Formation Specifications

**Words** are anything other than identifiers (pronouns and demonstratives), markers, and classification nouns. It is sometimes referred to as **“ordinary word”** to distinguish it from these.

Words that frequently form function words and compound words have two‑syllables. Other words have three or more syllables.

\* Two‑syllable words other than markers may be abolished in the future or reduced to the minimum required.

### 1.2.1. A posteriori

Most of the words used are borrowed from the following languages.

As a rule, preference is given to words with no or few consonant clusters.

- Polynesian languages (Hawaiian, Maori, Samoan, etc.)
- Southeast Asian languages (Tagalog, Indonesian, etc.)
- Uralic languages (Finnish, etc.)
- Esperanto
- Japanese
- Other Western languages

Pronouns, markers, and some words are a priori.

## 1.3. Parts of speech

The vowel at the end of a word is called the **part of speech ending**.

The basic part of a word, without the part of speech ending, is called the **root**.

|Example|Root|Part of speech ending|
|:-:|:-:|:-:|
|pusile (*Cat*)|pusil|e (*Noun*)|

For words, the part of speech is determined by adding the part of speech ending to the root of the word, and the meaning based on the part of speech is used.

|Name|Part of speech ending|
|-|:-:|
|Noun|‐e|
|Verb (intransitive, transitive, and interjection)|‐a|
|Modifier (adjective and adverbs)|‐o|

### 1.3.1. Identifiers (pronouns and demonstratives)

There are no part of speech endings.

### 1.3.2. Markers (function words and prefixes)

Words are generated based on their own regularity.

They are distinct from regular words, and when we say **“regular words”, they do not include markers**.

There are some restrictions when used to form synthetic words in the next section.

### 1.3.3. Classification nouns (≒ proper nouns)

It corresponds to the proper noun of other general languages, but since “nouns that have proper names but do not point to individuals (e.g. dog breed names)” are also included in the classification nouns, they are better than general proper nouns. Also has a wider range.

There is no part of speech ending, but the identifier that represents the classified noun has a function equivalent to the part of speech ending, and can represent part of speech other than the noun.

Distinguished from ordinary words, **“words” do not include classification nouns.**

## 1.4. Compound words

You can create a new word by simply concatenating two or more words.

- lapolo (*big*) + pawome (*apple*) = lapolopawome (*huge apple*)
- bahubuso (*strongly*) + menela (*hit*) = bahubusomenela (*hard blow*)

There are no restrictions on the parts of speech of the words that are joined, as long as they make sense.

Accents are **fixed on the first syllable**, just like regular words. There can be no more than two accents.

- siwoda (*eat*) + kabela (*walk*) = siwodakabela (*walk and eat*)
- lapolo (*big*) + pileno (*small*) = lapolopileno (*various sizes*)

The basic part of a compound word, excluding the part of speech ending, is called the **stem**.

A compound word can have more than one part of speech, just like a normal word.

|Example|Stem|part of speech ending|
|:-:|:-:|:-:|
|siwodakabela (*walk and eat*)|siwodakabel|a (*verb*)|

## 1.5. Letters

Using a unique character, a syllable can be represented by **a single character.**

\* Notational meaning; some characters can be more than two bytes long on a computer.
