# Identifier

## Pronoun

Pronouns are for **all living things**. You can use pronouns for sentences that treat animals and plants as subjective (or important objects). If clearly shown, it can also be used for inanimate objects such as stones.

### Subjective

|⁠|I|You|Specific|Unspecific|Unique I|Unique you|
|-|:-:|:-:|:-:|:-:|:-:|:-:|
|**Meaning**|*I*|*you*|*the person*|*one person*|(*special*)|(*special*)|
|**Singular**|wa|ya|ba|na|la|ka|
|**Plural**|wawa|yaya|baba|nana|lala|kaka|

### Possessive

|⁠|I|You|Specific|Unspecific|Unique I|Unique you|
|-|:-:|:-:|:-:|:-:|:-:|:-:|
|**Meaning**|*my*|*your*|*that person*|*someone*|(*special*)|(*special*)|
|**Singular**|wo|yo|bo|no|lo|ko|
|**Plural**|wowo|yoyo|bobo|nono|lolo|koko|

### Identifying unique pronoun

Always place **a unique person identifier** beginning with **l-** before the unique self name and **k-** before the unique partner name.

The available syllables and pronunciations are the same as for classification nouns.

It is used to express inherent personal names such as "Boku (僕/ぼく)", "Ore (俺/おれ)", "Ware (我/われ)", "Warawa (妾/わらわ)", "Kisama (貴様/きさま)", and "Nanji (汝/なんじ/*thou*)" in Japanese.

#### Notes

- Because they are pronouns, they cannot have any specific meaning. The same applies to pronouns that are personal names. If you want to add meaning to your writing, you must explain the meaning of the unique pronoun at the beginning of the text.
- As a rule, it should only be used in conversational text (the part in quotation marks). In principle, it should only be used in conversational text (the part in quotation marks), not in ground text.

## Directive (directive identifiers)

The plural form is used only when an exact number is required. The plural form should only be used when exact numbers are required; using it in a cursory manner gives the impression of exaggeration.

For example, if there are three cats, and presence is important and **number is not important**, then simply anu tu pusile (the cats are here).

### Position: Subjective

|Directive|Short|Middle|Long|
|:-:|:-:|:-:|:-:|
|**Meaning**|*this*|*it*|*that*|
|**Singular**|ta|sa|za|
|**Plural**|tata|sasa|zaza|

### Position: Possessive

|Directive|Short|Middle|Long|
|:-:|:-:|:-:|:-:|
|**Meaning**|*this*|*it*|*that*|
|**Singular**|to|so|zo|
|**Plural**|toto|soso|zozo|

### Place: Subjective

|Directive|Short|Middle|Long|
|:-:|:-:|:-:|:-:|
|**Meaning**|*here*|*there*|*there*|
|**Singular**|tu|su|zu|
|**Plural**|tutu|susu|zuzu|

### Place: Possessive

|Directive|Short|Middle|Long|
|:-:|:-:|:-:|:-:|
|**Mean**|*there*|*there*|*there*|
|**Singular**|te|se|ze|
|**Plural**|tete|sese|zeze|
