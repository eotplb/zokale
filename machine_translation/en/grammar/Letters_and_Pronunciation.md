# 2. Letters and Pronunciation

All words are written in lowercase, except for classification nouns. Allophones are equal and there is no preference.

There is a difference in the rules between regular words and classification nouns. Classification nouns have a slightly wider range of expression.

When pronouncing, **be sure to unify to one pronunciation.** Do not use multiple differnt sounds properly.

## 2.1. Vowels

The noise is recognized to some extent.

|||||||
|:-:|:-:|:-:|:-:|:-:|:-:|
|**Capital etters**|A|E|I|O|U|
|**Small letters**|a|e|i|o|u|
|**IPA**|a / ä / ɑ|e / e̞ / ɛ|i / ɪ̈|o / o̞ / ɔ|u / ɯ̹ / ɯ|

## 2.2. Consonants

Almost no unusual noise was observed.

||||||||||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**Capital etters**|B|D|G|H|K|L|M|N|P|S|T|W|Y|Z|
|**Small letters**|b|d|g|h|k|l|m|n|p|s|t|w|y|z|
|**IPA**|b|d|g|h / x|k|l / ɾ|m|n|p|s|t|w|j|z|

|||||
|:-:|:-:|:-:|:-:|
|**Syllables**|hu|si|zi|
|**IPA**|hu / fu|si / ʃi|zi / d͡ʒi|
|**IPA**|hɯ̹ / fɯ̹|sɪ̈ / ʃɪ̈|zɪ̈ / d͡ʒɪ̈|
|**IPA**|hɯ / fɯ|||

## 2.3. Syllables

**Vowel** or **Consonant + Vowel**.

Vowel-only syllables are allowed only in the first syllable of **markers (function words and prefixes)**.

### 2.3.1. Prohibited syllables

- The syllables **wu**, **yi**, and **ye** are not allowed.
- The **w‐** and **y‐** consonants cannot be used in the final syllable of a word (other than identifiers, markers, and classification nouns).

## 2.4. Accents

Always place it on the **first syllable**.

## 2.5. Classification nouns

**Words that are only used in certain regions (e.g. money units)** are generally treated as classification nouns.

Classification nouns have their own rules, as follows.

### 2.5.1 Extended letters

These letters can be used.

|||||||
|:-:|:-:|:-:|:-:|:-:|:-:|
|**Capital letters**|C|F|J|X|N*|
|**Small letters**|c|f|j|x|n*|
|**IPA**|t͡ʃ|f|d͡ʒ|ʃ|ɴ*|

- **Only the notated pronunciation can be used**.
- Replace fu, ji, xi with hu, zi, si. **fu, ji, xi** cannot be used.
- n is for **repellent sounds**. It is **always** pronounced this way, unaffected by vowels or consonants that come before or after it.

### 2.5.2. Extended syllables

Use **vowel** and **repellent** only syllables anywhere.

**Repellents always form a syllable by themselves**. The syllable (C)Vn in common languages becomes (C)V|n, **two syllables**.

### 2.5.3. Sound Replacement

Some pronunciations will be replaced with a sound similar to it.

- Convert /ts/ to two syllables of **tu + vowel**, or just **t**.
- A **sulcus sound** such as /kj/ is converted to two syllables of **/ki/j + vowel/**.
- Convert **prompt sounds** such as *hakka* to **tu** or omit **t**.
- **Long sounds** such as *beer* are converted to **vowels**.
- Convert wu to **uu** and yi to **ii**. **Speak one syllable at a time, not as a long sound**.
- Convert ye to **ie**. **Each syllable is pronounced exactly as it should be.**

### 2.5.4. Specific accent

Always on the **final syllable**, never more than one accent.

### 2.5.5. Marker

Classification nouns must always be preceded by a **class name identifier** beginning with **d‐**. Details are given on the Nouns page.

## 2.6. Full Syllable List

Here is a list of all syllables in ordinary words.

||||||||||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|a|ba|da|ga|ha|ka|la|ma|na|pa|sa|ta|wa|ya|za|
|e|be|de|ge|he|ke|le|me|ne|pe|se|te|we|‐|ze|
|i|bi|di|gi|hi|ki|li|mi|ni|pi|si|ti|wi|‐|zi|
|o|bo|do|go|ho|ko|lo|mo|no|po|so|to|wo|yo|zo|
|u|bu|du|gu|hu|ku|lu|mu|nu|pu|su|tu|‐|yu|zu|
