# Compound word

By concatenating two or more words, markers, or prefixes together, you can create words with new meanings.

## How to create

Simply connect. It can be concatenated with words of any part of speech, but the combinations are limited to the following three ways.

- word + word (+ word …)
- marker + word (+ word …)
- marker + identifier

### note

- Classification nouns cannot be linked to any other.
- Markers can only be connected to the beginning of a word.
- Identifiers can only be concatenated after the marker and cannot contain words.
- It cannot contain more than one marker or identifier.
- **Some markers, such as tense and phase markers, are prohibited from being used in compounds because they obscure the meaning**.
- The more words you concatenate, the wider the range of interpretation, and the harder it will be for the other person to understand your intentions.
- Choose a part of speech that makes sense. Some words have different meanings depending on the part of speech, so using the wrong part of speech will not convey your intentions correctly.
- The same “modifier + noun” or “modifier + verb” **can have a different meaning in a synthetic word**. If you use the wrong accent, the other person will not be able to understand your intentions correctly.
- When creating nouns or verbs with modifiers, always join the modifier before the noun or verb.
- Omit **imu** for “of” which is not possessive.
- When linking a marker to a pronoun or an indicative, the pronoun or the indicative modifies the marker.
- You can combine the same parts of speech as long as they make sense. Noun + noun, verb + verb, and modifier + modifier can be combined.
- Compound words are also accented **only on the first syllable**, with the second root and beyond pronounced with a lower inflection than the first root.
- You can insert a hyphen (‑) if you want to make the word connection clearer. However, the hyphen must be **U+2011 NON‐BREAKING HYPHEN**.

## Examples of compound words

### Word concatenation

- kudolo (*of dog*) + tahane (*home*) = kudolotahane (*doghouse*)
- siwoda (*eat*) + kabela (*walk*) = siwodakabela (*try out the food at various restaurants*)
- lapido (*rapid*) + menulisa (*write*) = lapidomenulisa (*write in shorthand*)
- kelo (*of time*) + maze (*machine*) = kelomaze (*clock*)
- hela (*operate*) + maze (*machine*) = helamaze (*computer*)

### Marker concatenation

- oyu (*like*) + kudolo (*of dog*) = oyukudolo (*dog like*)
- so (*the*) + iyu (*reason/cause*) = iyuso (*the reason/the cause*)

## Indefinite expression

The indefinite pronoun “na” can be linked to a question marker to express an unspecified object.

|⁠|Noun|Modify noun|Modify verb|Modify negative verb|
|:-:|:-:|:-:|:-:|:-:|
|**ige**<br>*When*|igena<br>*sometime*|igeno<br>*former*|igeno<br>*any time*|‐|
|**iga**<br>*Where*|igana<br>*somewhere*|igano<br>*somewhere*|igano<br>*anywhere*|ino igano<br>*nowhere*|
|**igu**<br>*Who*|iguna<br>*someone*|iguno<br>*someone*|iguno<br>*anyone*|ino iguno<br>*noone*|
|**igo**<br>*What*|igona<br>*something*|igono<br>*something*|igono<br>*anything*|ino igono<br>*nothing*|
|**igi**<br>*Why*|‐|‐|igino<br>*some reason*|ino igino<br>*not ~ some reason*|

- (suta) ani **igeno** gunite okoba. = He is in **former** memory.
- **ino igano** (suta) anu to bahe pusile. = There are no cats **anywhere** in this city.
- **iguno** bolida inibolata zo kilibe. = **Anyone** can borrow that book.
- eka pudota anu latine **igona**. = **Something** has fallen to the floor.
- ino **igino** tuwela to pinute. = This door does**n't** open for **some reason**.

In addition to the above, various indefinite expressions can be made by combining numerical markers and indefinite pronouns.

- etu + no = etuno = one day, sometime (modify verb)
	- eko **etuno** igeyumo notema anu suboke wa. = I will pass the exam one day.

## Non pronoun and Non noun

By concatenating the unmarked **ini** with the specific pronoun ba to form **iniba**, we can **interpret** the sentence as follows

- Subject-less sentences
- Transitive verb sentences without an object
- Imperfect intransitive verb sentences without a complement

As you can see from the use of “ba”, **actors and objects are specified**. Conversely, it cannot be used in situations where it is not specified.

||||
|-|-|-|
|**Normal sentence**|doluma pusile wa.|I made the cat sleep.|
|**Make the subject non pronoun**|doluma pusile iniba.|Made the cat sleep.|
|**Make the object non pronoun**|doluma iniba wa.|❌ (Unable English translate)|
