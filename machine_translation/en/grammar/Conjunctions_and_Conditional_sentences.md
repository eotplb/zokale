# Conjunctions and Conditional sentences

Use **conjunctions** to connect sentences, phrases, and clauses.

## Equivalence conjunctions

A conjunction used when the objects to be connected are in an equal relationship.

### ima (***and***)

It is used when two or more items are mentioned. It is equivalent to ***and*** in English, but it is always followed by ima, even when listing three or more items, and commas are not allowed.

The abbreviation “a” is also allowed, but its use in conversation is discouraged.

- bumila pusile **ima** kudole **ima** linute wa. = I have a cat, a dog, and a bird.

### imo (***or***)

It is used to select one of two or more items. It is equivalent to the English word ***or***, but it is always followed by imo, even when listing three or more items, and commas are not allowed.

The abbreviation “o” is also allowed, but its use in conversation is discouraged.

- haluta bihane **imo** nike **imo** kale wa. = I want vegetables or meat or fish.

### imu (***pseudo possessive case***)

It is used to simply express “B of A”. It is equivalent to the English word of, **but in the opposite order.**

The abbreviation “u” is also allowed, but its use in conversation is discouraged.

There are two possible expressions for **imu**.

#### Genitive (B belonging to A)

- pogise **imu** pusile = cat of dark
- palite **imu** butone = button on the cloth

Some are usually represented by noun phrases. See the modifiers page.

- sekole **imu** gule = **sekolo** gule = school teacher
- tamike **imu** zaluke = **tamiko** zaluke = trace of magic

#### Nominative subject (A to B)

Replacing B with a verb results in the sentence “intransitive verb (B) + subject (A)”.

- mobile **imu** lisatolapide = acceleration of the car ➡ lisatolapida mobile. = The car accelerates.
- pabutoge **imu** yupele = balloon rise ➡ yupela pabutoge. = The balloon rises.

It is not possible to target the object of the nominative case. “Transitive verb (B) + object (A)” does not hold as a sentence.

See the sentence embedding page for the alternative expressions shown in the example.

❌zope **imu** hasoge = pursuit of truth ➡ ❌hasoga zope. = Pursue the truth.  
⭕ola oli hasoga zope = Pursuing the truth

## Subordinating conjunction

It is used to add an explanation (description sentence) to a subject (main sentence).

The main sentence and the description sentence do not have to be in one sentence, but if there is no description sentence, it cannot be used. It can be a long sentence with two or more description sentences.

**The description sentence and the explanatory sentence are independent sentences**. You can replace an object in the description sentence with an indicator such as sa in the explanatory sentence, **but you cannot omit the notation itself**. The tense is also independent.

### inu (***but***)

It is used to express contrast, contrast, or difference from the description sentence. It is equivalent to ***but*** in English.

- eka laluta tupule wa. **inu** ino tuwela pinute. = I solved the riddle. But the door would not open.
- eka laluta tupule wa **inu** ino tuwela pinute. = I have solved the riddle, but the door won't open.
- eka puhuta pusile. **inu** eka ino telukena wa. = The cat spoke. But I was not surprised.
- eka puhuta pusile **inu** eka ino telukena wa. = The cat spoke, but I wasn't surprised.

### ine (***therefore***)

It is used to describe the result of a description sentence. It is equivalent to ***therefore*** in English.

- eka malepa powalo kohate. **ine** ino kiteha sa wa. = The lithographic limestone is broken. Therefore, I cannot decipher it.
- eka malepa powalo kohate **ine** ino kiteha sa wa. = The lithographic limestone is broken, so I can't decipher it.

### iho (***because***)

It is used to explain the cause or reason of the description sentence. It is equivalent to ***because*** in English.

- ino kiteha powalo kohate wa. **iho** eka malepa sa. = I can't decipher the lithographic limestone. Because it's broken.
- ino kiteha powalo kohate wa **iho** eka malepa sa. = I can't decipher the lithographic limestone because it's broken.

### otu (***when, while***)

It is used when the main sentence is acting or being acted upon and the actor of the main sentence or a third party is explaining what the main sentence is acting or being acted upon. The sentence cannot be divided, and three or more things are connected by **otu + sentence**. It is a form of speech, equivalent to the English subordinating conjunction ***when*** and ***while***, **but in the opposite order**. 

- doluma wa **otu** kohota okob.a = When I go to bed, he wakes up.
- edu doluma wa **otu** edu belana okoba. = When I'm asleep, he is studying.
- edu doluma wa **otu** eka pakela okoba. = He escaped when I was asleep.
- owa edu doluma wa **otu** eka pakela okoba? = Did he run away when I was asleep?
- edu pelata wa **otu** eka doluma wa. = I slept when I was playing = I fell asleep
- edu siwoda wa **otu** edu lukela kilibe wa. = I read books while I eat = I read a book while I eat.
- edu siwoda pawome wa **otu** edu doluma anu kamale okoba **otu** edu pekata ane omoba. = He's asleep in his room while I'm eating an apple and she's playing outside at the same time.
- edu menuga kanale bapol **otu** eka pisada sa. = It ran aground as the ship was moving through the canal = The ship ran aground while traveling through the canal.

If you are answering a question, you may omit the description sentence.

- owa eka hala igo anu iga egu kahapo nokute ya? = Where were you last night and what were you doing?
	- **otu** eka pelata iso pusile anu tahane wa. = At that time, I was at home playing with my cat.

## Subjunctive mood 

### if ~ then ~

It is represented by one of the following. Either the conditional statement or the conditional statement can come first.

||
|-|
|**ihi** *Conditional statement* **ihe** *A statement when a condition is met*|
|**ihe** *A statement when a condition is met* **ihi** *Conditional statement*|

**Conditional statements have no ambiguous interpretation**. The conditional is determined by the literal meaning of the word. Even if you add an **ambiguous expression (oyo)** or a **negative expression (ino)**, there is always only one interpretation.

- **ihi** howata pawome ago wa ya **ihe** <u>senano wa</u>. = If you could give me an apple, I would be happy.
- **ihe** <u>menuga wa</u> **ihi** ino sada sa. = I will go, as long as it doesn't rain.

### Conditional Sentences as Responses

You can also use **ihi** (assumption) or **ihe** (suggestion) clauses by themselves, in which the other person responds to the statement.

- bolita delagona ima mika ago kanunale wa! = I will slay dragons and make a name for myself (become a celebrity)!
	- **ihi** bolita delagona ya, owa? = If you kill the dragon, right?
	- **ihe** bolita gigate wa. = Then I will defeat the Titans.

### more the better

It is expressed by combining the subjunctive and comparative classes.

- **ihi** ika belana da Ingulixu ya **ihe** ika losutabo sa. = If you study English more, it will be better = The more you study English, the better you will get.

## Hypothesis

Place **iha** at the beginning of the sentence.

- **iha** lenuta lane pusile. = Suppose a cat flies in the sky.
- **iha** bolida kayuta tamike ya. = Suppose you can do magic.

## Request and Want

If you simply want to say what you want, use the verb **haluta**.

- **haluta** (lawola)* pawome wa. = I would like to get an apple. = I want an apple.
- **haluta** siwoda pawome wa. = I wish I could eat an apple. = I would like an apple.
- **haluta** howata pawome ago wa ya. = I wish that you would give me an apple.
- **haluta** ola howata pawome ya wa. = I wish you would give me an apple.
- **haluta** (lawola) pusile wa iha bolida. = If possible, I would like to get a cat = If I could, I would want a cat.
- oba **haluta** pawome! = Give me an apple! / Give me the apple!

\* The word “want” technically needs to be combined with the verb lawola (to get), but it can be omitted if the object is simply the object of the request.

The word haluta is also called an **auxiliary verb** because it can assist the meaning of other verbs. In addition to haluta, there are several other words that can be used as auxiliary verbs.

## wish

If you want to say something that is not necessarily going to happen, use the verb **dezila**.

What the actual probability is depends on the context. Usually, we don't use different words for different probabilities.

- **dezila** sade wa. = I wish for rain. = I wish it would rain.
- **dezila** ola lenuta lane pusile wa. = I wish my cat could fly. = I wish cats could fly.
- **dezila** ola bolida lenuta lane pusile wa. = I want my cat to fly.
