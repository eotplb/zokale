# No article

There is no equivalent to the definite article (*the*) or indefinite article (*a, an*) in English.

## Unstated subject

- If the object is undefined, **the subject** becomes the object. It can be a person, an object, or an event.
- If the subject is not the object, **it is necessary to specify who (what) is the object in principle.**

Depending on the context, the object may be something other than the subject even if it is not explicitly stated. If you still can't identify the subject, the subject may be unspecified, or the subject may not be identified in the first place.

<table style="text-align:center">
<tr><td>kulena</td><td>kasobe</td><td>wa</td></tr>
<tr><td>Verb</td><td>Object</td><td>Subject</td></tr>
<tr><td>wash</td><td>(my) face</td><td>I</td></tr>
<tr><td colspan="3">I wash <strong>(my)</strong> face</td></tr>
</table>

In the above example, the object of the face is unspecified in English and other common Western languages, **but in Zokale, the subject is the object, so the object of the face is "I"**. Even if there were two or more people in the room, the object of the face would still be "I" because it is not possible to wash another person's face without explicit action.

If you use the possessive in the above example, “kulena **wo** kasobe wa” (*I wash my face*), you are emphasizing that you have washed your face.

## Take one of the objects (a lump) as an example.

We separate the words we use for people and for other things, **but the standard takes precedence over the rules for pronouns**.

### For people

Use the unspecified pronoun **na**.

- **na** = **one** person
- oko**nana** = a man

### Other than that

Use **tizo** to indicate "one" or "with".

- **tizo** pusile = **one** cat
- **tizo** puke = **one** book
- **tizo** lonage = a rumor
- **tizo** taso lakabe = a tree

## Multiple objects without a definite quantity

Use **tolo** for “a little, a few” and **belapo** for “some”.

- **tolo** siwode = **a little** food
- **belapo** dokase = **some** sweets

## When the object or quantity is not determined at all

Use directives to express them.

- **zozo** tahute = **those** stars

## Example

- haluta **tizo** pusile wa = I would like to have one cat with me
	- haluta pusile wa = I would like to have one cat
		- Because judging by common sense, you don't want more than two cats when you don't specify the number of cats.
