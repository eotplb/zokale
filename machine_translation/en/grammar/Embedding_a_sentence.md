# Embedding a sentence

Embed one sentence into another.

As explained on the Nouns page, nouns are not consecutive in the same clause or phrase. The boundary between the sentence to be embedded and the sentence to be embedded is clear, but you can put a comma at the end of the sentence to make it clearer.

The three markers other than ola are basically used to create nouns that describe a state of affairs (in Zokale this is called a state noun clause).

## ola: Verb attention markers

|||
|-|-|
|**Interpretive sequence**|Subject -> Complement -> Object -> **Verb**|

Interpret sentences by focusing on the verbs in the sentence. In other words, the same interpretation as usual.

- beda koneke kudole = Dog pulling a sled
	- **ola** beda koneke kudole = Dog pulling a sled
		- <u>**ola** beda koneke kudole</u> yukusa wa = I run like a dog pulling a sled
- pusile okoba = He is a cat
	- **ola** pusile okoba = He is a cat
		- wakala **ola** pusile okoba wa = I believe he is a cat

When used as a subject, object, or complement, it means **“to do”**.

- keketa kudole. = The dog laughs.
	- **ola** keketa kudole = The dog laughs
		- lanuko <u>**ola** keketa kudole</u>. = It's rare for a dog to laugh.
- bulama anu bahe ito siwode wa. = I live in the city to eat.
	- **ola** bulama anu bahe ito siwode wa = I live in the city to eat
		- penigo <u>**ola** bulama anu bahe ito siwode wa</u>. = It's a struggle for me to live in the city to eat.
- siwoda iniba. = I eat.
	- **ola** siwoda iniba = I eat
		- gahabo <u>**ola** siwoda iniba</u>. = Eating is fun.
- eka siwoda pawome okoba. = He ate the apple.
	- **ola** eka siwoda pawome okoba = He ate the apple
		- ino obo memaka <u>**ola** eka siwoda pawome okoba</u>. = We can't allow him to eat the apple.
- kutena pawome wa. = I like apples.
	- **ola** kutena pawome wa = I like apples
		- edu mohina <u>**ola** kutena pawome wa</u> okoba. = He knows that I like apples.
- yukusa da Melosu. = Melos is running.
	- **ola** yukusa da Melosu = Melos is running
		- eka odo kokuma ise <u>**olu** yukusa da Melosu</u> da Selinuntiusu. = Selinuntius was saved by Melos running.

#### Relationship to Dependent Sentences

Since **ola** embeds **oto** subordinate sentences directly into the text, the meaning is the same as when using **oto**.

- edu mohina **ola** <u>kutena pawome wa</u> okoba. = He knows that I like apples.
- <u>kutena pawome wa</u> **oto** edu mohina so okoba. = I like apples, and he knows it.

### Extra grammar: Topic sentence

In a sentence like "The noze is long about elephant." which consists of a subject and a body, you can use **ola** as a verb, and the embedded sentence will function as the body for **"elephants are (subject)**. Since the concept of subject does not exist in natural language, there is no need to use it. You can also use **olu** for the subject of a sentence, as described below.

**Note: Due to the use of Japanese-specific expressions (topic‐prominent language), it is not possible to accurately translate them into English. Even the translations that have been done are interpretations.**

- ola pitoko nazone **gayahe**. ≒ Noze is long about elephant.
- ola pileno dosute **pusile**. ≒ Forehead is narrow about cat.
- ola eka doluma iniba da Hanako **da Taloo**. ≒ Hanako put Tarou to sleep.

The following examples are context-sensitive. Alone, the sentence makes no sense.

- owa tilata igo ya? = What do you want to order?
	- da Unagi wo <u>balite</u>. = My choice is eel.
	- da Unagi **wo** <u>iniba</u> ❌ (Unable English translate)
- owa olikikolo zanoga siwoda igo iniba? = What should I eat to lose weight?
	- ola ino lebata <u>home</u> da Kon‐niyaku. = Konnyaku does not make people fat.
	- ola ino lebala <u>iniba</u> **da Kon‐niyaku** ❌ (Unable English translate)

## olu: Subject attention markers

|||
|-|-|
|**Interpretive sequence**|Object -> Complement -> Verb -> **Subject**|

Interpret sentences by focusing on the subject in the sentence.

- kutena pawome wa. = I like apples.
	- **olu** kutena pawome <u>wa</u> = I which like apples
		- maloha <u>**olu** kutena pawome wa</u> okoba. = He loves me who likes apples.
		- eka mawida anu mimale <u>**olu** kutena pawome wa.</u> = Being a lover of apples, I bought some at the store.
- keketa kudoke. = The dog laughs.
	- **olu** keketa kudoke = Laughing dog
		- lanuko <u>**olu** keketa kudole</u>. = Laughing dog is rare.
- eka alozokala iyu sakite kudole. = The dog died of illness.
	- **olu** eka alozokala iyu sakite kudole = The dog which died of illness
		- eku dilamo alozokala anu kubulonohage upu bakile <u>**olu** eka alozokala iyu sakite kudole</u>. = The dog who died of illness is now sleeping quietly in a hilltop graveyard.
- eka maluka iyu sade palite. = My clothes got wet from the rain.
	- **olu** eka maluka iyu sade palite = My clothes which got wet from the rain
		- eka yaposa anu okoba iso <u>**olu** eka maluka iyu sade palite</u> wa. = I hugged him in rain‐wet clothes.
- eka pudota anu lubane pusile. = The cat fell into the hole.
	- **olu** eka pudota anu lubane pusile = The cat which fell into the hole
		- edu odo haluta kokuma aga <u>**olu** eka pudota anu lubane pusile</u> omoba. = She is being asked for help by a cat that has fallen into a hole.
- lipuga upu gulate penegine. = Penguins sliding on ice.
	- **olu** lipuga upu gulate penegine = Penguins which sliding on ice
		- <u>**olu** lipuga upu gulate penegine</u> eka lipugapudota upu lumo libise wa. = I slid down a snowy slope like a penguin gliding on ice.

If the embedding target includes speech (ota, oto, otu), **only the main sentence will be the target of the interpretation order**.

- edu siwoda lote kudole otu **menala kudole**. = **The dog dances** while eating bread.
	- olu edu siwoda lote kudole otu **menala kudole** = **A dog dancing** while eating bread
		- eka kita <u>olu edu siwoda lote kudole otu **menala kudole**</u> anu paluke wa. = I saw <u>**a dog dancing** while eating bread</u> in the park.

## ole: Object attention markers

|||
|-|-|
|**Interpretive sequence**|Subject -> Complement -> Verb -> **Object**|

Interpret sentences by focusing on the object in the sentence.

- kutena pawome wa. = I like apples.
	- **ole** kutena <u>pawome</u> wa = Apples which I like
		- mawida <u>**ole** kutena pawome wa</u> okoba. = He buys my favorite apple.
		- eka ino suta anu mimale <u>**ole** kutena pawome wa</u>. = My favorite apple wasn't in the store.
- pulega loto mimale anu sekole imu napule wa. = I run a bakery next to the school.
	- **ole** pulega loto mimale anu sekole imu napule wa = A bakery which I run next to the school
		- yuseno salapa sokolo lote anu <u>**ole** pulega loto mimale anu sekole imu napule wa</u> omoba. = She often buys chocolate bread at the bakery I run next to school.
		- kabulana isu magale <u>**ole** pulega loto mimale anu sekole imu napule wa</u>. = The bakery I run next to the school is popular with students.

## olo: Complement attention markers

|||
|-|-|
|**Interpretive sequence**|Subject -> Object -> Verb -> **Complement**|

Interpret sentences by focusing on the complements (prepositions) in the sentence.

- belana da Ingulixu anu sekole wa. = I study English in school.
	- **olo** belana da Ingulixu anu sekole wa = School which I study English
		- eka siwoda pawome anu <u>**olo** belana da Ingulixu anu sekole wa</u> okoba. = He ate an apple at the school where I study English.
		- kahiko iki to bahe <u>**olo** belana da Ingulixu anu sekole wa</u>. = The school where I study English is the oldest in the city.
- salina zamupa tamike anu latihanohage wawa. = We will be shooting magic at each other on the practice field.
	- **olo** salina zamupa tamike anu latihanohage wawa = Practice field which we will be shooting magic at each other
		- eka helitapudota kalihobome ago <u>**olo** salina zamupa tamike anu latihanohage wawa</u> bomapesawate. = The bomber dropped a nuclear bomb at the practice ground where we shoot magic.
		- kaba abeside ise muyule <u>**olo** salina zamupa tamike anu latihanohage wawa</u>. = The practice area where we shoot magic is protected by barriers.

## oli: No subject marker

Embeds a subject-less sentence by placing it **immediately after** other attention markers.

|Marker|Interpretive sequence|
|:-:|-|
|ola **oli**|Complement -> Object -> **Verb**|
|ole **oli**|Complement -> Verb -> **Object**|
|olo **oli**|Object -> Verb -> **Complement**|

- eka helita nisume anu <u>ole **oli** doluma pusile anu kamale</u> wa. = I threw a mouse at a cat sleeping in the room.
- eka menuga ago kamale ito <u>ola **oli** doluma pusile</u> wa. = I went to the room to put the cat to sleep.
