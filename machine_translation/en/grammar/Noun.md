# Noun

## Overview

A noun is the name of a thing or thing.

### Word configuration

It is expressed by adding the part of speech ending **‐e**.

- siwod**e** = meal
- pusil**e** = cat

### About noun phrase

It represents the sequence **(Modifier + (Modifier...)) + Noun**. Modifiers include markers.

### Sectioning of clauses and phrases

In Zokale, **nouns (including pronouns) are never consecutive in a clause or phrase**.

If there is a sequence of nouns, they belong to different clauses/phrases (they are separated by a phrase).

- oyu golile siwoda <u>pawome</u> **pusile**. = **The cat** eats <u>the apple</u> like a gorilla.
- siwoda pawome oyu <u>golile</u> **pusile**. = **Cats** eat apples like <u>gorillas</u>.
- siwoda <u>pawome</u> **pusile** oyu golile. = **Cat** eats <u>apple</u> like a gorilla.

\* The word **oyu** used in the above example functions as both a preposition (*like ~*) and a modifier (*like ~*).

In addition to this, clauses are separated by the following conditions:

- No verb comes after the noun.
- There are no modifiers after the noun.
- A marker is placed (the above pusile oyu golile also consists of two clauses, "like a gorilla" and "cat").

You may not be able to understand clause separation right away, but once you learn to understand the parts of speech of words, it will come naturally.

## Plural Expressions

With the exception of pronouns, there is no such thing as a marker or suffix to indicate plurality.

- anu tu pawome. = There are (more than one or two) apples here.
- anu kamale pusile. = (more than one or two) cats are in the room.

If you need to be explicit, you can use numerals or modifiers, but **usually do not focus on singular or plural**.

- **kolumo** pawome = **three** apples
- **belapo** pusile = **several** cats
- **moneto** kudole = **a lot of** dogs

## Gender expression

No distinction is made between men and women. Use a prefix if necessary.

|⁠|man|woman|
|-|:-:|:-:|
|**Prefix**|oko‐|omo‐|

It can be combined with any word other than marker, **but depending on how it is used, it may lead to discriminatory expressions or may not convey the intention of the statement correctly.**

|Pronoun|ba|na|
|:-:|:-:|:-:|
|**oko‐**|he|a man|
|**omo‐**|she|a woman|

|⁠person|Unexpected|Man|Woman|
|-|:-:|:-:|:-:|
|**Gender**|nede|okonede|omonede|
|**Sex**|home|okohome|omohome|

## Classification Name Identifiers

In Zokale, proper names and nouns that describe the type of each field, such as “Yorkshire terrier” or “Doberman” for dogs, are called **classification nouns**.

Classification nouns are always preceded by an identifier beginning with **d-**.

- Accents are **fixed on the final syllable**. The accent is **fixed on the final syllable**, so the accent may change from the original word.
- Capitalize all words in categorial nouns; the same applies to categorial nouns that span two or more words.
- Verbification is limited to intransitive verbs. **It is not possible to use it as a transitive verb**. If you want to include an object, use the summary marker ibo (about).
- **Syllables, pronunciations, and letters that cannot be expressed by Zokare cannot be included as part of the name**. You cannot include syllables, pronunciations, or letters that cannot be expressed in Zokare as part of the name.
	- Symbols and pictographs cannot be included in names. If you use a symbol, it will do what it is supposed to do. If you want to use a symbol or pictogram, use it only for its original function.
- If you want to pronounce the proper name with an accurate accent, or if you want to use symbols or pictograms, use the **word origin identifier** described later.

|⁠|Normal|Possessive|Intransivinize|
|-|:-:|:-:|:-:|
|**Singular**|da|do|du|
|**Plural**|dada|dodo|‐|

### Expressions of river, mountain, and place names

||
|-|
|Modifier + Classification name identifiers + Unique name|

- Mount Fuji = maluno da Fuzi
- Hudson river = yoko da Hadoson
- Aegean sea = mowano da Eege
- New York city = baho da Niyuyooku *or* baho da Niyu‐Yooku

### Expression of a person's name

The first and last name should be written in all capital letters, and only the first letter of the first name should be capitalized. Whichever letter is written first, follow the officially registered notation. The same applies when the word origin identifier described later is used instead of the unique classification identifiers.

- YAMADA Hanako (山田花子) = da YAMADA Hanako
- Lake Speed = da Leiku SUPIIDO

If there is a possibility of confusion in notation, it is acceptable to use either notation or pronunciation. **However, this is only acceptable if the person (or relative) does not mind.**

- SUZUKI Ichirou (鈴木**一郎**) = da SUZUKI **Iciloo** *or* da SUZUKI **Icilou**
- Dale Earnhardt = da **Deilu** AANHAATO *or* da **Deelu** AANHAATO

Generation numbers should be written using Arabic numerals, **without spaces** after the first or last name.

-  Napoleon Ⅲ = Napoleon3

### Expression of a title

If it is composed of multiple words, write the first letter in all capital letters. All accents are the final syllables.

- I read ⁠*Man Who Climbed the Tower*⁠ on the roof of the tower.
	- eka lukela da **Olu Eka Yupela Tolune Okohome** anu tolune imu watape wa.

In front of the proper name included in the title, write the classification name identifier in **lowercase letters**.

- I read ⁠*Tolune and Kelumine* ⁠ in front of the mirror of the tower.
	- eka lukela da **<u>da</u> Tolune Ima <u>da</u> Kelumine** uhu tolune imu kelumine wa.

These are the same when arranging two or more titles with conjunctions (*ima* or *imo*). When pronouncing, stop once **before and after** the conjunction that connects the titles.

- *Red and Blue* and *Yellow* = da Wele Ima Bile ima da Kowale
- *Mr. Suzuki and Mr. Tanaka* or *Mr. Takahashi* = da da SUZUKI Imo da TANAKA imo da da TAKAHASI

### Expression of onomatopoeia

Write the first and last letters in uppercase. If you want to stretch the sound, write additional vowels in **even units**. But when you pronounce it, **do not stretch it**.

- Cry of a cat = da MiyA
- Cry of a monster = da KixaaA

### Make up for the missing words

||
|-|
|Modifier that indicates the type of word that is missing **da** Proper name|

- Kiritampo (きりたんぽ) = **siwodo** da Kilitanpo = Kiritampo of food
- Okoshi (おこし) = **dokaso** da Okosi = Okoshi of candy
- Yacón = **siwodomezo** da Yaakon = Yacón of ingredient

The modifier that precedes the unique classification identifiers is to supplement what it is. It is not required, but it can help the understanding of a third party. You can write it once in the same document at the beginning, and then omit the modifiers as long as they are not duplicated.

## Word Origin Identifier

|⁠|Normal|Possessive|
|-|:-:|:-:|
|**Singular**|ga|go|
|**Plural**|gaga|gogo|

This is **always placed** in front of a word to indicate the origin or original sound of the word when it is difficult to translate or transliterate. It can also be used to explain the etymology or original sound of a word.

||
|-|
|Word origin identifier\[:Use letter\]\[:Country code\] word|

- ga:840 fall = autumn of American English
- ga:156 信息 = information of Chinese
- ga:156 土豆 = potato of Chinese
- ga:158 馬鈴薯 = potato of Taiwanese Chinese
- ga:643 Зо́калэ = Zokale of Russian

### Note

- If possible, use modifiers and classification nouns as much as possible.
- To help the understanding of third parties, the country code (see [the dictionary of classification nouns in Japanese](../../../ja/dictionary/分類名詞辞書.md#国名%20(206))) is included as much as possible. Pronunciation is separated by digit. The colon is not pronounced.
- If it is necessary to use different characters such as those used in the past, write them together with the target characters. Normally not needed. Pronunciation is in the target language. The colon is not pronounced.
- When using **“g-”**, **it is essential to explain what the following word means.** Unless it's a word that everyone in the world knows, you can't use g- without an explanation.
- It is supposed to be used in unavoidable situations, **so if you abuse it, your intentions will not be conveyed to the other person correctly.**
