# Verb

## Overview

Verbs express the action of the agent (=subject).

### Word configuration

It is expressed by adding the part of speech ending **‐a**.

- siwoda = eat
	- **siwoda** pawome wa. = I **eat** an apple.
- doluma = sleep
	- **doluma** anu kamale wa. = I **sleep** in the room.

### About verb phrase

It is a sentence minus the subject. The modifiers include markers.

||
|-|
|**(modifier + (modifier …)) + (Verb) + (modifier + (modifier …)) + Verb + (Object) + (complement + (complement))**|

Usually there is only one verb, but when there are two verbs in a row, as in “I **wish** to **eat**” the sequence is as shown above. This will be covered in the Conditional Sentences page.

## Derived from a variety of words

The meaning of the verb changes depending on the word from which it is derived.

### Derived from the word for state

To be in a state (to make a state).

|Derived from|Meaning|⁠|Verb|Meaning of intransitive verb|Meaning of transitive verb|
|:-:|-|-|:-:|-|-|
|welo|*red*||wela|*turn red*|*blush, dye red*|
|lapolo|*large, big*||lapola|*grow up*|*make it big*|
|mehano|*warm*||mehana|*get warmer, warm up*|*warm*|

### Derived from the word for name

When derived from a word that describes an instrument, tool, or device, it means to use it for its original purpose.

However, if there are **more than two uses**, it cannot be made into a verb.

In some cases, the verb becomes **a transitive intransitive verb** with an object.

|Derived from|Verb|
|:-:|:-:|
|puwele<br>*syringe*|puwela<br>*inject*|
|piyane<br>*piano*|piyana<br>*play the piano, play on the piano*|
|kalusone<br>*underwear*|kalusona<br>*put on underwear*|

## Transitive and intransitive verbs

A verb is transitive if it is accompanied by <u>an object</u>, and intransitive if it is not.

There are two types of intransitive verbs: those that do not require a complement (prepositional phrase) (full intransitive verbs) and those that do (imperfect intransitive verbs).

The meaning of some words changes between the two, and some words may have only one equivalent meaning.

- siwoda = dine, eat
	- siwoda iso omoba wa. = I will **dine** with her.
	- siwoda <u>pawome</u> wa. = I **eat** an <u>apple</u>.
- mehana = get warmer, warm up
	- mehana isa notime wa. = I will **warm up** by the fire.
	- mehana <u>siwode</u> wa. = I will **warm up** <u>the meal</u>.
- pudota = fall, drop
	- pudota ago yoke wa. = I **fall** into the river.
	- pudota <u>okoba</u> ago yoke wa. = I will **drop** <u>him</u> in the river.
- poluta = burn
	- poluta tahane. = The house will **burn**.
	- poluta <u>tahane</u> wa. = I will **burn** <u>the house</u> down.
- bolita = succumb, lost, defeat
	- eka bolita anu okoba wa. = I **lost** to him.
	- eka bolita <u>okoba</u> wa. = I **defeated** <u>him</u> = I defeated him, and I won.
		- eka lanakila anu okoba wa. = I won him. (\* This one is recommended if you want to avoid confusion.)

## Causative sentence and Imperative sentences

These are used to order someone to do something. The difference between the two is whether they are “explained in a sentence” or “an actual statement.”

### Causative sentence

The following markers are used depending on the degree of coercion.

|Lv|Marker|compulsory degree|Refutation/Rejection|Remarks|
|:-:|:-:|-|:-:|-|
|4|**oda**|make *n* do|×|Force them to do it. Do not allow any objection or refusal.|
|3|**odi**|do that for me|△|A general order or contractual request. Cannot be refuted or denied without a valid reason.|
|2|**odu**|Ask for it|○|Requests and solicitations.|
|1|**ode**|I will let you|○|Free will, permission, let it be free. to do or not to do.|
|0|**odo**|be forced to (passive)|(special)|**A passive sentence** when used alone. When combined with Lv 2-4, it indicates being commanded.|

- <u>**oda** eka belana</u> anu okoba wa. = I made him study.
- <u>**oda** eka belana</u> isa okoba anu okoba wa. = I used him to get her to study = I commanded him to let her study.
- <u>**odi** eka belana</u> da Ingulixu anu omoba wa. = I had her study English for me.
- <u>**odu** eka siwoda</u> pawome iso ya wa. = I asked him to eat the apple with you.
- <u>**ode** eka pelata</u> kudole anu pakule wa. = I let the dog play freely in the park.
- <u>**oda odo** eka belana</u> anu okoba wa. = He's made me learn.

#### Passive sentence

We use **odo** by itself to indicate that the actor is affected by something.

It is not necessary to consider the object that influences the actor as the *cause* **(iyu)** or the *origin* **(aga)**. It can be, but the emphasis is on both.

- <u>**odo** eka siwoda</u> anu wa pawome. = The apple was eaten by me.
- odo eka siwoda iyu wa pawome. = The apple was eaten because of me.
- odo eka siwoda iyu wa anu delagone pawome. = The apple was eaten by the dragon because of me.

### Imperative sentences

To give a command, place an imperative marker before the verb phrase and **omit the subject**. Place an *exclamation mark* **(!)** or *question mark* **(?)** at the end of a sentence is also acceptable.

As a general rule, the target of the imperative sentence is fixed to **an unspecified organism (or its equivalent or existence)**. The context will determine who the target is. If you are addressing someone in particular, you should begin the sentence with a categorical noun or the unique first person, followed by a comma. The pronunciation is also separated by a comma.

The following markers are used depending on the degree of force.

|Lv|Marker|compulsory degree|Refutation/Rejection|Remarks|
|:-:|:-:|-|:-:|-|
|4|**oba**|do|×|Force them to do it. Do not allow any objection or refusal.|
|3|**obi**|do it|△|A general order or contractual request. Cannot be refuted or denied without a valid reason.|
|2|**obu**|can do you|○|Requests and solicitations. The tone of voice is determined by the context. Adding **owa** makes the expression explicitly polite.|
|1|**obe**|I will do for you|○|Letting go, allowing.|
|0|**obo**|be forced to (passive)|(special)|Combine with Lv 2-4 to make passive voice statements.|

- **oba** lapido siwoda! = Hurry up and eat!
	- da Takasi, **oba** lapido siwoda! = Takashi, hurry up and eat!
	- la kisama, **oba** lapido siwoda! ≒ You hurry up and eat!
- **obi** ino yukusa anu kabitabe! = No running in the hallway!
- **obi** mabusuta okoba. = Help him.
- **obu** siwoda pawome iso! = Eat an apple with me!
- **obu** siwoda pawome iso? = Will you eat the apple with me? / Will you eat an apple with me?
	- owa **obu** siwoda pawome iso? ≒ 
Will you eat the apple with me? / Will you eat the apple with me?
- **obu** kokuma! = Help!
- **obe** pelata kudole anu pakle. = You can let your dog play in the park all you want.
- **obe** piyata anu ya? ≒ I could give you a massage. What do you say?
- **oba obo** siwoda anu gigate! = I am being eaten by a giant!

\* ≒: It cannot be accurately translated into English because it uses expressions specific to Japanese.

## Negation

To negate an action, place **ino** before the verb phrase.

- **ino** siwoda pawome wa. = I **don't** eat an apple.
- oba **ino** doluma! = **Don't** sleep!
- oba **ino** eko bukaso hidaso siwoda! = **Don't** eat leisurely tomorrow!

The addition of negation can change the meaning of a sentence.

- eka pudota ago lubane <u>iyu wa</u> okoba. = He fell down a hole <u>because of me</u>.
	- **ino** eka pudota ago lubane <u>iyu wa</u> okoba. = He didn't fall down the hole <u>thanks to me</u>.

There is no specific order of precedence for other markers. If you cannot decide where to place a marker due to the complexity of modifiers and other verbs, place it just before the word you want to negate. You can also place it between the modifier and the verb.

|I am|capable of sleeping|capable of not sleeping|
|-|-|-|
|**can**|bolida doluma wa|bolida **ino** doluma wa|
|**can't**|**ino** bolida doluma wa|**ino** bolida **ino** doluma wa|

If there are three or more negations in a row, a noun clause may be used. For more information, please refer to the speech patterns page.

## Connecting verb phrases

Use **ima** to connect them. They must be arranged in chronological order, from left to right.

Except for auxiliary verbs, you must use the conjunction **otu** to perform two or more actions at the same time. For more information on auxiliary verbs and **otu**, see the page on conjunctions and conditional sentences.

- doluma anu tahane **ima** belana anu sukole **ima** pelata anu tohe wa. = I sleep at home, study at school, play in the town.

## Greetings

There are two types of greetings: normal greetings that can be used by anyone, and specific greetings that have a limited use.

In writing, they can only be used in statements (the part in quotation marks).

### Normal greetings

Unlike normal sentences, it is expressed with **zero or more adverbs + verbs**. It is technically a kind of **interjection**, but in grammar it is treated as a verb.

- taloha. = Hello.
- pitko taloha. = It's been a while.
- toloha. = Good bye.
- pitoko toloha. = See you one day.

### Unique greetings

If you are a close friend, you can use a unique greeting. It can be used like a password. You can also force the use of greetings from other languages (with some grammatical restrictions).

In this case, a categorical noun is just a string of letters and has no meaning other than a greeting. However, depending on what you say, it may be misunderstood as a name calling (see Imperative sentences), so it is limited to close relationships.

Don't forget that **the accent of classification nouns is fixed on the final syllable**. Be careful not to accidentally pronounce them with the accent of the original language. Also, if a greeting consists of two or more words, this restriction may make it difficult to pronounce. In this case, combine the words into one and pronounce it.

- da Halohaloo. = HelloHello. (Unique greetings using classification nouns)
- da Kon‐niciwa. = Kon‐nichiwa. (こんにちは) (Specific greetings using Japanese greetings as classification noun)
- da Saluton. = Saluton. (Unique greetings using Esperanto greetings as categorical noun)
- da xoho. = co'o. (Unique greetings using Lojban farewell address as a categorical noun)

### Question interjection

You can use a **question + verb + question mark** as an **interjection** when asking a casual question in a close relationship. If you use it with someone else, it will be taken as a light-hearted remark and you should avoid using it.

As a prerequisite, the person asking the question must know the subject or topic you are referring to. If you don't have any food by your side and you say “owasiwoda? (*Do you want to eat?*)”, the other person will ask “owa siwoda igo wa? (*What am I eating?*).”

|Topic|Example|Meaning|
|-|-|-|
|Health|owasehata?|How are you?|
|Meal|owasiwoda?|Do you want to eat/dring?|
|Move|owamenuga?|Do you want to go to?|
|Move|owamaguba?|Do you want to return?|
|Sleep|owadoluma?|Do you want to sleeping?|
|Solicitation|igapelata?|Where to play?|
|Choice|igobalita?|What would you choose?|
|Choice|igohala?|What action will you take?|

#### Example of interjection

||||
|-|-|-|
|**okonede**|edu gahabo pelata geme okoba.|He looks like he's having fun playing the game.|
|**omonede**|nedosiluma okoba omoba.|She's staring at him.|
|**okonede**|eka habata omobo nedosilume okoba.|He noticed the way she was looking at him.|
|**okonede**|**“owapelata?”**|**“Want to play?”**|
|**omonede**|“pelata!”|“I want to play!”|
