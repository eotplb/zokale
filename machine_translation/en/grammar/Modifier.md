# Modifier

## Overview

They modify the nouns, verbs, and other modifiers that follow.

In other common languages, they are called adjectives when they modify nouns, and adverbs when they modify verbs. For convenience, these names are sometimes used in this explanation.

In addition to the following explanations, there are other ways of modifying words using compound words. For details, please refer to the page on compound words.

### Word configuration

It is expressed by adding the part of speech ending **‐o**.

- lapolo = big, large
	- **lapolo** okonede = **large** man
- pileno = small
	- **pileno** pusile = **small** cat

## Modifies a noun

- kekete = laugh
	- **keketo** nugale = **laughter's** essence
- dolume = sleep
	- **dolumo** zokale = **sleeping** world
- line = steel
	- **lino** hapage = **steel** desk
- sekole = school
	- **sekolo** gule = **school** teacher

## Modifies a verb

- lapido = rapid, hurry
	- **lapido** kabela = walk **fast**
- lapolo = widely, greatly
	- **lapolo** baluta = avoid **greatly**

## Modify other modifiers

- lago = very, extremely
	- **lago** lapolo kudole = **very** big dog
	- **lago** sukalo hesile = **extremely** difficult question

## Verbal modifier

Some modifiers can be used like verbs.

- **lapolo** ta pusile. = The cat is **big**.
- **lago kahiko** za helamaze. = That computer is **very old**.

## Proximate expressions (metaphors)

By placing the inferential marker **oyu** in front of a modifier, you can express “like (something).”

Some modifiers contain approximations from the beginning. In such cases, **oyu** is not necessary.

Approximations are used in place of verbs, **so they cannot be accompanied by verbs**. When accompanied by a verb, use **a metaphor** (see Prepositions).

- pusile = cat
	- **oyu** pusilo = **something like** a cat
		- edu lenuta lane oyu pusilo. = Something that looks like a cat is flying in the sky.
- penitale = pencil
	- **oyu** penitalo = **something like** a pencil
		- oyu penitalo ta. = This is like a pencil.
- wida = swim
	- **oyu** wido = **something like** swimming
		- oyu wido ani mapele sa. = It is like swimming in the ground.
- pileno huta = shout in a whisper
	- **oyu** pileno huto = **like** a whispered cry
		- oyu pileno huto za. = That was like whispering and shouting.

## Modifying a whole sentence

Place the modifier and comma at the beginning of the sentence, followed by the body of the sentence. Break the pronunciation once at the comma.

- **kawolo,** bolida siwoda pawome wa. = **Unfortunary,** I can't eat an apple.

## Note

Modifiers and the words they modify must always be paired.

|||
|-|-|
|**welo** lumahe ima mobile|A **red** house and a car (of an unknown color)|
|**welo** lumahe ima **welo** mobile|**Red** house and **red** car|
|**lapido** kulena ima siwoda|**Hastily** clean and (at an unknown speed) eat|
|**lapido** kulena ima **lapido** siwoda|**Hurry** to clean, **Hurry** to eat|
|**oyu** pusilo ima kudolo|**Something like** a cat and a dog|
|**oyu** pusilo ima **oyu** kudolo|**Something like** a cat and **something like** a dog|
