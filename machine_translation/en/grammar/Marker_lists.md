# Marker lists

- This list is tentative and fluid. It may increase, decrease, or be merged in the future.
- Markers whose functions and grammars have not yet been explained are also included.
- Please refer to each page for the details of the explained markers.

## Place

### Move

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**aga**|Preposition|from|Starting point, beginning point, starting target. Organisms are also targets.|
|**agi**|Preposition|of, from|Used to select from multiple targets.|
|**agu**|Preposition|through|Also used in the process of things.|
|**ago**|Preposition|to|End point, final destination, final goal. Without consequences.|
|**ana**|Preposition|into||
|**ani**|Preposition, Modifier|in|Can modify verbs when no complement is needed.|
|**anu**|Preposition|to, at|The mere capture of an object or place. With consequences.|
|**ane**|Preposition, Modifier|outside|Can modify verbs when no complement is needed.|
|**ano**|Preposition|out of||
|**alu**|Preposition|along<br>line up|Assume a road or river.<br>Aligned to a specific area.|
|**alo**|Preposition|beyond, over|Mountains and obstacles.|

### Around

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**abi**|Preposition|surrounded by|Short range|
|**abu**|Preposition|between|Connect two or more objects with “ima”|
|**abe**|Preposition|around|Short range. Can be used in combination with other location markers.|

## Numeric

### Time and Date

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**eta**|Preposition|from|It is also possible to explain the situation other than the date and time.|
|**etu**|Preposition<br>Question|spend times, spend days<br>Question: *How long* (old)|Require (required) days or times<br>Quantity, price, period, distance, etc.|
|**eto**|Preposition|until|Explanation of the situation is possible in addition to the date and time.|
|**ega**|Preposition<br>Subordinating conjunction|before|Determine if the sentence comes after the marker.|
|**egi**|Preposition<br>Modifier|before and after, around<br>mostly|Approximate time or date<br>mostly|
|**egu**|Preposition|at|A time or date at a single point. Includes relative values such as “tomorrow, yesterday,” etc.|
|**ege**|Preposition|every time|Fixed interval. Can be used with “egi”|
|**ego**|Preposition<br>Subordinating conjunction|after|Determine if the sentence comes after the marker.|
|**esa**|Preposition|per|Based on time, period, and unit.|
|**ewa**|Question|Question: *How long*|Quantity, price, period, distance, etc.|

### Tense

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**eka**|Modifier|past|The target is **verbs and copula sentence predicates only**. Same as below|
|**eki**|Modifier|a little while go||
|**eku**|Modifier|now||
|**eke**|Modifier|soon||
|**eko**|Modifier|future||

### Aspect

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**eda**|Modifier|after the start|The target is **verbs and copula sentence predicates only**. Same as below|
|**edi**|Modifier|before the start||
|**edu**|Modifier|in progress||
|**ede**|Modifier|before the finish||
|**edo**|Modifier|after the finish||

### General numeric

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**epa**|Preposition|from upper||
|**epu**|Preposition|to||
|**epo**|Preposition|from lower||

### Calculation

|⁠|Part of speech|Function|Supplementary|Remarks|
|:-:|-|-|:-:|-|
|**eza**|Preposition, Modifier|less than|<|When used as a prefix, it indicates two steps smaller than “ezu.”|
|**ezi**|Preposition, Modifier|below|≤|When used as a prefix, it indicates one steps smaller than “ezu.”|
|**ezu**|Preposition, Modifier|equal|＝|When used as a prefix, the size is expressed in ±2 steps based on “ezu.”|
|**eze**|Preposition, Modifier|above|≥|When used as a prefix, it indicates one steps greater than “ezu.”|
|**ezo**|Preposition, Modifier|greater than|>|When used as a prefix, it indicates two steps greater than “ezu.”|
|**eya**|Preposition, Modifier|other than|<>, !=||
|**eyu**|Preposition, Modifier|near equal|≒|It is also used to mean "equivalent" and "to the extent that".|
|**eyo**|Preposition, Modifier|different|≠||

### Other

|⁠|Classification|Function|Remarks|
|:-:|-|-|-|
|**eso**|ordinal|*n* th|Example: eso taso *first*|
|**ela**|calculation|fraction|ela (*Integer* ima) *Numerator* mahela *Denominator*|
|**eli**|calculation|exponentiation|eli *Numeric* imu *Factorial value*<br>\* In the case of a power of two, the power value is omitted.|
|**elu**|calculation|square root|elu (eli *Integer* mahela) *Square root value*|
|**esu**|reading out loud|Pronunciation of each digit in 0–9|Used to read out very large digits, telephone numbers, postal codes, etc.|

## Space and Position

### Space and Position

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**upa**|Preposition, Modifier|from above||
|**upu**|Preposition, Modifier|to above, at above, on above||
|**upo**|Preposition, Modifier|go to above||
|**uda**|Preposition, Modifier|from below||
|**udu**|Preposition, Modifier|to below, at below, on below||
|**udo**|Preposition, Modifier|go to below||
|**uma**|Preposition, Modifier|from right||
|**umu**|Preposition, Modifier|to righr, at right, on right||
|**umo**|Preposition, Modifier|go to right||
|**ula**|Preposition, Modifier|from left||
|**ulu**|Preposition, Modifier|to left, at left, on left||
|**ulo**|Preposition, Modifier|go to left||
|**uha**|Preposition, Modifier|from front||
|**uhu**|Preposition, Modifier|to front, at front, on front||
|**uho**|Preposition, Modifier|go to front||
|**uba**|Preposition, Modifier|from behind||
|**ubu**|Preposition, Modifier|to behind, at behind, on behind||
|**ubo**|Preposition, Modifier|go to behind||

### Crossing the border

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**uta**|Preposition, Modifier|from the other side ||
|**utu**|Preposition, Modifier|over there||
|**uto**|Preposition, Modifier|over there||

### Distance

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**uka**|Preposition|from nearby||
|**uku**|Preposition|to nearby, at nearby, on nearby||
|**uko**|Preposition|go to nearby||
|**uga**|Preposition|from afar||
|**ugu**|Preposition|to afar, at afar, on afar||
|**ugo**|Preposition|go to afar||

## Action and Thinking

### Action and Thinking

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**isa**|Preposition<br><br>Question|use (means, tools)<br><br>Question: *How* (old)|The steps include the use of third parties such as public transportation.<br>When used as a prefix, it means “I want to (propose).”<br>‐|
|**ise**|Preposition|by|Achievement, thanks. Regardless of living or non‐living.|
|**isi**|Preposition|as|behavior, state. Unlike “oyu”, there is no ambiguity|
|**ita**|Preposition|the more||
|**ito**|Preposition|for the purpose of|When used as a prefix, it becomes a modifier “to”|
|**isu**|Preposition|against (consideration, price)|It is also used to mean *“for”*, which is a contrasting usage of *“against”*. Be careful not to be confused with “ago”|
|**iyu**|Preposition|cause, reason|When used as a prefix, it means “I want to (request).”|
|**ibo**|Preposition|about, regarding|Supplementary details of the target.|
|**iso**|Preposition|with|Regardless of living or non‐living.|
|**iya**|Preposition|instead of|When used as a prefix, it means “substitute” or “alternative”|
|**itu**|Preposition, Modifier|even|e.g. Even cats learn entertainment.|
|**iwa**|Question|Question: *How*|‐|

### Comparison

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**ika**|Preposition, Modifier|more|Targets for comparison|
|**iku**|Preposition, Modifier|as much as|Comparison|
|**iko**|Preposition, Modifier|than|Comparison|
|**iki**|Preposition, Modifier|most|Superlative |

### Conjunction and Negation

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**ima**|Coordinating conjunction|and|Aabbreviation: a|
|**imo**|Coordinating conjunction|or|Aabbreviation: o. Putting “ino” in front of it makes it an exclusive disjunction.|
|**imu**|Pseudo‐possessive|of|**The word order is reversed from English.** Aabbreviation: u<br>Can be used up to 2 times in a row. Not allowed more than 3 times.|
|**imi**|Coordinating conjunction|and also|ihi *Conditional statement 1* imi *Conditional statement 2* [imi *Conditional statement 3* …] ihe *A statement when a condition is met*<br>If one of the conditions is met, the condition is satisfied. Aabbreviation: i|
|**ime**|Coordinating conjunction|or else|ihi *Conditional statement 1* ime *Conditional statement 2* [ime *Conditional statement 3* …] ihe *A statement when a condition is met*<br>If even one of the conditions is met, the condition is not met. Aabbreviation: e|
|**iha**|<center>‐</center>|hypothesis||
|**ihi**|Subordinating conjunction|if|ihi *Conditional statement* ihe *A statement when a condition is met*|
|**ihu**|Subordinating conjunction|nor|*Noun phrase 1* ihu *Noun phrase 2*|
|**ihe**|Subordinating conjunction|then|ihi *Conditional statement* ihe *A statement when a condition is met*|
|**iho**|Subordinating conjunction|because||
|**ina**|Subordinating conjunction<br>Modifier<br>Preposition|though<br>nevertheless<br>despire|Determined by whether the sentence comes after the marker.<br>Determined by whether the verb phrase comes after the marker.<br>Determined by whether the noun phrase comes after the marker.|
|**ini**|<center>‐</center>|non‐|Often used as a prefix.<br>When combined with “ba”, it becomes non pronoun or non noun.|
|**inu**|Subordinating conjunction|but|It can't be used for situations that can't realistically be reversed.|
|**ine**|Subordinating conjunction|therefore||
|**ino**|<center>‐</center>|not|**Multiple negation prohibited.**|
|**ida**|Subordinating conjunction|not only that||
|**idi**|Modifier<br>Coordinating conjunction|also||
|**idu**|Subordinating conjunction, Modifier|whichever<br>in any case|Two or more assumptions are required.|
|**ide**|Preposition|except||
|**ido**|Subordinating conjunction|whereas, on the other hand|Contrast|
|**ite**|Subordinating conjunction|≒ even if|Be prepared and act for the purpose.|

### Question

|⁠|Part of speech|Function|Remarks|
|:-:|-|:-:|-|
|**igo**|Question|what||
|**igi**|Question|why||
|**iga**|Question|where||
|**ige**|Question|when||
|**igu**|Question|who||

## Special

### General‐purpose part of speech

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**owa**|Question|General‐purpose question|Place at the beginning of a sentence with a question.<br>At the beginning of a sentence in a polite imperative.<br>At the end of a sentence in a question asking for confirmation.|

### Embedding a sentence

|⁠|Classification|Function|Interpretive sequence|
|:-:|-|-|-|
|**ola**|Embed|Interpreting sentences by focusing on verbs and embedding them.|Subject -> Complement -> Object -> **Verb**|
|**olu**|Embed|Interpreting sentences by focusing on subject and embedding them.|Object -> Complement -> Verb -> **Subject**|
|**ole**|Embed|Interpreting sentences by focusing on object and embedding them.|Subject -> Complement -> Verb -> **Object**|
|**olo**|Embed|Interpreting sentences by focusing on complement and embedding them.|Subject -> Object -> Verb -> **Complement**|
|**oli**|Embed|Create a clause that omits the subject.|Use in combination with other embedded markers.|

### Causative

|⁠|Classification|Function|Remarks|
|:-:|-|-|-|
|**oda**|Causative|make *n* do||
|**odi**|Causative|do that for me||
|**odu**|Causative|Ask for it||
|**ode**|Causative|I will let you||
|**odo**|Passive|be forced to|Used in combination with other causative markers. Alone, it becomes a passive sentence.|

### Imperative

|⁠|Classification|Function|Remarks|
|:-:|-|-|-|
|**oba**|Imperative|do||
|**obi**|Imperative|do it||
|**obu**|Imperative|can do you<br>let's|Whether or not to put “owa” in front of the sentence changes the way the speaker comes across (good or bad).<br>Putting “owa” at the end of a sentence makes it more persistent.|
|**obe**|Imperative|I will do for you||
|**obo**|Passive|be forced to|Use in combination with other imperative markers|

### Assertion, Similarity and Guessing

|⁠|Part of speech|Function|Remarks|
|:-:|-|-|-|
|**oya**|Modifier|must be ~, absolute ~||
|**oyu**|Preposition, Modifier|like, just like||
|**oyo**|Modifier|may be, probably|The probability depends on the context. It is recommended to provide a reason. **No multiple guesses**.|

### Speech

|⁠|Classification|Function|Remarks|
|:-:|-|-|-|
|**ota**|Speech|Direct speech|Conveying hearsay with statements.|
|**oto**|Speech|Indirect speech|Conveying hearsay in writing.|
|**otu**|Speech<br>(Special)|Simultaneous operations<br>Unknown progress|Talk about two or more things at the same time. Perform two or more actions at the same time.<br>When used alone, it means “when you notice” or “unknowingly”. **Be sure to put it at the beginning of the sentence.**|

### Prefix

|⁠|Classification|Function|Remarks|
|:-:|-|-|-|
|**ono‐**|Prefix|Antonym|Create words that have opposite meanings, such as “push” and “pull.”|
|**oko‐**|Prefix|Man prefix||
|**omo‐**|Prefix|Woman prefix||
|**oso‐**|Prefix|Reflexive prefix|Use only when necessary. It can be linked to identifiers, nouns, and verbs.|
