# Tense and Aspect

Indicates when the sentence refers to a point in time.

- The modifiers are **verbs and copula sentence predicates**; all others must use prepositions of the time system or embedded sentences.
- Tense/phase markers cannot be connected to words to indicate tense or phase. In this case, you can use temporal modifiers or turn the marker into a modifier (see Markers page).

## Tense

The general flow of time is represented by the following five **tense markers**.

Tense markers are placed in front of verbs or verb modifiers. The present tense is omitted unless you want to specify what you are doing.

|Past|A little while ago|Now|Soon|Future|
|:-:|:-:|:-:|:-:|:-:|
|eka|eki|eku|eke|eko|

- **eka** siwoda pawome wa. = I ate an apple.
- **eko** pakela wa. = I escape sooner or later.

All tenses are **definite**. To make a guess, always place the guess marker **oyo** before the target of the guess.

- eko **oyo** pakela wa. = I **will** escape sooner or later.

### Handling the future tenses

Used when the time of action cannot be determined.

- **eke** belana da Bikoolu wa. ≒ I will be learning Bicol shortly.
	- Learn the Bicol language in the near future.
- **eko** belana da Ilokano wa. ≒ I will eventually learn Ilocano.
	- Learn the Ilocano language at some future time.

### Extra grammar: Tense that continues between 2 points

If two consecutive tense markers are placed, the situation or condition resulting from the action is maintained for the duration.

- The tense of speech is fixed at the present. The past and future are discussed in terms of the present.
- The state outside the period shows only the fact that **other than the situation or state stated in the sentence**.
	- Even if the “I lost my wallet (as a result, I don't have a wallet)” condition disappears, that doesn't mean that the wallet is back in my hands. Details need to be supplemented with sentences.

||
|-|
|**Continuing tense** **Tense during action** Sentence|

#### Forward continuation tense (Past → Present → Future)

<table>
<tr><th style="text-align:center">Past</th><th style="text-align:center">A little while ago</th><th style="text-align:center">Present</th><th style="text-align:center">   </th><th style="text-align:center">   </th></tr>
<tr><td colspan="5"><strong>eku</strong> eka kiwala pitake wa.</td></tr>
<tr><td colspan="5">I have lost my wallet.</td></tr>
<tr><td colspan="5">I lost my wallet at some point in the past and that condition has continued to the present.</strong></td></tr>
</table>

<table>
<tr><th style="text-align:center">Past</th><th style="text-align:center">A little while ago</th><th style="text-align:center">   </th><th style="text-align:center">   </th><th style="text-align:center">   </th></tr>
<tr><td colspan="5"><strong>eki</strong> eka kiwala pitake wa.</td></tr>
<tr><td colspan="5">I had lost my wallet not long ago.</td></tr>
<tr><td colspan="5">I lost my wallet at some point in the past and remained without it until a short while ago.</strong></td></tr>
</table>

<table>
<tr><th style="text-align:center">   </th><th style="text-align:center">A little while ago</th><th style="text-align:center">Present</th><th style="text-align:center">   </th><th style="text-align:center">   </th></tr>
<tr><td colspan="5"><strong>eku</strong> eka kiwala pitake wa.</td></tr>
<tr><td colspan="5">I lost my wallet some time ago.</td></tr>
<tr><td colspan="5">I lost my wallet some time ago and it has remained in that state up to the present.</strong></td></tr>
</table>

<table>
<tr><th style="text-align:center">Past</th><th style="text-align:center">A little while ago</th><th style="text-align:center">Present</th><th style="text-align:center">Soon</th><th style="text-align:center">Future</th></tr>
<tr><td colspan="5"><strong>eko</strong> eka kiwala pitake wa.</td></tr>
<tr><td colspan="5">I am still missing my wallet.</td></tr>
<tr><td colspan="5">I lost my wallet at some point in the past and it has been in a semi-permanent state.</strong></td></tr>
</table>

#### Reverse continuation tense (Future → At the time of speech)

Expressions that go back in time are also possible.

<table>
<tr><th style="text-align:center">   </th><th style="text-align:center">   </th><th style="text-align:center">Present</th><th style="text-align:center">Soon</th><th style="text-align:center">Future</th></tr>
<tr><td colspan="5"><strong>eku</strong> eko kiwala pitake wa.</td></tr>
<tr><td colspan="5">I lost my wallet in the future.</td></tr>
<tr><td colspan="5">I lost my wallet at some point in the future, and my return to the present day does not change that.</strong></td></tr>
</table>

## Aspect

It refers to a **state** that cuts out a **certain part of a series of target actions (from start to finish)**.

|Examples of actions|Contents of series of actions|
|-|-|
|Eat an apple|From the time you start eating an apple to the time you finish eating it.|
|Knock on the door|From when you start knocking on the door until you finish knocking the required number of times.|
|Cutting three vegetables|From the start of cutting the 1st vegetable to the end of cutting the 3rd vegetable.|
|Calling multiple students|From calling the first student to finishing calling all students.|
|Jumping|From the beginning of the jumping preparatory motion to the peak of the jumping.|

The following five **aspect markers** can be used to express more detailed time.

|Before the start|After the start|In progress|Before the finish|After the finish|
|:-:|:-:|:-:|:-:|:-:|
|edi|eda|edu|ede|edo|
|**About to start** <br>a series of actions|**Starting** <br>a series of actions|**In the middle** of <br>a series of actions|**About to complete** <br>a series of actions|**Completing** <br>a series of actions|

Aspect markers are placed after the tense markers.

- eke **ede** belana wa. = I will finish studying soon.
- eka **edo** siwoda pawome wa. = I had finished my apple.
- eko **edi** siwoda pawome wa. = I will start eating apples soon.

Aspect markers can be used by themselves, and the placement is the same as for tense markers.

- **edi** siwoda pawome wa. = I have **about to start** eat an apple.
- **eda** siwoda pawome wa. = I have **started** eating an apple.
- **edu** siwoda pawome wa. = I am eat**ing** an apple.
- **ede** siwoda pawome wa. = I have **about to finish** eat an apple.
- **edo** siwoda pawome wa. = I have **finished** eating an apple.

### edi: Before the start 

Indicates that you are about to start a series of actions.

- edi kuwola okoba. = He is trying to dying.
- edi malaga ago da Amelika wa. = I am about to leave for America.

### eda: After the start

Indicates that a series of actions are starting.

- eda lisata anu tahane imu lahele pusile. = The number of cats is beginning to increase in my neighborhood.
- eda bataka ibo da Nihon lapuse. = The number of children in Japan is beginning to decline.

### edu: In progress

Indicates that a series of actions are in the middle of a series of actions.

It **does not** include the meaning “repeat”.

- edu hipate wa.
	- ⭕It is in the middle of the action from the start of the jump to the end of the jump.
	- ❌I am jumping. = It means that the action of jumping is performed more than once.

- edu siwoda pawome wa.
	- ⭕It is in the middle of the action from the start of eating the apple to the end of eating.
	- ❌I eat apples repeatedly. = It ends up eating anything other than existing apples.

- edu heluma so pinute.
	- ⭕It is in the middle of the operation from when the door starts to close until it finishes closing.
	- ❌The door is repeatedly closed. = The door will close more than once.

Use a numerical system marker to indicate a period of time. Even then, **repeat must be explicit**.

- edu siwoda pawome eta ega kolumo palibe wa. = I have been eating apples for three days now.
	- I have been doing a series of actions “eating an apple” for three days. I haven't eaten anything other than the apples that existed when I started eating.
- edu dulito siwoda pawome eta ega kolumo palibe wa. = I have been eating apples for three days.
	- I have been doing a series of actions “eating an apple” for three days. In addition to the apples that are present when you start eating, you are eating other apples.

### ede: Before the finish

Indicates that a series of actions is about to end.

- ede kuwola so kudole. = The dog is dying.
- ede zaseta hilita dokase wa. ≒ It's getting hard for me to put up with sweets.
- eka ede bosuka anu wa mobile. = The car nearly hit me.

### edo: After the finish (After perfect)

The following three conditions are met simultaneously.

#### 1. The action has been completed

The action is not forcibly or voluntarily terminated in the middle of the process.

#### 2. The state brought about by the completion of the action continues

Whereas *edu* continues the action itself, **edo** continues the state resulting from the completion of the action.

- ❌edu kiwala pitake wa. = I'm about to lose my wallet now.

- edo kiwala pitake wa. = I have lost my wallet.
	- As a result of the completed “lose wallet” action, the wallet is still lost today.

#### 3. It doesn't matter when the action was completed

If necessary, supplement with tenses, modifiers, and prepositions.

- edo siwoda pawome wa. = I have finished eating an apple.
	- I don't know one day, but I am finished with the apples, so those apples are currently gone.
- **eki** edo siwoda pawome wa. = I have finished eating an apple **a while ago**.
	- I have finished eating an apple a while ago, so that apple is not present.
- edo **kahapo** siwoda pawome wa. = I have finished eating an apple **yesterday**.
	- I have finished eating an apple yesterday, so that apple is not present.
- edo siwoda pawome **ega kolumo palibe** wa. = I have finished eating an apple **before three days**.
	- I have finished eating an apple before three days, so that apple is not present.
- edo yumo pawata **bukaso** lumase imu tipane. = All hospital appointments for **tomorrow** have been finalized.
	- All hospital appointments for tomorrow have already been finalized, so they will not be able to examine me tomorrow.

If the complemented expression refers to the future at the time of utterance, the completion of the action is also in the future.

- edo binasa **egu 2094 buwose** da Mapela = The earth have destroyed **in 2094**.
	- Since the earth have destroyed in 2094 AD, which is in the future from now, there will be no earth in the future from then on.
		- Used in time travel stories when a person visiting the past tells of a definited future.

## Combination of tense and aspect

By combining tenses, aspects, and guesses, you can express time in 5 × 6 × 2 = 60 different ways.

This is used in novels to explain the situation of a person who has come across time.

### Cautions with combinations

You cannot use combinations of contradictory tenses or aspects.

**You need to understand the flow of time properly, not just the meaning of the markers.**

❌eku edo siwoda pawome wa. ≒ I have just finished eating an apple. ➡ Reason: You are calling the past the present.  
⭕eki edo siwoda pawome wa. = I had finished eating an apple a while ago.

\* It cannot be accurately translated into English because it uses expressions that are unique to Zokale.

## Tense and Aspect marker list

|‐|Before the start|After the start|Nonaspect|In progress|Before the finish|After the finish|
|-|:-:|:-:|:-:|:-:|:-:|:-:|
|**Past**|eka edi|eka eda|eka|eka edu|eka ede|eka edo|
|**A little while ago**|eki edi|eki eda|eki|eki edu|eki ede|eki edo|
|**Nontense**|edi|eda|‐|edu|ede|edo|
|**Soon**|eke edi|eke eda|eke|eke edu|eke ede|eke edi|
|**Future**|eko edi|eko eda|eko|eko edu|eko ede|eko edo|
