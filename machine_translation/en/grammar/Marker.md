# Marker

A marker is a mark that indicates the function or wording of the word that follows it.

Just as nouns and verbs change from their original meanings depending on their modifiers, the meaning and function of the words that follow the marker also change depending on the marker.

There are two main types of markers: **function words** and **prefixes**.

## Spec

- It is composed of three letters: **vowel + consonant + vowel**.
- Function words can be used as prefixes, but some are unsuitable or obviously unusable. In some cases, the function of the prefix is predetermined by the grammar. If a marker-word compound is not specified in the grammar, it must be explained at the beginning of a conversation (sentence).
- **It is not called a “word”**. When you say “word”, be careful not to include the marker in the word.

## Function words

- These are words whose meaning or function is not changed by their modifiers, such as conjunctions, interrogatives, comparative classes, prepositions, etc. For convenience, we use the same names as in other common languages, but Zocalor treats them all as markers.
- For convenience, we use the same names as in other common languages, but in Zokale, all of these are also treated as markers.
- Some words can be used on their own, but many require a word to follow them.
- Some function words have more than one function.

### Naming conventions

Functional terms are named according to the following rules.

#### Basic functions

The first letter represents the general classification.

|||
|:-:|-|
|a|Places, objects, and organisms (or their equivalents or events)|
|e|Time flow, numbers, and calculations|
|u|Space, position|
|i|Action, thinking|
|o|Special (Significantly change the role of words, etc.)|

#### Functions

The second letter indicates the general function. There is no rule of thumb, as functions vary widely.

|||
|:-:|-|
|g|Behavior and time flow|

#### Actual functions

The third character represents the details of the function based on the second character.

|||
|:-:|-|
|a|Starting point, base point, active action, affirmation. With action. It is often paired with “o”.|
|i|Starting point, base point, active action, affirmation. Not accompanied by action. Often paired with “e”.|
|u|A place or moment. A certain point that does not move or is motionless. In-between, middle.|
|e|Destination, endpoint, passive action, negation. Not accompanied by action. Often paired with “i”.|
|o|Destination, endpoint, passive action, negation. With action. Often paired with “a”.|

## Prefixes

- Markers do not have a meaning by themselves, but have the ability to add meaning by being combined before a word.
- Like function words, they are treated as markers in Zokale.
- As with compound words, accents are placed **only** on the first syllable.

## Using markers as regular words

If you want to use a marker as a modifier, you can turn it into a normal word according to its part of speech ending by appending **sid‐** to it. Some markers may not be suitable for wordification.

- ane (*in ~*) + sid + o (*modifier*) = anesido (*inside ~*)

It is not necessary for markers that have a modifier function, but you can concatenate **sid‐** to them.

## Order of placing markers

There is no particular order in which markers should be placed when using multiple markers.

However, if you put tense markers and phase markers first, it will be easier to construct the meaning.

<table style="text-align:center">
<tr><td>eka</td><td>oyo</td><td>obo</td><td>belana</td><td>anu</td><td>omoba</td><td>okoba.</td></tr>
<tr><td>Tense</td><td>Guess</td><td>Passive</td><td>Verb</td><td>Preposition</td><td>Object</td><td>Subject</td></tr>
<tr><td>past</td><td>would be</td><td>be forced to</td><td>study</td><td>to</td><td>she</td><td>he</td></tr>
<tr><td colspan="7">He would have let her study him.</td></tr>
</table>

Markers that are more important than others, such as instruction markers, should be placed first.
