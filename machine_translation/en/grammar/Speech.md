# Speech

## Direct speech and Indirect speech

There are two types of conversational sentences: **direct sentences** that express what you or your partner said, and **indirect sentences** that express what you said in your own words.

In the example below, only three sentences are connected at most, but there is no limit to the number of sentences you can connect. You can mix direct and indirect speech. However, it is not recommended to use more than three sentences, as it makes it difficult to understand the sentences.

**The tense of each sentence is independent.** ⁠Be careful not to make a mistake in specifying the tense.

### Direct speech

||
|-|
|“*Speaker or Third party says*” [**ota** “*Third party says*” …] **ota** *Speaker's action*|

- “eka lenuta lane pusile.” **ota** eka puhuta wa. = “The cat was flying in the sky,” I said.
- “edu lenuta lane pusile.” **ota** eka puhuta anu wa okoba. = “The cat was flying in the sky,” He said to me.

#### Direct speech imperatives

- “obi ino yukusa anu kabitabe!” **ota** eka puhuta anu wa okoba. = “Don't run in the hallway,” he told me.

#### Direct speech questions

- “owa anu iga pusile?” **ota** eka puhuta anu wa omoba. = “Where is the cat?” she said to me.
- “owa anu iga pusile?” **ota** owa eka puhuta anu wa omoba? = “Where's the cat?” she said to me?

### Indirect speech

||
|-|
|*Speaker's thoughts or hearsay from a third party* [**oto** *Speaker's thoughts or hearsay from a third party* …] **oto** *Speaker's action*|

- eka lenuta lane pusile **oto** eka puhuta wa = The cat is flying in the sky, I said.
- edu lenuta lane pusile **oto** eka puhuta anu wa okoba = The cat is flying in the sky, He said to me.
- eka lenuta lane pusile **oto** eka puhuta na = Someone said the cat is flying in the sky.

#### Indirect speech imperatives

- obi ino yukusa anu kabitabe **oto** eka puhuta anu wa okoba. = Don't run in the hallway, he told me.

#### Indirect speech questions

- owa anu iga pusile? **oto** eka puhuta anu wa omoba. = Where is the cat, she told me.
- owa anu iga pusile? **oto** owa eka puhuta anu wa omoba? = Where is the cat, she told me?

### Mixture of direct and indirect speech

The speaker's action is always written last, and the sentence is interpreted from left to right.

- “eka nikaha omoba.” **ota** eka loha okoba **oto** pikila wa. = “She is married,” I believe he heard.
- “eka nikaha omoba.” **ota** eka loha okoba **oto** owa pikila ya? = Do you think he heard, “She's married?”
- “owa eka nikaha omoba?” **ota** eka loha okoba **oto** owa pikila ya? = “Did she get married?” Do you think he asked that?

## Subordinate sentences (noun clauses)

Indirect speech can be used for more than just conversation.

### Target is the object

- pusile sa **oto** wakala wa. = It's a cat, I think.
- pusile sa **oto** owa wakala ya? = Do you think it's a cat?
- edu doluma anu tahane okoba **oto** wakala wa. = He is sleeping at home, I think.
- palekano maguba okoba **oto** wakapona omoba. = She believes he will come back alive.
- eka sudaho kuwola okoba **oto** ino mohina omoba. = She doesn't know that he is already dead.
- owa sudaho kuwola okoba **oto** ino mohina omoba. = She doesn't know if he is already dead.
- eka sudaho kuwola okoba **oto** owa mohina oboba? = Does she know that he is already dead?
- owa eka sudaho kuwola okoba **oto** owa mohina oboba? = Does she know if he is already dead?

#### Multiple negations and Multiple guesses

A series of negations or guesses is prohibited in principle, as it makes the meaning difficult to understand. They cannot be used in creative writing, except for intentional staging.

❌iho pikila iho kohoto anu da Tookiyo lapolo gulale **oto** ino bolida wa. = I can't think of anything that wouldn't cause a major earthquake in Tokyo. = I don't think there will be a major earthquake in Tokyo.

❌oyo bolida siwoda pawome wa **oto** eka oyo wakala wa. = Maybe I would like to eat an apple, I might have thought. = I could possibly want to eat an apple.

### Target is the subject

- eka mohina okobo kuwole omoba **oto** mepago sa. = It's unfortunate that she found out about his death.
- eka melupa pedaso mukase okoba **oto** bahaga sa **oto** owa wakala ya? = Do you think it's a blessing that he has forgotten his painful past?

### Target is the modifier

- doloka pusile **oto** oyu so menala wa. = As the cat rolls, I dance.
- edu lula linute **oto** oyu so loha wa. = The birds are singing, and it sounds like it to me.
- edu lula linute **oto** owa oyu so loha ya? = Does it sound to you like a bird singing?
