# Question

A question is indicated by placing a question declaration marker **owa** at the beginning of the sentence and a question mark **?** at the end of the sentence.

## Easy question

### How to create a questionnaire

Place an **owa** at the beginning of a normal sentence and a question mark **?** at the end of a sentence.

- siwoda pawome ya. = You eat an apple.
	- **owa** siwoda pawome ya? = Do you eat an apple?
- anu da Tookiyo ya. = You are in Tokyo.
	- **owa** anu da Tookiyo ya? = Are you in Tokyo?

### How to answer a question

Answer with the **verb** that is the subject of the question. To deny, place **ino** before the verb.

If the target verb phrase contains modifiers, it is optional to include them in the response. A modifier is only needed if you are answering a question with a different definite condition (ambiguous question -> definite answer, definite question -> ambiguous answer).

- owa ino **polola** ya? = Aren't you hungry?
	- polola. = I am hungry.
	- ino polola. = I'm not hungry.
- owa oyu **polola** okoba? = Do you think he is hungry?
	- (oyu) polola. = I think he is hungry.
	- (oyu) ino polola. = I think he is not hungry.

If the scope of the subject changes in response to a question, answer with a **statement**.

- owa **haluta** siwoda pawome okobaba? = Are they **demanding** to eat the apple? = Do they want to eat the apple?
	- **haluta** siko ba. = He is the only one who is **demanding** = Only he will eat it.

### Asking for Confirmation

To ask someone to confirm what they are saying, put owa? at the **end of a normal sentence**. The answer is the same as in the question.

- polola ya. = You are hungry.
	- polola ya **⁠owa?** = Are you hungry, aren't you?
		- polola. = I am hungry.
		- ino polola. = I'm not hungry.

## 5W1H

When asking “when”, “where”, “who”, “what”, “why”, and “how” questions, replace the target words with **question markers**.

The same question marker will have a different meaning depending on the part of speech that is being asked.

### ige *when*

- owa tula <u>egu **ige**</u> ago paluke ya? = When do you come to the park?
- owa belaya <u>eto **ige**</u> ya? = How long do you want to study?
- owa edu mebatasa <u>eta **ige**</u> anu tu ya? = How long have you been trapped in here?
- owa <u>eto **ige**</u> takale? = How long is the deadline?

### iga *where*

- owa <u>anu **iga**</u> ya? = Where are you?
- owa menuga <u>ago **iga**</u> ya? = Where do you want to go?
- owa eka tula <u>aga **iga**</u> ya? = Where do you come from?
- owa eka tabata <u>anu **iga**</u> iso omoba ya = Where did you meet her?

### igu *who*

- owa **igu** ya? = Who are you?
- owa eka loha so puhute <u>aga **igu**</u> ya? = Who told you the story?
- owa eka pudota ya ago lubane **igu**? = Who dropped you in the hole?

### igo *what*

- owa **igo** ta? = What is this?
- owa **igo** yo nome? = What is your name?
- owa hala **igo** ya? = What will you do?
- owa edu hala **igo** ya? = What are you doing?
- owa haluta hala **igo** ya? = What do you want to do?
- owa pelata hala **igo** ya? = What do you do to play?
- owa menulisa <u>isa **igo**</u> ya =? What do you use to write with?
- owa eka bumila bese <u>aga **igo**</u> ya? = What did you get the water from?
- owa oda belana ya **igo**? = What makes you want to study?

### igi *why*

- owa **igi** belana do Zokale wagone ya? = Why do you want to learn about Zokale?

### isa *how*

The means/method marker **isa** has a slightly different format than the other question markers because it can be used as an adverb.

owa **isa** belana do Zokale wagone ya? = How will you learn about Zokale?

### etu *how, how many, how much*

The number of days and hours consumed marker **etu** is formatted slightly differently from other question markers because it can be used as an adverb.

- owa **etu** eka belana do Nihon wagone ya? = How much Japanese have you learned?

Specific objects are complemented with modifiers and prepositions.

- owa **etu** eka <u>pitoko</u> belana do Nihon wagone ya? = How long period of time have you been studying Japanese?
- owa **etu** eka belana do Nihon wagone <u>etu pitoko palibe</u> ya? = How long days have you been studying Japanese?
