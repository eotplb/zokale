# Comparative and Superlative

It is used to compare two or more objects.

All markers used are **prepositions**. **Only noun phrases** can be placed, except when using embedded sentences (see the embedded sentences page).

## Excel: ika + iko

You can also place two or more **ika** to compare.

### (Subject) is more (Verval phrase/Modifier phrase/Noun phrase) than (Comparison object)
||
|-|
|*Verval phrase/Modifier phrase/Noun phrase* **ika** *Comparison object* \[*Comparison object* …\] *Subject*|

#### Target is the verbal phrase

- moneto doluma <u>**ika** pusile</u> wa. = I sleep more than a cat. (I > cat)
- 3x moneto doluma <u>**ika** pusile</u> wa. = I sleep three times more than my cat. (I > cat)
	- Pronunciation: kolumo bano moneto doluma ika pusile wa.
- moneto doluma <u>**ika** pusile</u> <u>**ika** kudole</u> wa. = I sleep more than a cat, more than a dog. (I > dog > cat)

#### Target is the modifier phrase

- lalugo <u>**ika** omoba</u> wa. = I'm taller than she is. (I > she)
- sonilo bono <u>**ika** okoba</u> omoba. = She's more elegant and beautiful than he is. (she > he)
- lapido <u>**ika** pusile</u> kudole oto wakana wa. = Dogs run faster than cats, in my opinion. (dog > cat) \* Use with subordinate sentences

#### Target is the noun phrase

- bono kelase <u>**ika** ya</u> wa. = I'm doing better than you. (I > you)

### (Subject) is (Verb phrase) with (Superior object) rather than (Inferior object)

**ika** and **iko** can come first.

||
|-|
|*Verb phrase* **ika** *Inferior object* **iko** *Superior object* *Subject*|
|*Verb phrase* **iko** *Superior object* **ika** *Inferior object* *Subject*|

- kutena ika pusile <u>**iko** kudole</u> wa. = I like dogs better than cats. (dog > cat)
- kutena ika pusile ika kudole <u>**iko** linute</u> wa. = I prefer birds to cats, dogs to birds. (bird > dog > cat)
- kutena ika posile ima kudole <u>**iko** linute</u> wa. = I like birds better than cats and dogs. (bird > cat = dog)
- kutena ika pusile ika kudole <u>**iko** linute</u> wa oto wakana okoba. = He thinks I prefer birds to cats, dogs to birds. (bird > dog > cat) \* Use with subordinate sentences

## Almost equal: iku + iko

You can also put two or more **iku** and treat them equally.

### (Subject) is equal to (Comparator) (Verb phrase/Modifier phrase/Noun phrase)

||
|-|
|*Verb phrase/Modifier phrase/Noun phrase* **iku** *Comparison object* \[**iku** *Comparison object* …\] *Subject*|

The method for targeting each part of speech is the same as for **ika**.

- ane pelata <u>**iku** kudole</u> wa. = I play outside as much as my dog. (I ≒ dog)

### (Subject) is as much (Verb phrase) as (Object of comparison 1) is (Object of comparison 2).

**iku** and **iko** can come first.

||
|-|
|*Verb phrase* **iku** *Object of comparison 1* \[**iku** *Object of comparison 1* …\] **iko** *Object of comparison 2* \[**iko** *Object of comparison 2* …\] *Subject*|
|*Verb phrase* **iko** *Object of comparison 2* \[**iko** *Object of comparison 2* …\] **iku** *Object of comparison 1* \[**iku** *Object of comparison 1* …\] *Subject*|

The same meaning can be obtained by connecting **iku** or **iko** with ima for equally treated objects.

- kutena **iku** pusile **iko** kudole wa. = I like dogs as much as I like cats. (cat ≒ dog)
- kutena **iku** pusile ima kudole wa. = I like cats and dogs equally. (cat ≒ dog)
- kutena **iku** pusile **iko** kudole **iko** linute wa. = I like dogs and birds as much as I like cats. (cat ≒ dog ≒ bird)
- kutena **iku** pusile **iko** kudole ima linute wa. = I like dogs and birds as much as I like cats. (cat ≒ dog ≒ bird)

## More than: iku + ika

By combining iku and ika, we can express **“more than (≥).”**

### (Subject) is more than (Comparator) (Verb phrase/Modifier phrase/Noun phrase)

||
|-|
|*Verb phrase/Modifier phrase/Noun phrase* **iku ika** *Comparator* *Subject*|

The method for targeting each part of speech is the same as for **ika**.

- bono nede <u>**iku ika** wa</u> ya. = You are a better person than I am. (you ≥ I)

### (Subject) is more than (Object of Comparison 1) and (Object of Comparison 2) is more than (Verb Phrase).

You can use either **iku ika** or **iko** first.

||
|-|
|*Verb Phrase* **iku ika** *Object of Comparison 1* **iko** *Object of Comparison 2* *Subject*|
|*Verb Phrase* **iko** *Object of Comparison 2* **iku ika** *Object of Comparison 1* *Subject*|

- kutena <u>**iku ika** pusile</u> iko kudole wa. = I like dogs as much or more than I like cats. (dog ≥ cat)

## Composite

If the result of the previous comparison is the target of the next comparison, the target word can be omitted.

- kutena (ika) **iku** pusile **iko** kudole **ika** sa **iko** linute wa. = I like dogs as much as I like cats, but I like birds even more. (bird > cat ≒ dog)
- kutena (ika) **iku** pusile **iko ika** kudole **iko** linute wa. = I like dogs as much as I like cats, but I like birds more. (bird > cat ≒ dog)
- kutena (ika) **iku** pusile ima kudole **ika** sa **iko** linute wa. = I like cats and dogs equally, but I like birds more than that. (bird > cat ≒ dog)
- kutena (ika) **iku** pusile ima kudole **ika iko** linute wa. = I like cats and dogs equally, but I like birds better. (bird > cat ≒ dog)

## Most superior: iki + iko

### The (Subject) is the one whose (Top‐level object) is the most (Verb phrase).

||
|-|
|*Verb phrase* **iki iko** *Top‐level object* *Subject*|

- kutena <u>**iki iko** musuto pusile</u> wa. = I like black cats best.

### The (Subject) is the most (Verb phrase/Modifier phrase/Noun phrase) in the (Scope).

||
|-|
|*Verb phrase/Modifier phrase/Noun phrase* **iki** *Scope* *Subject*|

The method for targeting each part of speech is the same as for **ika**.

- bahubuso <u>**iki** to pusile</u> baluko pusile. = The white cat is the strongest of all these cats.

### The (Subject) is the most (Top‐level object) of the (Scope) in the (Verb phrase).

**iki** and **iko** can come first.

||
|-|
|*Verb phrase* **iki** *Scope* **iko** *Top‐level object* *Subject*|
|*Verb phrase* **iko** *Top‐level object* **iki** *Scope* *Subject*|

- kutena <u>**iki** pusile</u> iko musuto pusile wa. = I like black cats best of all cats.

## Use as modifiers

Used in the comparative and superlative classes, **ika**, **iku**, and **iki** can also be used as modifiers of verb phrases, modifying phrases, and noun phrases.

### Pseudo-comparative class

When something is required to be higher, **ika** is used as a modifier.

You can use **ika iko** target in the same way as in the regular comparative class, but the meaning will be weaker than if you use it as a modifier.

- **ika** taluba kane wa. = I need **more** money.
- **ika** haluta siwoda pawome wa. = I want to eat **more** apples.
- oyo **ika** kilumo bukase. = Tomorrow will be **more** cold.

### Equal and the Same

When **iku** is used as a modifier, it means “equal” or “same”.

- <u>**iku** sekole imu magale</u> da Takasi ima da Noliko. = Takashi and Noriko are students at the same school.

### Emphasized expression

Use **iki** as a modifier if you want to emphasize especially strongly that you are above all objects.

The degree of emphasis depends on the context, but if you don't specify the scope of comparison beforehand, the scope of comparison can be anything that exists in the world.

This is more meaningful than “iki iko *top‐level target*” in the same format as the regular superlative.

- <u>**iki** kutena</u> musuto pusile wa. = I definitely like black cats.
- <u>**iki** bahubuso</u> to setate. = This weapon is the most powerful in the universe.
