# Preposition

It is used to describe what the words used to complement the object, verb, or subject mean.

## Overview

The word that is the target of the preposition is called the **complement**. The combination of a preposition and its complement is called a prepositional phrase, but you can usually call the preposition and its complement together as a complement.

The way a word acts is “complement -> object -> verb.” A sentence without an object is “complement -> verb.”

If the subject is the object, it is “complement -> subject.”

Only noun phrases can be used in prepositional phrases. Therefore, the number of prepositions that can be used for subjects that do not act on verbs is extremely limited. These limitations can be solved by using embedded markers (see the sentence embedding page).

### Examples of prepositions

A place, thing, or creature that does not move from its place **anu**

<table style="text-align:center">
<tr><td>siwoda</td><td>pawome</td><td><strong>anu</strong></td><td><u>sekole</u></td><td>wa.</td></tr>
<tr><td>Verb</td><td>Object</td><td><strong>Preposition</strong></td><td><u>Complement<u></td><td>Subject</td></tr>
<tr><td>eat</td><td>apple</td><td><strong>at</strong></td><td><u>school</u></td><td>I</td></tr>
<tr><td colspan="5">I eat an apple at school.</td></tr>
</table>

A metaphor **oyu**\*

<table style="text-align:center">
<tr><td>siwoda</td><td>pawome</td><td><strong>oyu</strong></td><td><u>golile.</u></td><td>pusile</td></tr>
<tr><td>Verb</td><td>Object</td><td><strong>Preposition</strong></td><td><u>Complement<u></td><td>Subject</td></tr>
<tr><td>eat</td><td>apple</td><td><strong>like</strong></td><td><u>gorilla</u></td><td>cat</td></tr>
<tr><td colspan="5">Cat eats apple like a gorilla.</td></tr>
</table>

<table style="text-align:center">
<tr><td>siwoda</td><td>pawome</td><td>pusile</td><td><strong>oyu</strong></td><td><u>golile.</u></td></tr>
<tr><td>Verb</td><td>Object</td><td>Subject (noun)</td><td>Subject <strong>(preposition)</strong></td><td>Subject <u>(complement)</u></td></tr>
<tr><td>eat</td><td>apple</td><td>cat</td><td><strong>like</strong></td><td><u>gorilla</u></td></tr>
<tr><td colspan="5">A gorilla‐like cat eats an apple.</td></tr>
</table>

\* Note: **oyu** can also function as a modifier to modify verbs.

<table style="text-align:center">
<tr><td><strong>oyu</strong></td><td><u>golile</u></td><td>siwoda</td><td>pawome</td><td>pusile.</td></tr>
<tr><td><strong>Modifier</strong></td><td><u>Complement</u></td><td>Verb</td><td>Object</td><td>Subject</td></tr>
<tr><td><strong>like</strong></td><td><u>gorilla</u></td><td>eat</td><td>apple</td><td>cat</td></tr>
<tr><td colspan="5">Cat eats apple like a gorilla.</td></tr>
</table>

### Examples of plural prepositions

You can put any number of prepositions in a sentence, as long as they make sense.

There is no set order, but we recommend using chronological order or the order in which the objects appear.

Example: **aga** for starting point, **agu** for passing point, **ago** for destination point

<table style="text-align:center">
<tr><td>menuga</td><td><strong>aga</strong></td><td><u>tahane</u></td><td><strong>agu</strong></td><td><u>sekole</u></td><td><strong>ago</strong></td><td><u>pakule</u></td><td>pusile.</td></tr>
<tr><td>Verb</td><td><strong>Preposition</strong></td><td><u>Complement</u></td><td><strong>Preposition</strong></td><td><u>Complement</u></td><td><strong>Preposition</strong></td><td><u>Complement</u></td><td>Subject</td></tr>
<tr><td>go</td><td><strong>from</strong></td><td><u>home</u></td><td><strong>through</strong></td><td><u>school</u></td><td><strong>to</strong></td><td><u>park</u></td><td>cat</td></tr>
<tr><td colspan="8">The cat goes from home, through the school, to the park.</td></tr>
</table>

Example: To act or think together **iso**, to express a means or method **isa**, to express the time required or elapsed **etu**

- menuga **iso** <u>yusutabe</u> **isa** <u>kulute</u> **aga** <u>da Tookiyo</u> **ago** <u>da Oosaka</u> **etu** <u>1h:15m </u> wa.
	- My friend and I will take the train from Tokyo to Osaka in 1 hour and 15 minutes.

### Difference between anu and ago

While **anu** focuses on reachability, **ago** focuses on movement.

|Example|Meaning|Interpretation|
|-|-|-|
|eka menuga **anu** paluke wa.|I went to the park|I arrived, moving towards the park.|
|eka menuga **ago** paluke wa.|I made my way to the park|I started to move towards the park (I don't know if I arrived).|
