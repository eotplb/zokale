# Numeric

## Numeral

|Numeric|Word|
|-:|-|
|0|nulo|
|1|taso|
|2|kakuso|
|3|kolumo|
|4|nelumo|
|5|biso|
|6|kuloso|
|7|sepo|
|8|kewalo|
|9|nado|
|10|deko|
|100|hako|
|1000|seko|
|10⁴|mako|
|10⁸|kago|
|10¹²|tago|
|10¹⁶|sego|

## Writing numbers

Numbers 0–9 can be written as words or Arabic numerals. **Numbers 10 and above should always be written in Arabic numerals**.

When separating digits with symbols, use a **comma** to separate <u>every **four** digits</u>.

## Read the numbers

### Read the words faithfully

It is suitable for small numbers.

- Two or more digits are expressed by arranging the **number + digit number word**.
- If the digit word has a value of 1, **omit taso**.
- Digits with a value of 0 are omitted.

|Numeric|How to read it out loud (words in bold are digits)|Calculation formula|
|-:|-|-|
|64|kuloso **deko** nelumo|6 × 10 + 4|
|128|**hako** kakuso **deko** kewalo|100 + 2 × 10 + 8|
|512|biso **hako** **deko** kakuso|5 × 100 + 10 + 2|
|1024|**seko** kakuso **deko** nelumo|1000 + 2 × 10 + 4|
|3,2768|kolumo **mako** kakuso **seko** sepo **hako** kuloso **deko** kewalo|3 × 10000 + 2 × 1000 + 7 × 100 + 6 × 10 + 8|
|4729,0000|nelumo **seko** sepo **hako** kakuso **deko** nado **mako**|(4 × 1000 + 7 × 100 + 2 × 10 + 9) × 10000|
|12,3456,7890|**deko** kakuso **kago** kolumo **seko** meluno **hako** biso **deko** kuloso **mako** sepo **seko** kewalo **hako** nado **deko**|(2 × 10 + 2) × 100000000 + (3 × 1000 + 4 × 100 + 5 × 10 + 6) × 10000 + 7 × 1000 + 8 × 100 + 9 × 10|

### Reads like a code number.

This is good for large numbers.

Place esu in front of the number, separate every four digits from the last place, supplement the digit units for numbers over 10,000, and read the words 0–9 one digit at a time from the top place.

If the right (small) place from a word in a certain digit is all zeros, it is always omitted.
If a large digit within the range of four digits becomes zero, the reading of that digit can also be omitted.

**esu** is a marker for reading out. **It is not used for writing.**

|Numeric|How to read|
|-:|-|
|4096|**esu** nelumo nulo nado koluso|
|12,3456,7890|**esu** taso kakuso **kago** kolumo nelumo biso kuloso **mako** sepo kewalo nado nulo|
|23,0000,0000|**esu** kakuso kolumo **kago**|
|356,0700,0020|**esu** kolumo biso kuloso **kago** (nulo) sepo nulo nulo **mako** (nulo) (nulo) kakuso nulo|

#### Zip code, phone number, serial number, etc.

Use **ima** to separate numbers from numbers.

- Zip code 12345‐6789 = esu taso kakuso kolumo nelumo biso **ima** kuloso sepo kewalo nado

## Modification by number

The meaning changes depending on which part of speech it modifies and whether there is a unit modifier present. You can also use ordinal numbers as described below.

If it involves a unit, it is also a modifier, so it needs to be a modifier, not a noun.

### Modifier the noun

It indicates the number of pieces. The same is true for any other modifiers that modify the noun, **except units**. There is no particular order in which they should be modified.

- siwoda <u>**kolumo** pawome</u> wa. = I eat three apples.
	- siwoda <u>**kolumo** lapolo pawome</u> wa. = I eat three big apples.
	- siwoda <u>lapolo **kolumo** pawome</u> wa. = I eat three big apples.

If units are present, they are purely numerical.

- siwoda <u>**10 senutometolo** pawome</u> wa. = I eat a 10cm apple.
- siwoda <u>lebalo **10 senutometolo** pawome</u> wa. = I eat an apple 10 cm wide.

### Modifier the verb

It indicates the number of pieces. The same is true for any other modifiers that modify the noun, **except units**. There is no particular order in which they should be modified.

- <u>**kolumo** siwoda</u> pawome wa. = I <u>eat</u> an apple **three times**.
- <u>**kolumo** lapido siwoda</u> pawome wa. = I <u>eat</u> an apple <u>**three times** in a hurry</u>.
- <u>lapido **kolumo** siwoda</u> pawome wa. = I <u>eat</u> an apple <u>**three times** in a hurry</u>.
- <u>yaleno lapido **kolumo** siwoda</u> pawome wa. = I <u>eat</u> an apple <u>**three times** again in a hurry</u>.

If units are present, they are purely numerical.

- <u>**50 senutometolo** ganapa</u> lipine wa. = I will finish the ribbon to 50 cm.
- <u>lebalo **kolumo senutometolo** ganapa</u> lipine wa. = I will finish the ribbon 3cm wide.

Some expressions are more specific with a preposition.

- <u>**10 metolo** helita</u> pawome wa. = I will throw the apple 10 meters.
	- helita pawome <u>uto **10 metole**</u> wa. = I will throw the apple 10 meters across.

### Modifier the modifier

It is a purely numerical value.

- <u>**kolumo** kolukeno</u> ika okoba omobo tamiko gune. = Her magic power is three higher than his.
- <u>**kolumo senutometolo** lapolo</u> ika zo pawome to pawome. = This apple is 3 cm bigger than that apple.

## Ordinal number

Place **eso** in front of the number. It can be combined with other markers.

- siwoda <u>kolumo pawome</u> wa. = I eat three apples.
	- siwoda <u>**eso** kolumo pawome</u> wa. = I eat the third apple.
	- siwoda uma <u>**eso** kolumo pawome</u> wa. = I eat the third apple from the right.
	- siwoda ula uda <u>**eso** kolumo pawome</u> wa. = I eat the third apple from the bottom left.
	- <u>**eso** kolumo siwoda</u> pawome wa. = I eat an apple third.
	- <u>**eso** kolumo siwoda</u> ula uda <u>**eso** kolumo pawome</u> wa. = I eat the third apple from the bottom left, I eat the third.

## Decimals

Decimal points are separated by **kiko**, and decimals are pronounced one digit at a time, regardless of the number of digits.

- 3.14159 = kolumo **kiko** taso nelumo taso biso nado

## Fractions

The reading of the number is the same as the code number.

||
|-|
|**ela** (*Integer* **ima**) *Numerator* **mahela** *Denominator*|

- 3⅖ = **ela** kakuso **ima** koluso **mahela** biso

## Factorial

In the case of a power of two, the power value is omitted.

||
|-|
|**eli** *Numeric* **imu** *Factorial value*|

- 4² = **eli** nelumo
- 3⁸ = **eli** kolumo **imu** kewalo

## Square root (√)

||
|-|
|**elu** (**eli** *Integer* **mahela**) *Square root value*|

- √2 = **elu** kakuso
- 2√5 = **elu eli** kakuso **mahela** biso

## Calculation

The following words and symbols are used for basic arithmetic operations.

|Symbol|Word|
|:-:|:-:|
|+|lisata|
|-|bataka|
|×|hanaba|
|÷|mahela|
|=|iku|
|( )|aga ~ ago|

### Reading out a formula

Formulas are read using a different grammar and meaning than usual.

They cannot be embedded in sentences, except for statements (the parts in quotation marks). Like numbers, formulas are written in Arabic numerals and symbols.

||||||
|:-:|:-:|:-:|:-:|:-:|
|1|+|1|=|2|
|taso|lisata|taso|iku|kakuso|

Expressions with parentheses put the expression between **aga** and **ago**, which correspond to opening and closing parentheses. You can also nest two or more.

||||||||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|3|×|(|(|8|-|2|)|÷|3|)|=|6|
|kolumo|hanaba|aga|aga|kewalo|bataka|kakuso|ago|mahela|kolumo|ago|iku|kuloso|

## Dates and Days

Dates and times have different meanings depending on the prepositions they are combined with, but the prepositions used are the same.

**In Zokale, we use different prepositions for numbers and spaces**. Please be careful not to use the wrong prepositions.

|Absolute representation|to|from|spend|until|
|-|:-:|:-:|:-:|:-:|
|**Preposition**|egu|eta|etu|eto|

|Relative representation|before|after|from the before|to the after|spend|until ~ ago|until ~ after|
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**Preposition**|ega|ego|eta ega|eta ego|etu|eto ega|eto ego|
|(Abbreviation)|||eta -|eta +||eto -|eto +|

“From” and “until” contain the number specified in the complement. “spend” is the same for both expressions.

- From June 23 = from exactly midnight on June 23
- From May 4 = 24:00 on May 4 (exactly midnight on May 5)

For example, “from before (after)" and “until before (after)” can be written by replacing ega with “-” and ego with “+” and concatenating them into a complement.

### Date

|⁠|Century|Year|Month|Week|Day|
|-|:-:|:-:|:-:|:-:|:-:|
|**Noun**|sigole|buwose|mahine|semane|palibe|
|**Modifier**|sigolo|buwoso|mahino|semano|palibo|
|**Verb**|sigola|buwosa|mahina|semana|paliba|

#### Absolute date

Use ISO 8601 YYYY‑MM‑DD instead of words for notation. Hyphens between numbers use **U+2011 NON‐BREAKING HYPHEN**.

The reading is done by separating the numbers into their digits. **esu** is not required.

|||
|-|-|
|**Example**|July 25th, 1983 (USA) / 25th July 1983 (UK)|
|**Notation**|1983‑07‑25|
|**Pronunciation**|taso nado kewalo kolumo **buwose** sepo **mahine** kakuso biso **palibe**|

You can also use the following notation, using the first letter of each word except century.

|||
|-|-|
|**Example**|July 25th, 1983 (USA) / 25th July 1983 (UK)|
|**Notation**|1983b‑7m‑25b|
|**Pronunciation**|taso nado kewalo kolumo **buwose** sepo **mahine** kakuso biso **palibe**|

|||
|-|-|
|**Example**|July 25th (USA) / July THE 25th (UK)|
|**Notation**|7m‑25p|
|**Pronunciation**|sepo **mahine** kakuso biso **palibe**|

|||
|-|-|
|**Example**|the 25th (USA) / THE 25th (UK)|
|**Notation**|25p|
|**Pronunciation**|kakuso biso **palibe**|

#### Relative date

The notation can be either a word or a shortened notation combining the first letter of the word and **“+”**.

|||
|-|-|
|**Example**|three days|
|**Notation**|3 palibe|
|**Reduction**|3p|
|**Pronunciation**|kolumo pabile|

The year and month are also written the same way, just with different words. If you want to pronounce more than one word, use **“ima”** to join them.

|||
|-|-|
|**Example**|three years and seven month|
|**Notation**|3 buwose ima 7 mahine|
|**Reduction**|3b+7m|
|**Pronunciation**|kolumo **buwose** <u>ima</u> sepo **mahine**|

#### Usage examples

|||
|-|-|
|**Notation**|belana da Ingulixu etu 3m‐1p eto 4m‐7p wa.|
|**Pronunciation**|belana da Ingulixu etu kolumo mahine taso palibe eto nelumo mahine sepo palibe wa.|
|**Meaning**|I will be studying English from March 1 to April 7.|

|||
|-|-|
|**Notation**|eka malaga da Nihon eta aga 2p etu 6p wa.|
|**Pronunciation**|eka malaga da Nihon eta aga kakuso palibe etu kuloso palipe wa.|
|**Meaning**|I spent six days traveling around Japan starting the day before yesterday.|

|||
|-|-|
|**Notation**|anebelana ego 1s ago da Amelika etu 2b+6m wa.|
|**Pronunciation**|anebelana ego taso semane ago da Amelika etu kakuso buwose ima kuloso mahine wa.|
|**Meaning**|I'm going to the US in a week to study for two and a half years.|

### Time and Duration

|⁠|Hour|Minute|Second|
|-|:-:|:-:|:-:|
|**Noun**|hawole|minute|biyode|
|**Modifier**|hawolo|minuto|biyodo|
|**Verb**|hawola|minuta|biyoda|

#### Time (Absolute notation)

Instead of using words, use ISO 8601 hh:mm:ss or hh:mm or hh. It is also possible to use the time difference expressions described below. To prevent line breaks, insert a **U+2060 WORD JOINER** after the colon and decimal point joining the numbers.

The number is read out loud, separated by digits. Milliseconds are read in the same way as decimals, using kiko. **esu** is not necessary. You can omit the last unit word except **hh**.

|||
|-|-|
|**Example**|2時14分36秒205|
|**Notation**|02:⁠14:⁠36.⁠205|
|**Pronunciation**|kakuso **hawole** taso nelumo **minute** kolumo kuloso **kiko** kakuso nulo biso **(biyode)**|

|||
|-|-|
|**Example**|2時14分36秒205 UTC+9|
|**Notation**|02:⁠14:⁠36.⁠205+09:00|
|**Pronunciation**|kakuso **hawole** taso nelumo **minute** kolumo kuloso **kiko** kakuso nulo biso **biyode** **lisato** nado **hawole**|

|||
|-|-|
|**Example**|18時3分24秒|
|**Notation**|18:03:24|
|**Pronunciation**|taso kewalo **hawole** kolumo **minute** kakuso nelumo **(biyode)**|

|||
|-|-|
|**Example**|twenty‐one fifty‐six|
|**Notation**|21:56|
|**Pronunciation**|kakuso taso **hawole** taso nelumo **(minute)**|

|||
|-|-|
|**Example**|three o'clock|
|**Notation**|03:00|
|**Pronunciation**|kolumo **hawole**|

#### Duration (Relative notation)

The notation uses the first letter of the word, and the pronunciation uses ima to connect the units. Unlike time, **units cannot be omitted**.

**There is no upper limit to the numerical value of each unit**, so it can be expressed in either the hexadecimal or decimal system.

|||
|-|-|
|**Example**|two hours and fourteen minutes and thirty‐six seconds and two‐eight‐five|
|**Notation**|2h:⁠14m:⁠36.⁠285b|
|**Pronunciation**|kakuso **hawole** <u>ima</u> taso nelumo **minute** <u>ima</u> kolumo kuloso **kiko** kakuso kewalo biso **biyode**|

#### Usage example

|||
|-|-|
|**Notation**|eka pelata anu kamale eta 19:30 eto 22:45 wa.|
|**Pronunciation**|eka pelata anu kamale eta taso nado hawole kolumo nulo eto kakuso kakuso hawole nelumo biso wa.|
|**Meaning**|I played from 7:30 p.m. to 10:45 p.m. in my room.|

|||
|-|-|
|**Notation**|menuga aga da Tookiyo etu 1h:15m ago da Oosaka wa.|
|**Pronunciation**|menuga aga da Tookiyo etu taso hawole ima taso biso minute ago da Oosaka wa.|
|**Meaning**|I'm going to Osaka, an hour and 15 minutes from Tokyo.|

|||
|-|-|
|**Notation**|binasa ego 9h:46m da Mapele.|
|**Pronunciation**|binasa ego nado hawole ima nelumo kuloso minute da Mapele.|
|**Meaning**|In 9 hours and 46 minutes, the Earth will be destroyed.|

|||
|-|-|
|**Notation**|eka bolida kita eto -5h anu lane wolola.|
|**Pronunciation**|eka bolida kita eto ega biso hawole anu lane wolola.|
|**Meaning**|Until five hours ago, the aurora was visible in the sky.|

### Extra grammar: Extreme shortening of date and time

The following contractions can be achieved in sentences that use paired prepositions, such as **eta ~ eto** or **ega ~ ego**.

|||
|-|-|
|**Notation**|malaga anu da Nihon 3m‐4p/4m‐12p wa.|
|**Meaning**|I will be take a trip to Japan from March 4 to April 12.|

|||
|-|-|
|**Notation**|gula da Nihon wagone anu zokalo 5m‐6p+3b+7m wa.|
|**Meaning**|I will be teaching Japanese around the world for 3 years and 7 months starting May 6.|

|||
|-|-|
|**Notation**|eka pelata anu kamale 19:30/22:45 wa.|
|**Meaning**|I played in my room from 7:30 p.m. to 10:45 p.m.|

|||
|-|-|
|**Notation**|eka belana do Zokale wagone -3h:45m/+2h:15m wa.|
|**Meaning**|I studied Zokale from 3 hours 45 minutes before to 2 hours 15 minutes after.|

|||
|-|-|
|**Notation**|eka belana da Esupaniyolu -5h:25m/13:35 wa.|
|**Meaning**|I studied Spanish from 5 hours 25 minutes before to 1:35 p.m.|

|||
|-|-|
|**Notation**|eka belana da Ingulixu 09:20+1h:30m wa.|
|**Meaning**|I studied English for an hour and a half starting at 9:20 a.m.|

|||
|-|-|
|**Notation**|eka nawala lawale 14:50/-2h:35m wa.|
|**Meaning**|I lost my memory from 2:50 p.m. to 2 hours and 35 minutes before.|

### Combined date and time notation

The notation is almost identical to ISO 8601. The date and time are joined by a **“T”**, and the time difference is expressed as a relative value **+hh:mm**.

The date and time are pronounced with ima, and the units are not omitted. The time difference is indicated by **lisato (+)** or **batako (-)**.

The time difference can also be used to indicate normal time.

|||
|-|-|
|**Example**|2021年3月28日17時4分56秒 UTC+9|
|**Notation**|2021‑03‑28T17:04:56+09:00|
|**Pronunciation**|kakuso nulo kakuso taso **buwose** kolumo **mahine** kakuso nado **palibe** <u>ima</u> taso sepo **howale** melumo **minute** biso kuloso **biyode lisato** nado **hawole**|

#### Summer time

In notation, the time zone is suffixed with a **“K”** (the first letter of kize, which means “season”).

For pronunciation, gilakize (summer) is added at the end with ima.

|||
|-|-|
|**Example**|2021年3月28日17時4分56秒 UTC-2:30 Summer time|
|**Notation**|2021‑03‑28T17:04:56-02:30K|
|**Pronunciation**|kakuso nulo kakuso taso **buwose** kolumo **mahine** kakuso nado **palibe** <u>ima</u> taso sepo **howale** melumo **minute** biso kuloso **biyode batako** kakuso **hawole** <u>ima</u> kolumo nulo **minute** <u>ima</u> **gilakize**|

#### Coordinated universal time (UTC)

The notation is **“Z”** at the end of the time.

Pronunciation is **sutade (standard)** followed by ima.

|||
|-|-|
|**Example**|2021年3月28日17時4分56秒 UTC|
|**Notation**|2021‑03‑28T17:04:56Z|
|**Pronunciation**|kakuso nulo kakuso taso **buwose** kolumo **mahine** kakuso nado **palibe** <u>ima</u> taso sepo **howale** melumo **minute** biso kuloso **biyode** <u>ima</u> **sutade**|
