# Zokale

Zokale (do Zokale wagone) is an artificial international auxiliary language composed of:

- **International auxiliary language**
	- A language that allows people with different cultures and customs to communicate smoothly.
	- There are no idioms or phrasal verbs.
- **Strictly defined language**
	- Ambiguity is eliminated to the greatest extent possible by strictly defining meanings so that the same word will not be interpreted differently depending on the speaker.
- **Ideal language**
	- A language that pursues the author's ideals.

## Current development stage and status

RC (Release Candidate) version.

The grammar will **in principle** remain unchanged, except in the following cases

- A fatal flaw in the grammar is discovered.
- If there is a request from a third party, and after verification with a third party other than the requestor, it is determined that it is safe to add to the grammar.

**All words are not finalized**. They are subject to change, consolidation, or deletion due to changes in grammar or coined words.

## About the grammar

- The explanations at this time are in **reference format**. It is not structured to be read in order like a book. If you encounter something that is not explained, please refer to the corresponding page.
- The explanations are written on the assumption that they will be written in Latin characters. If you are writing with special characters (a prototype is available at [Dropbox](https://www.dropbox.com/sh/imj3m2r5qriefnr/AACg1Bcy2hddeaB5_MqUZECxa?dl=0)), some parts may be different.

## About dictionaries

- Headings are word roots or stems. Please omit the part-of-speech endings and search.
- [Identifier](grammar/Identifier.md) and [Marker](grammar/Marker.md) are not included. Please refer to the appropriate grammar.
- Classified nouns are not included in the dictionary unless they are very universal words.
- Parts of speech whose meaning can be easily guessed from other parts of speech may not be listed. **You can use parts of speech that are not listed as long as they make sense (Excluding words tagged as “◯◯化禁止”)**.
- Etymology is based on information on the Internet. **Etymology is not based on a publisher's dictionary and is not guaranteed to be accurate**. If a word is based on an incorrect etymology, the etymology itself will be removed, but the word will not be changed unless there is a need to do so.

## About verification texts

- If there is more than one way to express a word, multiple answers will be written.
- If the question can be interpreted in more than one way, I will write the answer for that way, or the answer that the author of the question can expect.

## Links

Note: Most of the information is written in Japanese.

- [Grammar site](https://eotplb.gitlab.io/do-zokale-wagone/)
- [Tumblr](https://www.tumblr.com/zokale4blog) (Comprehensive information site)
- [Mastodon](https://mastodon.social/@zokale) **(Planned to be abolished)**

## License

This artificial language is created by [SERIYAMA Harushige (芹山はるしげ)](https://gitlab.com/eotplb).

The license is [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
