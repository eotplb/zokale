# Frequently Asked Questions

## General questions

### Q. This English translation is something strange!?

Don't worry, 95% of the translations are excellent machine translations, and only 5% are manually corrected. Anyway, non‐Japanese people will not look at me if there is no English translation, and it is more important that there is an English translation than the quality of the translation, right?

### Q. Why did you create an artificial language?

If you were satisfied with the existing artificial languages, you wouldn't have created them **Don't ask useless questions**.

### Q. What is the classification of this artificial language?

It's basically a **international auxiliary language**, but it's also a **ideological language** (≠ philosophical language) and a **experimental language**.

### Q. Why not an artistic language? If you're going to make a new language, it should be an artistic language.

(Unlike Japan, international auxiliary languages are more popular in the world than artistic languages, so this question may be inappropriate.)

**The question is meaningless** because it has a different purpose.

If you really want “settings”, you can create a lot of classification nouns (equivalent to proper nouns) to recreate the worldview you want. You can use existing ones, completely original ones, whatever.

According to the language specification, classifiers must be preceded by **a unique class identifier**. Ordinary words and categorial nouns are like a browser and its extensions. **They are completely separate and can never be merged**.

In this case, it is better to use “Conworldlang (Constructed World Language)”, since “artistic language” is ambiguous about the subject of "art". In this case, it would be better to use "Conworldlang = Constructed World Language", which is the same way that international auxiliary languages are classified as a branch of auxiliary languages.

### Q. Do you want to take over the world with this artificial language?

Why not? This language is just a communication aid, like sign language or Braille.

But it does have a “philosophy” in it. **However, the language does contain a "philosophy": to treat all living things (humans, animals, plants, etc.) equally**.

### Q. Why don't you just use Esperanto?

Then use Esperanto. Don't leave it up to others to decide which artificial language to use. Follow the path you believe in.

### Q. Isn't it enough to use English in this world?

That's right. **If you can use English, you don't need to learn an artificial language**.

### Q. Does the lack of consonant clusters mean it's a clone of toki pona?

In your mind, if it doesn't have consonant clusters, it's all toki pona. I remember when the NES, Turbo Grafx, and GENESIS were in their prime, there was a dumb guy who called all of these consoles “NES”. You're in the same league as him.

### Q. What is the origin of language names?

There is no such thing. The names are completely a priori and are not based on grammar.

What's more, we didn't have a language name in mind from the beginning, but rather came up with various names during the development process. In the end, we decided on Zokale, which means “world”.

### Q. Why is the word order VOS?

The language originated as a **magic chanting language** used in the novels I wrote to simulate scenarios for tabletop RPGs (1st generation Zokale. The current Zokale is **3rd generation**. 2nd generation Zokale IS NOT a standard grammar). Since it's magic, it's important to know "what magic", “how”, and “to what” it's being cast, not who cast it. Inevitably, the order of priority is **verb -> object/complement -> subject**, with the almost meaningless subject placed last. That's all.

### Q. Why is most of the vocabulary from Pacific countries?

Q. Why is most of the vocabulary from Pacific countries? If we were to focus on Indo-European, most of the words would have been altered to such an extent that it would be impossible for most people to recognize the original words. How is that different from a priori? If that's the case, then it's better to adopt a language that is as aposteriori as possible. Well, it's not a familiar Western word, so it might not be any different from a priori anyway.

In case you're wondering, it's not a Zonal Auxiliary Language because it uses several completely different language systems.

### Q. Is the pronunciation range surprisingly wide?

Pronunciation is the biggest stumbling block for Japanese people trying to learn other languages, not just artificial languages. This is why I made it so that even if you pronounce the words as if they were Japanese, you can still understand what they mean, even if the pronunciation is a bit strange. The closest natural language pronunciation that I can think of is Spanish, which is also one of the easiest natural languages to pronounce, so I thought it would be easier for people from any country to pronounce.

### Q. Why are the pronunciation and syllables of the classifiers expanded?

To make transliteration easier. That's all there is to it. Of course, there is no need to use the expanded pronunciations and syllables, so you can use the same rules for transliteration of categorial nouns as for normal words. Regardless of which rules are used, they are still classified nouns, so the unique classification identifier is essential.

On the other hand, ordinary words should be kept simple. There is no need to optimize what cannot be optimized. For classification nouns, you can talk as much as you can talk.

### Q. It's too much to ask for all markers to be three letters!

There is no need to memorize all the markers. There are only a limited number of markers that are used in daily life, so you will only have trouble in the beginning. Well, the author is old and his brain cells are dead, so he can only remember about 40% of them.

### Q. Isn't it too easy to say that a compound word is just putting words together?

A. No, it's not. The artificial languages that abound in the world have piled up a mountain of unnecessary exception grammars, and this is the biggest reason why people lose motivation to learn, thinking, "I'll just use Esperanto instead of learning this complicated grammar. It is a common story that a person who disagrees with the irregular conjugation of English and creates an artificial language of his own, actually finds that the artificial language is full of exceptions and has a grammar as complex as English. If you make it complicated without a clear purpose or reason for making it complicated, it will not be understood by a third party. It is better to keep the grammar as simple as possible.

### Q. Speaking of which, what are the features of this artificial language?

A. There are no features. Simplicity means that there are no features.

If I had to list them, I would say the following, including the ones I already mentioned.

- No consonant clusters or double vowels, so pronunciation is easy. It's okay to use the whole Japanese language.
- The accent of ordinary words and markers is fixed on the first syllable. Classified nouns are fixed on the final syllable.
- Classified nouns are completely independent from regular words by virtue of their unique classification identifier.
- The unique first/second person can be used. Pronouns can be extended indefinitely, as in Japanese.
- It is composed of a black-and-white vocabulary that eliminates ambiguity to the utmost limit.

### Q. I'll ask one last time, why did you create this artificial language?

I have studied many artificial languages, but they all have some flaws and are not suitable for practical use. I think it's a waste of time to keep learning them. So, why don't I just create my own? The reason is quite simple.

## Grammar Questions

### Q. I want to use a word not in the dictionary.

There is no problem in creating a new compound word from an existing word. A. There is no problem in creating a new compound word from an existing word, except that if it is only used by speakers of a particular native language, it must be explained.

If you want to add a completely new word, it is better to add it as a classified noun. If the exact same word is added later as an official, common word, it will be difficult to correct. Classified nouns can also be used as automata, so there should be no problem.

In addition, **classified nouns allow the same word to have different meanings. In addition**, categorial nouns allow the same word to have different meanings, and there are plenty of words in the real world that are covered by them, so just explain them at the beginning of a conversation (or document).
