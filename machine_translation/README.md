# Precautions for the English version

- The quality of the English translation is extremely poor. It is natural because it is mostly machine translation.
- Please DO NOT ASK the author about the English translation. I can't answer.
- I have no plans to translate the [dictionary in Japanese](../ja/dictionary/Zokale.md) into English. Because dictionaries written in Japanese are written on the premise that you understand Japanese and Japanese culture and customs.
	- Cultures and customs differ depending on the learner's home country. The same word can have a different image.
	- If we were to make an English dictionary, we would need at least an American, British, and Australian version. In some cases, a Southeast Asian version may be necessary.
	- As I don't know any “living English,” I can't make one.

**Introductory explanatory notes for Japanese dictionary**

|||
|-|-|
|**タ⁠グ** (Tag)|Represents the classification or lineage of the word.|
|**名⁠詞** (Noun)|Represents the meaning of a noun.|
|**自⁠動⁠詞** (Intransive verb)|Represents the meaning when a verb is used as an intransive verb.|
|**他⁠動⁠詞** (Transive verb)|Represents the meaning when a verb is used as a transive verb.|
|**形⁠容⁠詞** (Adjective)|Represents the meaning when a modifier is used as an adjective.|
|**副⁠詞** (Adverb)|Represents the meaning when a modifier is used as an adverb.|
|**間⁠投⁠詞** (Interjection)|Represents the meaning when a verb is used as an interjection.|
|**原⁠義** (Original meaning)|Represents the meaning of the individual words that make up a compound word.|
|**語⁠法** (Word usage)|Here are some notes on the use of this word. **It is written with the assumption that Japanese people will read it, so if a non-Japanese person reads it, there is a possibility that the explanation is unintelligible.**|
|**連⁠語** (Collocation)|It contains examples of collocation.|
|**一⁠覧** (List)|It is a collection of collocations by classification.|
|**語⁠源** (Word origins)|It describes the etymology of the word.|

## Contents

- [README](en/README.md)
- [FAQ](en/FAQ.md)

### Grammar

1. [Language overview](en/grammar/Language_overview.md)
1. [Letters and Pronunciation](en/grammar/Letters_and_Pronunciation.md)
1. [Identifier](en/grammar/Identifier.md)
1. [Noun](en/grammar/Noun.md)
1. [Modifier](en/grammar/Modifier.md)
1. [No article](en/grammar/No_article.md)
1. [Verb](en/grammar/Verb.md)
1. [Marker](en/grammar/Marker.md)
1. [Tense and Aspect](en/grammar/Tense_and_Aspect.md)
1. [Preposition](en/grammar/Preposition.md)
1. [Question](en/grammar/Question.md)
1. [Speech](en/grammar/Speech.md)
1. [Conjunctions and Conditional sentences](en/grammar/Conjunctions_and_Conditional_sentences.md)
1. [Numeric](en/grammar/Numeric.md)
1. [Comparative and Superlative](en/grammar/Comparative_and_Superlative.md)
1. [Embedding a sentence](en/grammar/Embedding_a_sentence.md)
1. [Compound word](en/grammar/Compound_word.md)
1. [Marker lists](en/grammar/Marker_lists.md)

### Grammar verification sentences

- [58 sentences I want to eat an apple](en/grammar_verification_sentences/a1_58_sentences_I_want_to_eat_an_apple.md)
- [57 sentences of not eating enough apples](en/grammar_verification_sentences/a2_57_sentences_of_not_eating_enough_apples.md)
- [55 sentences to eat more apples](en/grammar_verification_sentences/a3_55_sentences_to_eat_more_apples.md)
- [About 100 linguistically interesting sentences](en/grammar_verification_sentences/a4_About_100_linguistically_interesting_sentences.md)
